import os
import shutil
import tempfile
from pathlib import Path
from typing import List, Tuple
import urllib.request
import tarfile
import segyio
import h5py
import xarray as xr
import numpy as np


def create_xarray_from_binary(
    filename_prefix: str,
    required_models: list,
    n: Tuple[int],
    d: Tuple[float],
    origin: Tuple[float] = (0, 0, 0),
    solver: str = None,
    verbose: bool = False,
):
    """Create HDF5 domain file from binary files.

    Parameters
    ----------
    filename_prefix : string
        Filename prefix template of the files containing the domain.
    n : tuple of int
        Dimensions in (x, y, z). Set y to 1 for a 2D medium.
    d : tuple of floats
        Spatial discretization in (x, y, z).
    origin : tuple of floats
        Origin in (x, y, z).
    solver: string
        If `solver` is one of 'matterhorn', 'salvus', or 'wfi',
        then the required binary (specific to the solver) files will be also created.

    Returns
    -------
    dataset: xarray.Dataset
        Xarray dataset containing the model.
    fn_h5: str
        File name of the netCDF file.
    """

    is3D = True if n[1] > 1 else False
    if is3D: print(f"3D domain")
    else: print(f"2D domain")

    filename_prefix = Path(filename_prefix)  # .resolve()
    model_list = dict()
    missing_models = 0
    for m in required_models:
        fn = Path(str(filename_prefix).format(m))
        if verbose:
            print(f"Reading file `{fn}`")
        if not fn.exists():
            print(f"The '{m}' model file is missing!")
            missing_models += 1
        else:
            model_list[m] = fn

    if missing_models:
        raise ValueError(f"{missing_models} model(s) file(s) is(are) missing.")
    else:
        fn_h5 = Path(str(filename_prefix).replace("_{}.bin", ".h5"))

    # Initialize the dataset with some coordinate arrays
    coords = {
        "x": np.arange(n[0]) * d[0] + origin[0],
        "z": np.arange(n[2]) * d[2] + origin[2],
    }
    if is3D:
        coords["y"] = np.arange(n[1]) * d[1] + origin[1]
    dataset = xr.Dataset(
        coords=coords
    )
    # Copy the data into xarray's data structure
    if is3D:
        shape = (n[1], n[0], n[2])
        labels = ("y", "x", "z")
    else:
        shape = (n[0], n[2])
        labels = ("x", "z")
    for model, fn in model_list.items():
        data = np.fromfile(str(fn), dtype=np.float32).reshape(shape) #.T # WHY WAS I TRANSPOSING THIS IN 2D? SEE ALSO BELOW
        dataset[model] = (labels, data)

    # Write data to disk in netCDF format
    dataset.to_netcdf(fn_h5, format="NETCDF4", engine="h5netcdf")

    if solver == "matterhorn":
        # Create binary files for Matterhorn
        # dataset["VP"].transpose("x", "z").data.astype("float32").tofile(
        #     str(model_list["VP"]).lower()
        # )
        # dataset["RHO"].transpose("x", "z").data.astype("float32").tofile(
        #     str(model_list["RHO"]).lower()
        # )
        dataset["VP"].data.astype("float32").tofile(
            str(model_list["VP"]).lower()
        )
        dataset["RHO"].data.astype("float32").tofile(
            str(model_list["RHO"]).lower()
        )
    # elif solver == 'wfi':
    #     # Create binary files for WFI
    #     # WFI requires the velocity model only
    #     # WFI models are written in double precision
    #     dset = dataset['VP'].transpose('x', 'z').data.astype('float64')
    #     with open(f"{fn_prefix}_{'vp.bin'}", 'wb') as f2:
    #         f2.write(np.int32(number_of_cells[0]))
    #         f2.write(np.int32(number_of_cells[2]))
    #         f2.write(np.float64(cell_size[0]))
    #         f2.write(np.float64(cell_size[2]))
    #         f2.write(np.float64(origin[0]))
    #         f2.write(np.float64(origin[2]))
    #         dset[:].tofile(f2)
    # elif solver == 'salvus':
    #     pass
    else:
        if solver is not None:
            raise ValueError("The solver is not recognized!")

    return dataset, str(fn_h5.resolve())


def marmousi_original(
    path: str = Path().cwd(),
    overwrite: bool = False,
    cleanup: bool = False,
    verbose: bool = False,
) -> str:
    """Download, extract, and prepare the original Marmousi model.

    The model files will be downloaded and saved in a directory
    named `marmousi_original_model` within a directory specified by the user.

    Parameters
    ----------
    path: string or path object
        Destination directory of the retrieved model files.
    overwrite: bool
        If `True`, the existing `marmousi_original_model` directory will be removed
        before downloading again the model files.
    cleanup: bool
        If `True`, the archive is removed after downloading the data.

    Returns
    -------
    fn_h5: str
        File name of the netCDF file.

    References
    ----------
    [1] http://www.reproducibility.org/RSF/book/data/marmousi/paper.pdf
    """

    # Information about the Marmousi model
    fn_url = "http://trip.rice.edu/downloads/ieee.tar.gz"

    # Files and directories
    dir_base = Path("model")
    fn_archive = Path("marmousi_archive.tar.gz")
    fn_h5 = dir_base / "marmousi_original.h5"
    fn_prefix = dir_base / "marmousi_original"

    # Print informative message
    message = f"""This function will fetch the Marmousi model and prepare it
    for the chosen solver.To do so, it will create the directory
    '{dir_base.name}' in the current working directory '{Path().cwd()}'."""
    if verbose:
        print(message)

    # Check if directory already exists on disk
    if dir_base.is_dir():
        if overwrite:
            if verbose:
                print(f"The existing directory '{dir_base}' has been removed.")
            shutil.rmtree(dir_base)
        else:
            raise FileExistsError(
                f"""WARNING: The directory '{dir_base}' already exists.
                You need to remove it, rename it, or pass `overwrite=True` to this utility."""
            )

    # Download the archive
    if not fn_archive.is_file():
        result = urllib.request.urlretrieve(fn_url, fn_archive)
    else:
        if verbose:
            print(f"The archive '{fn_archive.name}' already exists.")

    # Extract
    tarfile.open(fn_archive, mode="r:gz").extractall()

    # Rename folder
    os.rename("ieee", dir_base.name)

    # Model list
    model_list = {
        "VP": (dir_base / "velocity.segy", "m/s"),
        "RHO": (dir_base / "density.segy", "kg/m^3"),
    }

    # Fix the header - Wrong format
    for fn in model_list.values():
        with segyio.open(fn[0], mode="r+", ignore_geometry=True) as f:
            f.bin[segyio.su.format] = 5

    # Create xarray Dataset
    number_of_cells = [2301, 1, 751]
    cell_size = [4.0, 4.0, 4.0]
    origin = [0.0, 0.0, 0.0]
    # Initialize the dataset with some coordinate arrays
    dataset = xr.Dataset(
        coords={
            "x": np.arange(number_of_cells[0]) * cell_size[0] + origin[0],
            "z": np.arange(number_of_cells[2]) * cell_size[2] + origin[2],
        }
    )
    # Copy the data into xarray's data structure
    for model, (fn, units) in model_list.items():
        data = segyio.open(fn, ignore_geometry=True).trace.raw[:]
        dataset[model] = (("z", "x"), data)

    # Write data to disk in netCDF format
    dataset.to_netcdf(fn_h5, format="NETCDF4", engine="h5netcdf")

    # Remove archive file
    if cleanup:
        os.remove(fn_archive)
        for fn in dir_base.glob("*.segy"):
            os.remove(fn)
        os.remove(dir_base / "README")

    return str(fn_h5)


def marmousi_elastic():
    """ Download, extract, and prepare the Marmousi elastic model.
    """

    pass


def prepare_segymodel(
    path: str,
    cells: int,
    cellsize: float,
    origin: float,
    cleanup: bool = False,
    bin: bool = False,
) -> str:
    """ Prepare an arbitrary segy-model.
    """

    pass
