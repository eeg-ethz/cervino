import os
from collections import OrderedDict
from pathlib import Path, PurePosixPath
from pprint import pprint
from time import sleep

# For cluster stuff
from paramiko import SSHClient
import re

from ..core.definitions import sites_cluster_valid

def save_bytes_to_file(b: bytes, fn: os.PathLike):
    """Save bytes literals to file.
    """
    
    with open(fn, 'w') as f:
        #f.writelines(b.decode("utf-8").splitlines(keepends=True))
        f.write(b.decode("utf-8"))
        #for line in b.decode("utf-8").splitlines():
        #    f.write(line)

def check_stdout_for_errors(b: bytes, fn: os.PathLike, error: str='error'):
    """Check the standard output for errors.
    """
    
    if error in b.decode("utf-8").lower():
        print("*********************")
        print("Something went wrong!")
        print(f"Check '{fn}'!")
        print("*********************")


################################
# Cluster
################################

class Cluster():
    """Cluster class.
    """

    def __init__(self, user: str, site: str='euler', verbose: bool=False):
        """Constructor
        """
        self._user = user
        self._site = site
        self._verbose = verbose
        self._hostname = sites_cluster_valid[site]
        self._connection = None
        self._is_connected = False

    def _open_connection_to_cluster(self):
        """Open connection
        """
        # Connect to the cluster (ssh)
        self._connection = SSHClient()
        self._connection.load_system_host_keys()
        self._connection.connect(hostname=self._hostname, username=self._user)

        self._is_connected = True
    
    def open(self):
        """Open connection
        """
        if not self._is_connected:
            self._open_connection_to_cluster()
            if self._verbose: print(f"Connection open!")
        else:
            print(f"There is already a connection to the cluster!")

    def _close_connection_to_cluster(self):
        """Close connection
        """
        # Close connection to cluster
        self._connection.close()
        self._is_connected = False

    def close(self):
        """Close connection
        """
        if self._is_connected:
            self._close_connection_to_cluster()
            if self._verbose: print(f"Connection closed!")
        else:
            print(f"There is no connection to the cluster!")

    def _mkdir_on_cluster(self, file_list: list, remote_folder: os.PathLike):
        """Create folder on the cluster

        Parameters
        ----------
        file_list: list
        remote_folder: Path
        """

        # Unique folders
        folder_list_unique = set()
        folder_list_unique.add(remote_folder)
        for fn in file_list:
            folder_list_unique.add((remote_folder / fn).parent)
        
        if self._verbose: print(f"The unique folders are:")

        for folder in folder_list_unique:
            if self._verbose: print(f"\t'{folder.as_posix()}'")
            self._connection.exec_command(f"mkdir -p {folder.as_posix()}")

    def transfer_files_to_cluster(self, file_list: list, remote_folder: os.PathLike):
        """Transfer files to a cluster

        Parameters
        ----------
        file_list: list
        remote_folder: Path
        """
        if not isinstance(file_list, list):
            file_list = [file_list,]
        
        # Make sure that remote_folder is a Path object
        remote_folder = Path(remote_folder)

        if not self._is_connected:
            print(f"There is no connection to the cluster!")
            return 0

        # First create the required folders on the cluster
        self._mkdir_on_cluster(file_list, remote_folder)
        
        # Transfer input files to the cluster
        ftp_client = self._connection.open_sftp()
        for fn in file_list:
            if self._verbose: print(f"From: {Path(fn)}")
            if self._verbose: print(f"To: {(remote_folder / fn).as_posix()}")
            ftp_client.put(Path(fn), (remote_folder / fn).as_posix())
        ftp_client.close()

    def _mkdir_on_local(self, file_list: list, local_folder: os.PathLike):
        """Create folder on the cluster

        Parameters
        ----------
        file_list: list
        local_folder: Path
        """

        # Unique folders
        folder_list_unique = set()
        for fn in file_list:
            folder_list_unique.add((local_folder / fn).parent)
        
        if self._verbose: print(f"The unique folders are:")

        for folder in folder_list_unique:
            if self._verbose: print(f"\t'{folder.as_posix()}'")
            os.makedirs(folder.as_posix(), exist_ok=True)

    def transfer_files_from_cluster(self, file_list: list, remote_folder: os.PathLike, local_folder: os.PathLike=Path().cwd()):
        """Transfer files from a cluster

        Parameters
        ----------
        file_list: list
        remote_folder: Path
        """
        if not isinstance(file_list, list):
            file_list = [file_list,]
        
        # Make sure that remote_folder is a Path object
        remote_folder = Path(remote_folder)

        # Make sure that remote_folder is a Path object
        local_folder = Path(local_folder)

        if not self._is_connected:
            print(f"There is no connection to the cluster!")
            return 0

        # First create the required folders on the local system
        self._mkdir_on_local(file_list, local_folder)
        
        # Transfer input files to the cluster
        ftp_client = self._connection.open_sftp()
        for fn in file_list:
            if self._verbose: print(f"From: {(remote_folder / fn).as_posix()}")
            if self._verbose: print(f"To: {Path(fn)}")
            ftp_client.get((remote_folder / fn).as_posix(), Path(fn))
        ftp_client.close()

    def _submit_job_to_lsf_cluster(self, script_list: list, remote_folder: os.PathLike, getid: bool=True):
        """Submit job to the scheduler

        Parameters
        ----------
        script_list: list
            Scripts to submit to the scheduler of the cluster.
        remote_folder: Path
            Remote working directory.

        Returns
        -------
        jobid: dict
            ID of the jobs submitted to the scheduler.
        """
        jobid_dict = dict()
        for fn in script_list:
            fn = Path(fn)
            if self._verbose: print(f"The executed command is:")
            if self._verbose: print(f"\t'cd {PurePosixPath(remote_folder).as_posix()} && bash {fn.as_posix()}'")
            stdin, stdout, stderr = self._connection.exec_command(f"cd {PurePosixPath(remote_folder).as_posix()} && bash {fn.as_posix()}")
            # Extract the job id
            if getid:
                jobid = re.findall(r'(?<=<)[\d]+(?=>)', stdout.readlines()[0])[0]
                if self._verbose: print(f"The jobid is {jobid}")
                jobid_dict[fn.as_posix()] = jobid

        return jobid_dict
    
    def submit_jobs_to_cluster(self, script_list: list, remote_folder: os.PathLike, getid: bool=True):
        """Submit job to the scheduler
        """
        if not isinstance(script_list, list):
            script_list = [script_list,]
        
        # Make sure that remote_folder is a Path object
        remote_folder = Path(remote_folder)
        
        if self._site == 'euler':
            jobid_dict = self._submit_job_to_lsf_cluster(script_list, remote_folder, getid)
        
        return jobid_dict

    def print_status_jobs(self, jobid_dict: dict) -> dict:
        """Check the status of submitted jobs
        """

        # if isinstance(list_of_jobid, list):
        #     list_of_jobid = dict_of_jobid

        if not self._is_connected:
            print(f"There is no connection to the cluster!")
            return 0
        
        # Check status and return extended dictionary
        jobid_status_dict = dict()
        for uid, jobid in jobid_dict.items():
            # Check the status of the job
            stdin, stdout, stderr = self._connection.exec_command(f"bjobs {jobid}")

            bjobs = stdout.readlines()
            bjobs_header = bjobs[0].split()
            bjobs_values = bjobs[1].split()
            # Compose SUBMIT_TIME value
            submit_time = f"{bjobs_values[-3]} {bjobs_values[-2]} {bjobs_values[-1]}"
            del bjobs_values[-3:]
            bjobs_values.append(submit_time)
            # Check if the EXEC_HOST field is empty
            if len(bjobs_values) < 8:
                bjobs_values.insert(5, '')
            bjobs_dict = OrderedDict()
            for i in range(8):
                bjobs_dict[bjobs_header[i]] = bjobs_values[i]
            jobid_status_dict[uid] = bjobs_dict

        # Extract status information
        n_pend = n_run = n_done = n_other = 0
        for uid, bjobs_dict in jobid_status_dict.items():
            if bjobs_dict['STAT'] == 'PEND':
                n_pend += 1
            elif bjobs_dict['STAT'] == 'RUN':
                n_run += 1
            elif bjobs_dict['STAT'] == 'DONE':
                n_done += 1
            else:
                n_other += 1
        status_summary_list = [n_pend, n_run, n_done, n_other]
        print(f"PEND, RUN, DONE, OTHER: {status_summary_list}")

        # Iterate over jobs of the workflow
        for uid, bjobs_dict in jobid_status_dict.items():
            print(f"The status of task `{uid}` (job {bjobs_dict['JOBID']}) is {bjobs_dict['STAT']}.")

        return jobid_status_dict