#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 11.06.2019

@author: Filippo Broggini (ETH Zürich) - filippo.broggini@erdw.ethz.ch
"""

import os
import sys
import socket
from pathlib import Path
import subprocess
from collections import OrderedDict
from typing import List, Union
from copy import deepcopy
from tqdm import tqdm_notebook
import time
import toml

from ..core.core import Cervino, outputs_valid


# pipelines = {'2d_line': create_2d_line()}

################################
# Pipeline class
################################

class Pipeline_old():
    r"""Cervino's Pipeline class.
    """

    def __init__(self, cervino: Cervino=None, name: str=None):
        r"""Constructor for the Pipeline class.
        """
        self.cervino = cervino

        self.result_list_of_input = None

        # Valid tasks
        self.pipeline_valid = {
            '2d_line': self.create_2d_line
            }
    
    def __repr__(self):
        return 'This is a pipeline for Cervino'

    def _repr_html_(self):
        return f"<p>This is a pipeline for <strong>Cervino</strong></p>"

    def create(self, pipeline_dict: OrderedDict) -> Union[List[Cervino]]:
        r"""Create the Cervino input from a dictionary.

        Parameters
        ----------
        pipeline_dict: OrderedDict
            Dictionary describing the pipeline workflow.
        """

        # Check that the task is a valid one
        for task in pipeline_dict.keys():
            if task not in self.pipeline_valid:
                print(f'The <{task}> task is not correct!')
        
        # Create the various Cervino objects associated
        # with each specific pipeline
        for task, params in pipeline_dict.items():
            self.result_list_of_input = self.pipeline_valid[task](params)
        
        return self.result_list_of_input

    def create_2d_line(self, source_locations: list=None) -> List[Cervino]:
        r"""Create a sequence of shot gathers.

        Parameters
        ----------
        source_locations: list
            List of tuples with source locations.
        """

        temp_dict = self.cervino.get_input()

        cervino_list = []

        for i, loc in enumerate(source_locations):
            # Copy the input dictionary
            cervino_temp = deepcopy(temp_dict)
            # Modify source location
            cervino_temp['source']['point-source'][0]['location'] = loc
            # Add suffix to output filename_prefix
            for key, val in cervino_temp['output'].items():
                for output in val[outputs_valid[key]]:
                    output['filename-prefix'] = f"{output['filename-prefix']}_{i:04}"
            # Append to list
            cervino_list.append(Cervino(name=f'Shot_{i:04}'))
            cervino_list[i].from_dict(cervino_temp)

        return cervino_list
    
    def from_toml(self, fn: str) -> dict:
        r"""Create the Cervino input from a TOML file.

        Parameters
        ----------
        fn: str
            Complete input file in TOML format.
        """
        # I should add checks here

        self.input_dict = toml.load(fn)
    
    def run(self, solver_binary: str=None, progress: bool=True) -> int:
        r"""Execute the pipeline.

        The dictionary can be used to create
        the general section of a TOML input file.

        Parameters
        ----------
        user: string
            Username which will be used for authentication purposes
        site: string
            Name of the local workstation or name of the remote HPC cluster.
            So far, Cervino runs only on the ETH Euler HPC cluster.
        solver: string
            Matterhorn or Salvus
        """

        nt = len(self.result_list_of_input)

        if progress:
            with tqdm_notebook(total=nt, initial=0, desc='Pipeline') as pbar:
                for i, simulation in enumerate(self.result_list_of_input):
                    simulation.to_toml(f'{simulation.name}.toml')
                    simulation.to_solver_input()
                    simulation.run()
                    pbar.update(1)
                   # time.sleep(1.0)
        
        return 0

