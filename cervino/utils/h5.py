import h5py

def func_dataset(name, obj):
    if isinstance(obj, h5py.Dataset):
        print(name)

def func_group(name, obj):
    if isinstance(obj, h5py.Group):
        print(name)

def h5_inspect(fn: str=None, show_datasets: bool=True, show_groups: bool=True, show_attribs: bool=False):
    with h5py.File(fn, mode='r') as f:
        if show_datasets:
            f.visititems(func_dataset)
        if show_groups:
            f.visititems(func_group)