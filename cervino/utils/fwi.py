import os
from pathlib import Path

def _create_commands_file(
    fn_commands: os.PathLike,
    files: list,
    nranks: int,
    verbose: bool=False
):
    """Create commands file
    """

    fn_commands = Path(fn_commands)
    folder = fn_commands.parent
    if verbose: print(f"The file is '{fn_commands.as_posix()}'")
    if verbose: print(f"The folder is '{folder}'")

    script = []

    # ##########################################################################
    # Add one line for each input file
    # ##########################################################################
    for fn in files:
        script.append(f"cd {folder.as_posix()}; mpirun -n {nranks} matterhorn {fn.as_posix()}\n")

    # Write script to file
    with open(fn_commands, 'w', newline='\n') as f:
        # Further file processing goes here
        f.writelines(''.join(script))

def _create_submission_script_fwi_v1(
    fn_submission_script: os.PathLike,
    fwi_dict: dict,
):
    """Create script to submit a job
    to a LSF scheduler such as Euler

    Parameters
    ----------
    fn_solver_input: str
        Matterhorn input file.
    fn_submission_script: str
        Submission script file.
    job_name: str
        Job name.
    ncores: int
        Number of cores.
    nranks: int
        Number of MPI ranks.
    """

    files_to_transfer = []

    # Create command files
    _create_commands_file(fwi_dict['commands_step1'], fwi_dict['input_files_step1'], fwi_dict['nranks_step1'])
    _create_commands_file(fwi_dict['commands_step2'], fwi_dict['input_files_step2'], fwi_dict['nranks_step2'])
    
    files_to_transfer.append(Path(fwi_dict['commands_step1']))
    files_to_transfer.append(Path(fwi_dict['commands_step2']))
    files_to_transfer.append(Path(fn_submission_script))

    script = []

    script.append('#!/bin/bash\n')

    # ##########################################################################
    # Load modules and add paths to $PATH
    # ##########################################################################
    script.append('. /cluster/apps/local/env2lmod.sh\n')
    script.append('module load gcc/8.2.0\n')
    script.append('module load openmpi/4.0.2\n')
    script.append('module load llvm/6.0.0\n')
    script.append('export PATH=/cluster/project/eeg/apps/matterhorn_gitlab/bin:$PATH\n')
    script.append('\n')

    # ##########################################################################
    # Create simple log files with iteration and jobid
    # ##########################################################################
    script.append("echo 'Iteration, JOBID_WS' > jobid_ws.txt\n")
    script.append("echo 'Iteration, JOBID_WR' > jobid_wr.txt\n")
    script.append("echo 'Iteration, JOBID_INV' > jobid_inv.txt\n")
    script.append("rm -f jobid.txt && touch jobid.txt\n")
    script.append('\n')

    # ##########################################################################
    # Set initial empty dependency
    # ##########################################################################
    script.append("dep_cond=\"\"\n")
    script.append('\n')

    # ##########################################################################
    # Loop - Beginning
    # ##########################################################################
    script.append(f"for iter in {{{fwi_dict['iter_beg']}..{fwi_dict['iter_end']}}};\n")
    script.append("do\n")
    script.append('\n')

    # ##########################################################################
    # Step 1
    # ##########################################################################
    script.append(f"\texport OMP_NUM_THREADS={fwi_dict['ncores_step1']}\n")
    script.append(f"\tJOBID_WS=$(bsub -n {fwi_dict['ncores_step1'] * fwi_dict['nranks_step1']} -N -B -J \"ws[{fwi_dict['job_beg_step1']}-{fwi_dict['job_end_step1']}]\" -o \"ws_${{iter}}_%I.out\" -e \"ws_${{iter}}_%I.err\" -w \"${{dep_cond}}\" \"awk -v jindex=\$LSB_JOBINDEX 'NR==jindex' {fwi_dict['commands_step1'].as_posix()} | bash\" | awk '/is submitted/{{print substr($2, 2, length($2)-2);}}')\n")
    script.append('\n')

    # ##########################################################################
    # Step 2
    # ##########################################################################
    script.append(f"\texport OMP_NUM_THREADS={fwi_dict['ncores_step2']}\n")
    script.append(f"\tJOBID_WR=$(bsub -n {fwi_dict['ncores_step2'] * fwi_dict['nranks_step2']} -N -B -J \"ws[{fwi_dict['job_beg_step2']}-{fwi_dict['job_end_step2']}]\" -o \"wr_${{iter}}_%I.out\" -e \"wr_${{iter}}_%I.err\" -w \"${{dep_cond}}\" \"awk -v jindex=\$LSB_JOBINDEX 'NR==jindex' {fwi_dict['commands_step2'].as_posix()} | bash\" | awk '/is submitted/{{print substr($2, 2, length($2)-2);}}')\n")
    script.append('\n')

    # ##########################################################################
    # Logs
    # ##########################################################################
    script.append("\techo \"$iter, $JOBID_WS\" >> jobid_ws.txt\n")
    script.append("\techo \"$iter, $JOBID_WR\" >> jobid_wr.txt\n")
    script.append("\techo \"$JOBID_WS\" >> jobid.txt\n")
    script.append("\techo \"$JOBID_WR\" >> jobid.txt\n")
    
    # ##########################################################################
    # Create dependencies variabale for inversion step
    # ##########################################################################
    script.append("\tdep_cond=\"numdone($JOBID_WS,*) && numdone($JOBID_WR,*)\"\n")
    script.append('\n')

    # ##########################################################################
    # Step 3
    # ##########################################################################
    script.append(f"\texport OMP_NUM_THREADS={fwi_dict['ncores_step3']}\n")
    script.append(f"\tJOBID_INV=$(bsub -n {fwi_dict['ncores_step3'] * fwi_dict['nranks_step3']} -N -B -J \"inv\" -o \"inv.out\" -e \"inv.err\" -w \"${{dep_cond}}\" \"echo 'iteration $iter' > iter${{iter}}.txt\"  | awk '/is submitted/{{print substr($2, 2, length($2)-2);}}')\n")
    script.append('\n')

    # ##########################################################################
    # Logs
    # ##########################################################################
    script.append("\techo \"$iter, $JOBID_INV\" >> jobid_inv.txt\n")
    script.append("\techo \"$JOBID_INV\" >> jobid.txt\n")
    script.append('\n')

    # ##########################################################################
    # Create dependencies variable for next iteration
    # ##########################################################################
    script.append("\tdep_cond=\"done($JOBID_INV)\"\n")
    script.append('\n')

    # ##########################################################################
    # Loop - End
    # ##########################################################################
    script.append("done")

    # Write script to file
    with open(fn_submission_script, 'w', newline='\n') as f:
        # Further file processing goes here
        f.writelines(''.join(script))

    return files_to_transfer
