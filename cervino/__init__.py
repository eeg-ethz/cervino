__doc__ = """Cervino is a Computational Environment for Research
in Visualization, INversion, and Optimization
"""

# __all__ = ['domain',
#            'input',
#            'objective_functions',
#            'optimization',
#            'output',
#            'solvers',
#            'util',
#            'vis']

from cervino.core import *
from cervino.domain import *
from cervino.input import *
from cervino.output import *
from cervino.utils import *
from cervino.solvers import *

# This is extracted automatically by the top-level setup.py.
__version__ = '0.0.3'
