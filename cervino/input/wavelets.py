import math
import numpy as np

DTYPE = np.float64

__all__ = ['BaseWavelet', 'RickerWavelet', 'RickerIntegratedWavelet', 'UserWavelet']

_sqrt2 = math.sqrt(2.0)

class BaseWavelet():
    """Base class for source wavelets.

    This is implemented as a function object, so the magic happens in the
    `__call__` member function.

    Methods
    -------
    __call__(self, t=None, nu=None, **kwargs)

    """

    def __init__(self, wavelet_type, *args, **kwargs):
        self.wavelet_type = wavelet_type
        self.wavelet = None

    def __call__(self, it=None, **kwargs):
        """Callable object method for the seismic sources.

        Parameters
        ----------
        it : int, array-like
            Index(es) at which to evaluate the wavelet.

        """

        if it is not None:
            return self._evaluate_time(it)
        else:
            raise ValueError('An index must be provided.')

    def _evaluate_time(self, it):
        pass

    def _save_binary(self, fn_prefix):
        r"""Save wavelet to binary file.
        """
        with open(f"{fn_prefix}.bin", 'wb') as f:
            self.wavelet.astype('float32').tofile(f)

class RickerWavelet(BaseWavelet):
    """Ricker wavelet.

    The Ricker wavelet is the negative 2nd derivative of a Gaussian [1]_.

    Parameters
    ----------
    t : float, ndarray
        Time array.
    fc : float
        Central (peak) frequency of the wavelet.
    delay : float, optional
        Cycle or time delay to be applied to the wavelet.
        The default delay is 1/fc if `delay_type`=='cycles' or 0.0 if `delay_type`=='s'.
    delay_type : string, optional
        It can be either 'cycles' (default) or 's'. It indicates whether the parameter
        `delay` corresponds to a delay in cycles or seconds.

    Attributes
    ----------
    wavelet : float, ndarray
        Array that contains the wavelet.

    References
    ----------
    .. [1] N. Ricker, The form and laws of propagation of seismic wavelets,
       Geophysics, vol. 18, pp. 10-40, 1953.
    """

    def __init__(self, t, fc: float, dt=None, delay: float=None, delay_type: str='cycles', create_binary: bool=False, fn_prefix: str=None, fmt: str='bin'):

        # Set the wavelet type
        super().__init__('ricker')

        self.t = t
        self.dt = np.diff(self.t)[0] if dt is None else dt
        self.fc = fc
        self.nt = t.size

        self.fmt = fmt

        # This needs to be checked
        if delay_type == 'cycles':
            self.delay_cycles = 1.0 if delay is None else delay
            self.delay_seconds = self.delay_cycles / self.fc
        elif delay_type == 's':
            self.delay_seconds = 0.0 if delay is None else delay
            self.delay_cycles = self.delay_seconds * self.fc
        else:
            raise ValueError(f"'{delay_type}' is not a valid value.")

        t_source = self.t - self.delay_seconds

        tau = (np.pi * self.fc * t_source) ** 2
        a = 2.0
        self.wavelet = (1 - a * tau) * np.exp(-(a / 2) * tau)

        if create_binary:
            if fn_prefix is None:
                raise ValueError('The fn_prefix has not been defined!')
            else:
                self._save_binary(fn_prefix)
                self.filename = f"{fn_prefix}.bin"

    def _evaluate_time(self, it):
        return self.wavelet[it]

    def get_stf_dict(self):
        """ Return a dictionary with the source time function parameters.

        The dictionary can be used to create
        the input section of a TOML input file.
        """

        return {
            "center-frequency": self.fc,
            "wavelet": self.wavelet_type,
            "time-shift-in-seconds": self.delay_seconds,
            "time-shift-in-cycles": self.delay_cycles,
            "filename-prefix": self.filename.replace(f'.{self.fmt}', ''),
        }

class RickerIntegratedWavelet(BaseWavelet):
    r"""Integral of the Ricker wavelet.

    This wavelet is the time integral of a Ricker wavelet.

    Parameters
    ----------
    t : float, ndarray
        Time array.
    fc : float, optional
        Central (peak) frequency of the wavelet.
    normalized : bool, optional
        Normalize maximum amplitude to 1
    delay : float, optional
        Cycle or time delay to be applied to the wavelet.
        The default delay is 1/fc if `delay_type`=='cycles' or 0.0 if `delay_type`=='s'.
    delay_type : string, optional
        It can be either 'cycles' (default) or 's'. It indicates whether the parameter
        `delay` corresponds to a delay in cycles or seconds.

    Attributes
    ----------
    wavelet : float, ndarray
        Array that contains the wavelet.
    """
    pass
    # raise NotImplementedError('The integrated Ricker wavelet needs to be implemented!')

    # def __init__(self, t, dt=None, fc=20.0, delay=None, delay_type='c', normalized=False, **kwargs):

    #     # Set the wavelet type
    #     super().__init__('integrated_ricker')

    #     self.t = t
    #     self.fc = fc
    #     if dt is None:
    #         self.dt = np.diff(self.t)[0]
    #     else:
    #         self.dt = dt
    #     if normalized:
    #         self.coeff = np.sqrt(2.0) * np.pi * self.fc / np.exp(-0.5)
    #     else:
    #         self.coeff = 1.0
    #     self.nt = t.size

    #     # This needs to be checked
    #     if delay_type == 'cycles':
    #         if delay is None:
    #             self.delay_cycles = 1.0
    #         else:
    #             self.delay_cycles = delay
    #         self.delay_seconds = self.delay_cycles / self.fc
    #     else:
    #         if delay is None:
    #             self.delay_seconds = 0.0
    #         else:
    #             self.delay_cycles = delay
    #         self.delay_cycles = self.delay_seconds * self.fc

    #     t_source = self.t - self.delay_seconds
    #     tau = (np.pi * self.fc * t_source) ** 2
    #     a = 2.0
    #     self.wavelet = (self.coeff * t_source) * np.exp(-(a / 2) * tau)

    # def _evaluate_time(self, it):
    #     return self

    # def get_stf_dict(self):
    #     """ Return a dictionary with the source time function parameters.

    #     The dictionary can be used to create
    #     the input section of a TOML input file.
    #     """

    #     return {
    #         "center-frequency": self.fc,
    #         "wavelet": self.wavelet_type,
    #         "time-shift-in-seconds": self.delay_seconds,
    #         "time-shift-in-cycles": self.delay_cycles
    #     }

class UserWavelet(BaseWavelet):
    """User defined wavelet.

    Parameters
    ----------
    filename : string
        File containing the user defined wavelet.
    input_fmt: string, optional
        Format of the file: 'bin', 'npy' (not implemented yet)
    input_dtype: string, optional
        For now, the only accepted dtype is `float32`.
    nt : int, optional
        Lenght of the time array. Number of time steps.
        When nt is not None, the constructor will check if the binary file
        contains exactly nt samples.

    Attributes
    ----------
    wavelet : float, ndarray
        Array that contains the wavelet.
    """

    def __init__(self, filename: str=None, input_fmt: str='bin', input_dtype: str='float32', nt: int=None):

        # Set the wavelet type
        super().__init__('user')

        if filename is None:
            raise ValueError('filename is not defined!')
        else:
            self.filename = filename

        self.input_fmt = input_fmt

        if input_dtype != 'float32':
            raise ValueError('input_dtype can only be float32 for now!')
        self.input_dtype = input_dtype

        self.wavelet = np.fromfile(self.filename, dtype=np.dtype(self.input_dtype))

        self.nt = self.wavelet.size

        if nt is not None:
            if self.nt != nt:
                raise ValueError('The number of samples in the user defined wavelet is not correct!')


    def _evaluate_time(self, it):
        return self.wavelet[it]

    def get_stf_dict(self):
        """ Return a dictionary with the source time function parameters.

        The dictionary can be used to create
        the input section of a TOML input file.
        """

        return {
            "wavelet": self.wavelet_type,
            "filename-prefix": self.filename.replace(f'.{self.input_fmt}', ''),
            "format": self.input_fmt
        }
