import numpy as np
import matplotlib.pyplot as plt

from ..core.definitions import source_spatial_type_valid, source_spatial_type_valid_string

__all__ = ['BaseSource', 'PointSource']

class BaseSource(object):
    """Base class for representing a source emitter on a grid.
    """

    def __init__(self, loc, stf, spatial_type='q', spatial_weights=1.0, **kwargs):
        """Constructor for the BaseSource class.
        """

        self.loc = loc
        self.stf = stf

        if spatial_type not in source_spatial_type_valid.keys():
            raise ValueError(f"The spatial type '{spatial_type}' is not valid! The valid source types are '{source_spatial_type_valid_string}'.")
        else:
            self.spatial_type = str(spatial_type)
        self.spatial_weights = spatial_weights
        # self.shot = None

    # def set_shot(self, shot):
    #     self.shot = shot

    def plot(self, tmax=None,
             aspect='auto', style='wavelet', figsize=None):
        """Plot the source wavelet.

        Parameters
        ----------
        tmax : float
            Max time to plot.
        aspect: float, 'auto', 'equal'
            See matplotlib documentation.
        style: str
            Not used.
        """

        # Remove the ugly ticks
        plt.tick_params(
            which='both',   # both major and minor ticks are affected
            bottom=False,   # ticks along the bottom edge are off
            top=False,      # ticks along the top edge are off
            left=False,     # ticks along the left edge are off
            right=False     # ticks along the right edge are off
        )

        # Create plot
        line = plt.plot(self.stf.t, self.stf.wavelet, linewidth=3)
        plt.title('Source wavelet')
        plt.xlabel('Time [s]')
        plt.ylabel('Amplitude')
        if tmax is not None:
            plt.xlim((0, tmax))

        return line

    def get_source_dict(self):
        """Return a dictionary with the source parameters.

        The dictionary can be used to create
        the source section of a TOML input file.
        """

        return {
            "location": self.loc,
            "spatial-type": self.spatial_type,
            "spatial-weights": self.spatial_weights,
            "source-time-function": self.stf.get_stf_dict()
        }

class PointSource(BaseSource):
    """Derived class which defines one ore more
    point source emitters on a grid.

    Parameters
    ----------
    loc : float, ndarray
        Location of the source in the physical coordinates of the domain.
        loc should be a (n x 3) ndarray, where n denotes
        the number of sources.
    stf : BaseWavelet, ndarray
        Function of time that produces the source time function.
        If only one source location is present, then stf should be a (1 x nt) ndarray.
        If multiple (n) source locations are present, then stf can be a (1 x nt) ndarray
        or a (n x nt) ndarray. In the first case, the same stf will be used
        for all source locations.
    spatial_type : string, optional
        Spatial type of the source. Monopole source: 'q'.
        Dipole sources: 'fx', 'fy', or 'fz'.
    spatial_weights: float
        Weights to scale the amplitudes of specific components.
    **kwargs : dict, optional
        May be used to specify `approximation` and `approximation_width` to
        base class.
    """

    def __init__(self, loc, stf, spatial_type: str='q', spatial_weights: float=1.0):
        """Constructor for the PointSource class.
        """

        super().__init__(loc, stf, spatial_type, spatial_weights)

        self.nt = self.stf.nt

        # Check dimensions of the locations array
        if np.squeeze(self.loc).ndim == 1:
            self.ns = 1
        else:
            self.ns = self.loc.shape[0]
            if np.squeeze(self.stf.wavelet).ndim == 1:
                self.wavelet_dim = self.ns
            elif np.squeeze(self.stf.wavelet).ndim == self.ns:
                self.wavelet_dim = self.ns
            else:
                raise ValueError('Something is wrong with the dimensions of the wavelet!')

        # Add a check on mode
        # self.spatial_type = spatial_type