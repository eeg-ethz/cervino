import numpy as np

class WfiOutput():
    r"""Class to represent the output of WFI

    """
    def __init__(self, ina, inb, inc, ind, ine):
        r"""Constructor for the WfiOutput class.
        """
        self.sno = ina[:, 0]
        self.rno = ina[:, 1]
        self.fno = ina[:, 2]
        self.souno = ina[:, 3]
        self.recfacno = ina[:, 4]
        self.dtype = ina[:, 5]
        self.data = ina[:, 6] + 1j*ina[:, 7]
        self.w = ina[:, 8]

        self.gidx = inb[:, 0]
        self.gx = inb[:, 1]
        self.gz = inb[:, 2]

        self.souidx = inc[:, 0]
        self.sour = inc[:, 1]
        self.soui = inc[:, 2]

        self.recfacidx = ind[:, 0]
        self.recfacr = ind[:, 1]
        self.recfaci = ind[:, 2]

        self.fidx = ine[:, 0]
        self.f = ine[:, 1]


def _wfi_get_data(fn_data: str=None) -> str:
    """Extract the computed data from the WFI data file.

    Parameters
    ----------
    fn_data: str
        WFI output file.

    Returns
    -------
    WfiOutput
        Class representing the WFI output.
    """

    a = b = c = d = e = 0

    # Go through the file once to get the size of the data
    with open(fn_data, 'r') as fn:
        for line in fn:
            if 'DATA' in line:
                a =  a + 1
            elif 'SPOINT' in line:
                b =  b + 1
            elif 'SOURCE' in line:
                c =  c + 1
            elif 'RECEIVER' in line:
                d =  d + 1
            elif 'FREQ' in line:
                e =  e + 1
    
    print(a, b, c, d, e)

    ina = np.zeros((a, 9))
    inb = np.zeros((b, 3))
    inc = np.zeros((c, 3))
    ind = np.zeros((d, 3))
    ine = np.zeros((e, 2))
    
    a = b = c = d = e = 0
    
    # Go through the file once again to extract the actual data 
    with open(fn_data, 'r') as fn:
        for line in fn:
            if 'DATA' in line:                
                ina[a, :] = np.array(line.split()[1:]).astype(np.float)
                a =  a + 1
            elif 'SPOINT' in line:
                inb[b, :] = np.array(line.split()[1:]).astype(np.float)
                b =  b + 1
            elif 'SOURCE' in line:
                inc[c, :] = np.array(line.split()[1:]).astype(np.float)
                c =  c + 1
            elif 'RECEIVER' in line:
                ind[d, :] = np.array(line.split()[1:]).astype(np.float)
                d =  d + 1
            elif 'FREQ' in line:
                ine[e, :] = np.array(line.split()[1:]).astype(np.float)
                e =  e + 1

    return WfiOutput(ina, inb, inc, ind, ine)