 JSON Schema
============

"additionalProperties": false

The dependencies keyword allows the schema of the object to change based on the presence of certain special properties.

There are two forms of dependencies in JSON Schema:

    Property dependencies declare that certain other properties must be present if a given property is present.
    Schema dependencies declare that the schema changes when a given property is present.

Types
-----

* string
* Numeric types
* object
* array
* boolean
* null

Range
-----

"type": "number",
"exclusiveMinimum": 0
"maximum": 100,

Tuple
-----

enum
"additionalItems": false

Constant values
---------------

The const keyword is used to restrict a value to a single value.

Combining schemas
-----------------

