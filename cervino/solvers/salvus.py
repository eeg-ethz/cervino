import os
from pathlib import Path
import time

from .cervino_to_salvus import _convert_from_cervino
from .salvus_to_cervino import _convert_output_to_xarray
from ..core.core import Cervino, Tasks

from salvus.flow import api

solver_binary = 'salvus'

################################
# Local
################################

def _run_setup_local(tasks: dict) -> list:
    """Set up local simulation

    Parameters
    ----------
    tasks: dict
        Tasks of one pipe

    Returns
    -------
    solver_input_files: list
        List of tuples: uid and input file name
    """
    print("Setup local")
    # Create the input files for the selected solver
    solver_input_files = []
    tasks._iterate_over_tasks(solver_input_files, _convert_from_cervino)
    return solver_input_files

def _run_post_local(tasks: dict, output_folder: str) -> dict:
    """Post simulation

    Parameters
    ----------
    tasks: dict
        Tasks of one pipe
    output_folder: str
        General output folder of the cervino simulation.

    Returns
    -------
    solver_input_files: list
        List of tuples: uid and input file name
    """
    output_files_hdf5 = dict()
    # Create the input files for the selected solver
    for uid, task in tasks.get_tasks_dict().items():
        fn = f"{output_folder}/job_{uid}/output_{uid}.h5"
        output_files_hdf5[uid] = _convert_output_to_xarray(fn, task, output_folder)

    return output_files_hdf5

def _convert_output_local():
    """Convert the output to HDF5
    """
    return 'lol.h5'

def _run_compute_forward_simulation_local(cervino: Cervino, run_dict: dict, tasks: dict, timing: bool=False):
    """Run forward simulations with Salvus

    Parameters
    ----------
    cervino: Cervino
        cervino simulation object.
    run_dict: dict
        Run dictionary.
    tasks: dict
        Tasks of this pipe.

    Returns
    -------
    result: list
        List of tuple: standard output and standard error.
    output_file: str
        Output file in HDF5 format.
    """
    task_type = 'compute_forward_simulation'

    test = False
    if not test:
        # Setup
        time_start = time.time()
        uid_input_list = _run_setup_local(tasks)
        nt = len(uid_input_list)

        # Validate Waveform simulations
        s_list = []
        for _, s in uid_input_list:
            s_list.append(s)
            s.validate()
        cervino._is_validated = True
        time_elapsed = time.time() - time_start
        if timing: print(f"The pre processing took {time_elapsed}s.")

        # Run simulations
        time_start = time.time()
        api.run_many(
            input_files=s_list,
            ranks_per_job=run_dict['n_ranks'],
            site_name=cervino.workflow.dict[task_type][1], #.site,
            output_folder=cervino._output_folder,
            overwrite=True,
            get_all=True,
            verbosity=10
        )
        time_elapsed = time.time() - time_start
        if timing: print(f"The simulation took {time_elapsed}s.")
    result = []

    # Convert output to hdf5
    time_start = time.time()
    output_files_hdf5 = _run_post_local(tasks, cervino._output_folder)
    time_elapsed = time.time() - time_start
    if timing: print(f"The post processing took {time_elapsed}s.")
    return result, output_files_hdf5

################################
# Cluster
################################

def _run_setup_cluster():
    pass

def _compute_forward_simulation_cluster():
    pass

def _create_submission_script_lsf():
    pass

def _fetch_files_from_cluster():
    pass

def _transfer_files_to_cluster():
    pass
