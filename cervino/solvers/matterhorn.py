import os
from pathlib import Path, PurePosixPath
import multiprocessing
from ctypes import c_int32
import subprocess
from functools import partial
from collections import OrderedDict
from pprint import pprint
from time import sleep
from tqdm import tqdm_notebook
from numpy import prod

# For cluster stuff
from paramiko import SSHClient
import re

from .cervino_to_matterhorn import _convert_from_cervino
from .matterhorn_to_cervino import _convert_output_to_xarray
from ..core.core import Cervino, Tasks
from ..core.definitions import sites_cluster_valid
from ..utils.tools import save_bytes_to_file, check_stdout_for_errors

solver_binary = ['matterhorn',]

################################
# Counter stuff
################################

counter = multiprocessing.Value(c_int32)
counter_lock = multiprocessing.Lock()

def _update_counter():
    with counter_lock:
        counter.value += 1

################################
# Local
################################

def _run_setup_local(tasks: dict, output_folder: os.PathLike) -> dict:
    """Set up local simulation

    Parameters
    ----------
    tasks: dict
        Tasks of one job.
    output_folder: str
        General output folder of the cervino simulation.

    Returns
    -------
    solver_input_files_dict: dict
        Dictionary of input files: UID as key and input file name as value.
    """

    # Create logs folder
    # This method creates folder on the local system only
    Path("logs").mkdir(exist_ok=True)
    
    # Create general output folder
    # This method creates folder on the local system only
    # That's why `output_folder` is wrapped in a Path object
    Path(output_folder).mkdir(exist_ok=True)

    # Create task-specific output folder
    # This method creates folder on the local system only
    # That's why `output_folder` is wrapped in a Path object
    for uid in tasks.get_tasks_dict().keys():
        (Path(output_folder) / f"job_{uid}").mkdir(exist_ok=True)

    # Create the input files for the selected solver
    # The input files created by the `_convert_from_cervino` method
    # are OS-specific and the right OS is derived from the the subclass
    # of `output_folder`
    solver_input_files_dict = dict()
    tasks._iterate_over_tasks(solver_input_files_dict, partial(_convert_from_cervino, output_folder=output_folder))
    return solver_input_files_dict

def _run_post_local(tasks: dict, output_folder: os.PathLike, work_folder: os.PathLike) -> dict:
    """Post simulation

    Parameters
    ----------
    tasks: dict
        Tasks of one job.
    output_folder: str
        General output folder of the cervino simulation.

    Returns
    -------
    output_files_hdf5_dict: dict
        Dictionary of cervino output files: UID as key and output file name as value.
    """
    output_files_hdf5_dict = dict()
    # Create the output file for the selected solver
    for uid, task in tasks.get_tasks_dict().items():
        fn = output_folder / f"job_{uid}" / f"output_{uid}.h5"
        output_files_hdf5_dict[uid] = _convert_output_to_xarray(fn, task, output_folder, work_folder)

    return output_files_hdf5_dict

def _single_job_local(my_env: dict, uid_input: list) -> tuple:
    """Run one single local job

    Parameters
    ----------
    my_env: dict
        Environment.
    uid_input: list
        List of tuples: uid and input file name.

    Returns
    -------
    out, err: tuple
        Tuple comprising standard output and standard error.
    """
    print(f"The uid is {uid_input[0]}")
    # solver_binary = ["mpirun.openmpi", "-n", "8", "matterhorn"]
    # solver_binary = ['cmd.exe', "/c", 'dir']
    cmd = [*solver_binary, str(uid_input[1])]
    process = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)
    out, err = process.communicate()
    save_bytes_to_file(out, Path("logs") / f"{uid_input[0]}.out")
    check_stdout_for_errors(out, Path("logs") / f"{uid_input[0]}.out")
    save_bytes_to_file(err, Path("logs") / f"{uid_input[0]}.err")
    
    _update_counter()

    #return {uid_input[0]: (out, err)}
    return (uid_input[0], (out, err))

def _convert_output_local():
    """Convert the output to HDF5
    """
    return 'lol.h5'

def _run_compute_forward_simulation_local(
    cervino: Cervino,
    run_dict: dict,
    tasks: dict,
    timing: bool=False,
) -> int:
    """Run forward simulations with Matterhorn

    Parameters
    ----------
    run_dict: dict
        Run dictionary.
    tasks: dict
        Tasks of this pipe.

    Returns
    -------
    result: list
        List of tuple: standard output and standard error.
    output_file: str
        Output file in HDF5 format.
    """
    task_type = 'compute_forward_simulation'

    if 'setup_only' not in run_dict:
        run_dict['setup_only'] = False
    
    # Setup
    solver_input_files_dict = _run_setup_local(tasks, cervino._output_folder)
    nt = len(solver_input_files_dict)

    single_job_my_env = partial(_single_job_local, dict(os.environ, OMP_NUM_THREADS=f"{run_dict['n_ranks']}"))

    # Run simulations
    if not run_dict['setup_only']:
        with tqdm_notebook(total=nt) as pbar:
            with multiprocessing.Pool(run_dict['concurrent_jobs']) as pool:
                result_pool = pool.map_async(single_job_my_env, solver_input_files_dict.items())
                while not result_pool.ready():
                    if counter.value != 0:
                        with counter_lock:
                            increment = counter.value
                            counter.value = 0
                        pbar.update(increment)
                    sleep(5.0)
                pbar.update(counter.value)
                results = result_pool.get()
    else:
        results = []
    # result = []
    
    # Convert results to dictionary
    results_dict = dict() 
    for result in results:
        results_dict[result[0]] = result[1]
    
    # Convert output to hdf5
    cervino._output_files[task_type] = _run_post_local(tasks, cervino._output_folder, cervino._remote_folder)
    return results_dict

################################
# Cluster
################################

def _create_submission_script_lsf(
    fn_solver_input: os.PathLike,
    fn_submission_script: os.PathLike,
    job_name: str='cervino',
    ncores: int=24,
    nranks: int=1
    ):
    """Create script to submit a job
    to a LSF scheduler such as Euler

    Parameters
    ----------
    fn_solver_input: str
        Matterhorn input file.
    fn_submission_script: str
        Submission script file.
    job_name: str
        Job name.
    ncores: int
        Number of cores.
    nranks: int
        Number of MPI ranks.
    """

    script = []

    script.append('#!/bin/bash\n')

    # Load modules
    script.append('. /cluster/apps/local/env2lmod.sh\n')
    script.append('module load gcc/8.2.0\n')
    script.append('module load openmpi/4.0.2\n')
    script.append('module load llvm/6.0.0\n')
    script.append('export PATH=/cluster/project/eeg/apps/matterhorn_gitlab/bin:$PATH\n')

    if nranks > 1:
        script.append(f"bsub -n {ncores * nranks} -N -B -J \"{job_name}\" -o {job_name}.out -e {job_name}.err \'export OMP_NUM_THREADS={ncores}; mpirun -n {nranks} matterhorn {fn_solver_input.as_posix()}\'\n")
    else:
        script.append(f"bsub -n {ncores} -N -B -J \"{job_name}\" -o {job_name}.out -e {job_name}.err \'export OMP_NUM_THREADS={ncores}; matterhorn {fn_solver_input.as_posix()}\'\n")
    
    # Write script to file
    with open(fn_submission_script, 'w', newline='\n') as f:
        # Further file processing goes here
        f.writelines(''.join(script))

    # # -rw-r--r-- corresponds to 33188
    # # -rwxr--r-- corresponds to 33252
    # Path(fn_submission_script).chmod(33252)

    return fn_submission_script

def _get_model_files_from_task(task_dict: dict) -> tuple:
    """List of model files for a specific task:
    model parameters, ...

    Parameters
    ----------
    task_dict: dict
        Task dictionary.
    
    Returns
    -------
    uid: str
        UID.
    file_names: List[str]
        List of model file names for this task.
    """
    
    model_list = []
    # Model parameters files
    fn_prefix = Path(task_dict['domain']['model']['filename'].replace('.h5',''))
    for model in task_dict['domain']['model']['required-models']:
        model_list.append(Path(f"{str(fn_prefix)}_{model}.bin"))

    return (task_dict['general']['uid'], model_list)

def _get_additional_input_files_from_task(task_dict: dict) -> tuple:
    """List of additional input files for a specific task:
    DFT coefficients, ...

    Parameters
    ----------
    task_dict: dict
        Task dictionary.
    
    Returns
    -------
    uid: str
        UID.
    file_names: List[str]
        List of additional input file names for this task.
    """
    
    additional_input_list = []
    # DFT coefficents binary files
    if 'slice_dft' in task_dict['output']:
        additional_input_list.append(Path(task_dict['output']['slice_dft']['dft-wavelet-coefficients']))
    if 'volume_dft' in task_dict['output']:
        additional_input_list.append(Path(task_dict['output']['volume_dft']['dft-wavelet-coefficients']))
    # Receiver locations
    if 'gather' in task_dict['output']:
        for rec in task_dict['output']['gather']['receivers'].values():
            try:
                additional_input_list.append(Path(rec['receivers-filename']))
            except:
                pass

    return (task_dict['general']['uid'], additional_input_list)

def _transfer_files_to_cluster(cluster: SSHClient, list_of_files: list, output_folder: os.PathLike):
    """Transfer files to a cluster

    Parameters
    ----------
    cluster: SSHClient
    list_of_files: list
    output_folder: Path
    """

    # Transfer input files to the cluster
    ftp_client = cluster.open_sftp()
    for fns in list_of_files:
        if isinstance(fns, list):
            for fn in fns:
                print(f"From: {Path(fn)}")
                print(f"To {(output_folder / fn).as_posix()}")
                ftp_client.put(Path(fn), (output_folder / fn).as_posix())
        else:
            print(f"From: {Path(fns)}")
            print(f"To {(output_folder / fns).as_posix()}")
            ftp_client.put(Path(fns), (output_folder / fns).as_posix())
    result = ftp_client.close()
    return result

# def _transfer_files_to_cluster_uid(cluster: SSHClient, list_of_files: list, output_folder: Path):
#     """Transfer files to a cluster
#     """

#     # Transfer input files to the cluster
#     ftp_client = cluster.open_sftp()
#     for uid, fns in list_of_files:
#         path_output = output_folder #/ Path(f"job_{uid}")
#         if isinstance(fns, list):
#             for fn in fns:
#                 print(f"From: {fn}")
#                 print(f"To {os.fspath(path_output / Path(fn))}")
#                 ftp_client.put(fn, os.fspath(path_output / Path(fn)))
#         else:
#             print(f"From: {fns}")
#             print(f"To {os.fspath(path_output / Path(fns))}")
#             ftp_client.put(fns, os.fspath(path_output / Path(fns)))
#     result = ftp_client.close()
#     return result

def _run_setup_cluster(
    cluster: SSHClient,
    tasks: dict,
    tasks_file: str,
    output_folder: os.PathLike,
    remote_folder: os.PathLike,
    run_dict: dict
) -> list:
    """Set up cluster

    Parameters
    ----------
    cluster: SSHClient
        Cluster object.
    tasks: dict
        Tasks dictionary.
    output_folder: str
        General output folder of the cervino simulation.
    work_folder: str
        Work folder of the cervino simulation.
    run_dict: dict
        Cervino run dictionary.

    Returns
    -------

    """

    # Create submission script for each simulation
    # and get Matterhorn input files list
    solver_input_files_dict = _run_setup_local(tasks, output_folder)

    # Obtain model files
    model_files_dict = dict()
    tasks._iterate_over_tasks(model_files_dict, _get_model_files_from_task)

    # Obtain additional input files
    additional_input_files_dict = dict()
    tasks._iterate_over_tasks(additional_input_files_dict, _get_additional_input_files_from_task)

    # Obtain UNIQUE model files
    model_files_unique = set()
    for uid, fns in model_files_dict.items():
        for fn in fns:
            model_files_unique.add(fn)
    print(f"The unique model files are:")
    print(model_files_unique)

    #
    model_files_unique_folders = set()
    for fn in model_files_unique:
        model_files_unique_folders.add(fn.parent)
    print(f"The unique model files folders are:")
    print(model_files_unique_folders)

    # Obtain UNIQUE additional input files
    additional_input_files_unique = set()
    for uid, fns in additional_input_files_dict.items():
        for fn in fns:
            additional_input_files_unique.add(fn)
    print(f"The unique additional files are:")
    print(additional_input_files_unique)

    # Submission script
    submission_files_dict = dict()
    for uid, fn_solver_input in solver_input_files_dict.items():
        fn_submission_script = Path(f"submission_{uid}.sh")
        jobname = f"{uid}"
        ncores = 24
        nranks = int(prod([len(n) for n in tasks.get_tasks_dict()[uid]['domain']['geometry']['decomposition'].values()]))
        _ = _create_submission_script_lsf(fn_solver_input, fn_submission_script, jobname, ncores, nranks)
        submission_files_dict[uid] = fn_submission_script

    # Create folder structure
    # Create general output folder
    # path_remote = PurePosixPath(remote_folder)
    # dir_euler_sub = (Path(dir_remote) / fn).parent
    stdin, stdout, stderr = cluster.exec_command(f"test -d {remote_folder.as_posix()} && echo 1 || echo 0")
    result = int(stdout.readline())
    if not result:
        print(f"Directory '{remote_folder.as_posix()}' does not exist. I will create it for you.")
        result = cluster.exec_command(f"mkdir {remote_folder.as_posix()}")

    # Create task-specific output folder
    # By default, SSH can only open 10 sessions per network connection.
    # See `man sshd_config`, under `MaxSessions`.
    max_ssh = 10
    counter = 0
    for uid in tasks.get_tasks_dict().keys():
        if counter >= max_ssh:
            sleep(1.0)
            counter = 0
        path_output = remote_folder / output_folder / f"job_{uid}"
        result = cluster.exec_command(f"mkdir -p {path_output.as_posix()}")
        counter += 1

    # Create folders for model parameter files
    # By default, SSH can only open 10 sessions per network connection
    # See `man sshd_config`, under `MaxSessions`
    max_ssh = 10
    counter = 0
    for folder in model_files_unique_folders:
        if counter >= max_ssh:
            sleep(1.0)
            counter = 0
        path_output = remote_folder / folder
        result = cluster.exec_command(f"mkdir -p {path_output.as_posix()}")
        counter += 1
        
    # Copy files to cluster
    # Transfer Matterhorn input files
    result = _transfer_files_to_cluster(cluster, [fn for fn in solver_input_files_dict.values()], remote_folder) # / Path(output_folder))
    # Transfer model files
    if run_dict['transfer_domain_files']:
        result = _transfer_files_to_cluster(cluster, model_files_unique, remote_folder)
    else:
        print("WARNING: The domain files will not be transferred to the cluster!")

    # Transfer additional files: DFT coefficients, ...
    result = _transfer_files_to_cluster(cluster, additional_input_files_unique, remote_folder)
    # Transfer submission scripts
    result = _transfer_files_to_cluster(cluster, [fn for fn in submission_files_dict.values()], remote_folder) # / Path(output_folder))
    # Transfer tasks files (.pkl, .toml, ...)
    result = _transfer_files_to_cluster(cluster, [tasks_file], remote_folder) # / Path(output_folder))

    return solver_input_files_dict, additional_input_files_dict, submission_files_dict

def _run_post_cluster(tasks: dict, output_folder: os.PathLike, work_folder: os.PathLike) -> dict:
    """Post simulation

    Parameters
    ----------
    tasks: dict
        Tasks of one job.
    output_folder: str
        General output folder of the cervino simulation.
    work_folder: str
        Work folder for the simulation on the cluster.

    Returns
    -------
    solver_output_files: dict
        Dictionary with UIDs as keys and input file name as values.
    """
    output_files_hdf5 = dict()
    # Create the input files for the selected solver
    for uid in tasks.get_tasks_dict().keys():
        fn = work_folder / output_folder / f"job_{uid}"/ f"output_{uid}.h5"
        output_files_hdf5[uid] = fn

    return output_files_hdf5

def _run_compute_forward_simulation_cluster(
    cervino: Cervino,
    run_dict: dict,
    tasks: dict,
    timing: bool=False
) -> int:
    """Run forward simulations with Matterhorn

    Parameters
    ----------
    run_dict: dict
        Run dictionary.
    tasks: dict
        Tasks of this pipe.

    Returns
    -------
    result: list
        List of tuple: standard output and standard error.
    output_file: str
        Output file in HDF5 format.
    """
    
    task_type = 'compute_forward_simulation'

    print('Cluster run!')

    if 'transfer_domain_files' not in run_dict:
        run_dict['transfer_domain_files'] = True

    if 'setup_only' not in run_dict:
        run_dict['setup_only'] = False

    # Connect to the cluster (ssh)
    cluster = SSHClient()
    cluster.load_system_host_keys()
    hostname = sites_cluster_valid[cervino.workflow.dict[task_type][1]] #.site]
    cluster.connect(hostname=hostname, username=cervino.user)

    # Setup
    # Get file names
    solver_input_files_dict, additional_input_files_dict, submission_files_dict = _run_setup_cluster(
        cluster,
        tasks,
        cervino._job_tasks_files[task_type],
        PurePosixPath(cervino._output_folder),
        PurePosixPath(cervino._remote_folder),
        run_dict
    )

    cervino._input_files[task_type] = solver_input_files_dict

    uid_job_id_dict = dict()
    # Submit the job(s)
    if not run_dict['setup_only']:
        for uid, fn in submission_files_dict.items():
            print(f"cd {PurePosixPath(cervino._remote_folder).as_posix()} && bash {fn.as_posix()}")
            result = cluster.exec_command(f"cd {PurePosixPath(cervino._remote_folder).as_posix()} && bash {fn.as_posix()}")
            # Extract the job id
            jobid = re.findall(r'(?<=<)[\d]+(?=>)', result[1].readlines()[0])[0]
            print(f"The jobid is {jobid}")
            uid_job_id_dict[uid] = jobid
    else:
        print("WARNING: 'setup_only' is set to 'True'.")
        print("WARNING: No job has been submitted to the cluster.")
    
    cervino._job_id_dict[task_type] = uid_job_id_dict

    # Convert output to hdf5
    cervino._output_files[task_type] = _run_post_cluster(
        tasks,
        PurePosixPath(cervino._output_folder),
        PurePosixPath(cervino._remote_folder)
    )

    # Close connection to cluster
    cluster.close()
    print('Cluster done!')

    # return solver_input_files_dict, additional_input_files_dict, submission_files_dict
    return 0

def _fetch_from_cluster(cervino: Cervino, job: str) -> int:
    """Retrieve the output of a Cervino job from a cluster.

    Parameters
    ----------
    cervino: Cervino
        Cervino object.
    job: str
        Job type.
    """

    # Connect to the cluster (ssh)
    cluster = SSHClient()
    cluster.load_system_host_keys()
    hostname = sites_cluster_valid[cervino.workflow.dict[job][1]] #.site]
    cluster.connect(hostname=hostname, username=cervino.user)

    # Transfer output files from the cluster
    ftp_client = cluster.open_sftp()
    fns_uid = cervino._output_files[job]
    for uid, fn in fns_uid.items():
        remote_file = PurePosixPath(cervino._remote_folder) / fn
        print(f"The remote file is `{remote_file.as_posix()}`")
        local_file = Path(fn).relative_to(cervino._remote_folder)
        print(f"The local file is `{local_file}`")
        ftp_client.get(remote_file.as_posix(), str(local_file))
    
    # Close connection
    ftp_client.close()
    cluster.close()

    return 0

def _convert_output_cluster(cervino: Cervino, job: str, i: int) -> dict:
    """Convert to the cervino format the output created
    by a cervino simulation running on a cluster.

    Parameters
    ----------
    cervino: Cervino
        Cervino object.
    job: str
        Job type.
    """

    # Connect to the cluster (ssh)
    cluster = SSHClient()
    cluster.load_system_host_keys()
    hostname = sites_cluster_valid[cervino.workflow.dict[job][1]] #.site]
    cluster.connect(hostname=hostname, username=cervino.user)

    module_convert = f"cervino.solvers.convert_output_cluster"
    job_pkl = cervino._job_tasks_files[job]

    command = f"""conda activate cervino_cluster; \
bsub -N -B -J {i:04d}_conversion -o {i:04d}_conversion.out -e {i:04d}_conversion.err \
-cwd {PurePosixPath(cervino._remote_folder).as_posix()} \
python -m {module_convert} --job {job} --job_pkl {job_pkl.as_posix()} \
--output_folder {PurePosixPath(cervino._output_folder).as_posix()} \
--work_folder {PurePosixPath(cervino._remote_folder).as_posix()}"""

    print(command)

    result = cluster.exec_command(command)

    print(result[1].readlines())
    pprint(result[2].readlines())

    # Close the connection
    cluster.close()

    return result

def _check_status_cluster(cervino: Cervino, job: str) -> dict:
    """Check the status of submitted jobs
    """

    # Connect to the cluster (ssh)
    cluster = SSHClient()
    cluster.load_system_host_keys()
    hostname = sites_cluster_valid[cervino.workflow.dict[job][1]] #.site]
    cluster.connect(hostname=hostname, username=cervino.user)

    uid_job_id_list = cervino._job_id_dict[job]
    
    uid_jobstatus_dict = dict()
    for uid, jobid in uid_job_id_list.items():
        # Check the status of the job
        result = cluster.exec_command(f"bjobs {jobid}")

        bjobs = result[1].readlines()
        bjobs_header = bjobs[0].split()
        bjobs_values = bjobs[1].split()
        # Compose SUBMIT_TIME value
        submit_time = f"{bjobs_values[-3]} {bjobs_values[-2]} {bjobs_values[-1]}"
        del bjobs_values[-3:]
        bjobs_values.append(submit_time)
        # Check if the EXEC_HOST field is empty
        if len(bjobs_values) < 8:
            bjobs_values.insert(5, '')
        bjobs_dict = OrderedDict()
        for i in range(8):
            bjobs_dict[bjobs_header[i]] = bjobs_values[i]
        uid_jobstatus_dict[uid] = bjobs_dict

    # Close the connection
    cluster.close()

    # print(f"The status of the job {jobid} is {bjobs_dict['STAT']}.")

    return uid_jobstatus_dict
