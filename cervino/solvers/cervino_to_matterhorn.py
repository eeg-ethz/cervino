import os
from pathlib import Path, PurePosixPath, PureWindowsPath
import numpy as np
import toml
import xarray as xr

def _convert_from_cervino(task_dict: dict, output_folder: os.PathLike) -> tuple:
    """Convert a Cervino input to a Matterhorn input file

    Parameters
    ----------
    task_dict: dict
        Task dictionary.
    output_folder: Path
        General output folder of the cervino simulation.

    Returns
    -------
    uid: str
        UID.
    file_name: str
        Input file name.
    """

    if isinstance(output_folder, PurePosixPath):
        print("***** PurePosixPath")
        PathFunc = PurePosixPath
        newline = '\n'
    else:
        print("***** PureWindowsPath")
        PathFunc = PureWindowsPath
        newline = '\r\n'

    output_folder_uid = output_folder / f"job_{task_dict['general']['uid']}"

    # Matterhorn input file name
    # The input file is created on the local system only
    # That's why it is wrapped in a Path object
    matterhorn_file = Path(f"{task_dict['general']['uid']}.sim")

    is3D = True if task_dict['domain']['dimension'] == 3 else False

    #######################################################
    # Grid section
    #######################################################
    grid_dict = {
        'origin': task_dict['domain']['geometry']['origin'],
        'number_of_cells': task_dict['domain']['geometry']['number-of-cells'],
        'cell_size': task_dict['domain']['geometry']['cell-size']
    }

    #######################################################
    # Decomposition section
    #######################################################
    decomposition_dict = {
        'number_of_nodes': [len(n) for n in task_dict['domain']['geometry']['decomposition'].values()],
        'number_of_cells_per_node_x': task_dict['domain']['geometry']['decomposition'][0],
        'number_of_cells_per_node_y': task_dict['domain']['geometry']['decomposition'][1],
        'number_of_cells_per_node_z': task_dict['domain']['geometry']['decomposition'][2],
    }

    #######################################################
    # Time section
    #######################################################
    time_dict = {
        'number_of_timesteps': [task_dict['time']['number-of-timesteps']],
        'timestep_size': [task_dict['time']['time-step-in-seconds']],
    }

    #######################################################
    # Model section
    #######################################################
    model_dict = {
        'type': 'acoustic',
        'format': 'bin',
        'filename_prefix': str(PathFunc(Path(task_dict['domain']['model']['filename'].replace('.h5',''))))
    }

    #######################################################
    # Simulation section
    #######################################################
    ndim = 3 if is3D else 2
    simulation_dict = {
        'type': f"acoustic_{ndim}d",
        'order_time_operator': [2],
        'spatial_operator': [task_dict['physics']['spatial-operator']],
    }
    for bd in task_dict['physics']['boundaries']:
        if bd['type'] == 'pml':
            simulation_dict.update(
                {
                    'freesurface': bd['freesurface'],
                    'bc_type': 'pml',
                    'pml_width_in_gridpoints': [bd['pml-width-in-gridpoints']],
                    'pml_power': [bd['pml-power']],
                    'pml_frequency': [bd['pml-frequency']],
                    'pml_damping_vel': [bd['pml-damping-vel']]
                }
            )

    #######################################################
    # Input section
    #######################################################
    source_conversion = {
        'q': 'isotropic_stress_source',
        'fx': 'x_source',
        'fy': 'y_source',
        'fz': 'z_source',
    }

    input_dict = {
        'type': [source_conversion[task_dict['source']['point-source']['spatial-type']]],
        'location': task_dict['source']['point-source']['location'],
        'wavelet': task_dict['source']['point-source']['source-time-function']['wavelet'],
        'spread': 'trilinear',
        'scale_factor': [task_dict['source']['point-source']['spatial-weights']],
    }

    # User-defined wavelet
    if input_dict['wavelet'] == 'user':
        input_dict.update(
            {
                'filename_prefix': [task_dict['source']['point-source']['source-time-function']['filename-prefix']],
                'format': [task_dict['source']['point-source']['source-time-function']['format']]
            }
        )
    # Standard wavelets implememented in Matterhorn
    else:
        input_dict.update(
            {
                'central_frequency': [task_dict['source']['point-source']['source-time-function']['center-frequency']],
                'shift': [task_dict['source']['point-source']['source-time-function']['time-shift-in-cycles']]
            }
        )

    #######################################################
    # Output section
    #######################################################

    if is3D:
        field_conversion = {
            'vx': 'V100X',
            'vy': 'V010Y',
            'vz': 'V001Z',
            'sxx': 'S000XX',
        }
    else:
        field_conversion = {
            'vx': 'V10X',
            'vz': 'V01Z',
            'sxx': 'S00XX',
        }

    # Gathers
    output_gathers_list_of_dict = []
    if 'gather' in task_dict['output']:
        for rec in task_dict['output']['gather']['receivers'].values():
            for field in rec['fields']:
                if 'receivers-filename' in rec:
                    output_gather_dict = {
                        'type': 'shot_gather', #[task_dict['output']['gather']['type']],
                        'format': [task_dict['output']['gather']['format']],
                        'start_timestep': [task_dict['output']['gather']['start-timestep']],
                        'end_timestep': [task_dict['output']['gather']['end-timestep']],
                        'timestep_increment': [task_dict['output']['gather']['timestep-increment']],
                        'attribute': [field_conversion[field]],
                        'filename_prefix': [str(output_folder_uid / f"{rec['filename-prefix']}_{field}")],
                        'receivers_filename': [rec['receivers-filename']],
                    }
                else:
                    output_gather_dict = {
                        'type': 'shot_gather', #[task_dict['output']['gather']['type']],
                        'format': [task_dict['output']['gather']['format']],
                        'start_timestep': [task_dict['output']['gather']['start-timestep']],
                        'end_timestep': [task_dict['output']['gather']['end-timestep']],
                        'timestep_increment': [task_dict['output']['gather']['timestep-increment']],
                        'attribute': [field_conversion[field]],
                        'filename_prefix': [str(output_folder_uid / f"{rec['filename-prefix']}_{field}")],
                        'receiver_origin': rec['receiver-origin'],
                        'number_of_receivers': [rec['number-of-receivers']],
                        'receiver_increment': rec['receiver-increment'],
                    }

                output_gathers_list_of_dict.append(output_gather_dict)

    # Time slice
    output_slice_time_list_of_dict = []
    if 'slice_time' in task_dict['output']:
        for sl in task_dict['output']['slice_time']['slices'].values():
            for field in sl['fields']:
                output_slice_time_dict = {
                    'type': [task_dict['output']['slice_time']['type']],
                    'format': [task_dict['output']['slice_time']['format']],
                    'start_timestep': [task_dict['output']['slice_time']['start-timestep']],
                    'end_timestep': [task_dict['output']['slice_time']['end-timestep']],
                    'timestep_increment': [task_dict['output']['slice_time']['timestep-increment']],
                    'attribute': [field_conversion[field]],
                    'filename_prefix': [str(output_folder_uid / f"{sl['filename-prefix']}_{field}")],
                    'axis': sl['axis'],
                    'slice_index': [sl['slice-index']],
                }

                output_slice_time_list_of_dict.append(output_slice_time_dict)

    # DFT slice
    output_slice_frequency_list_of_dict = []
    if 'slice_dft' in task_dict['output']:
        for sl in task_dict['output']['slice_dft']['slices'].values():
            for field in sl['fields']:
                output_slice_frequency_dict = {
                    'type': [task_dict['output']['slice_dft']['type']],
                    'format': [task_dict['output']['slice_dft']['format']],
                    'attribute': [field_conversion[field]],
                    'filename_prefix': [str(output_folder_uid / f"{sl['filename-prefix']}_{field}")],
                    'axis': sl['axis'],
                    'slice_index': [sl['slice-index']],
                    'dft_frequencies': task_dict['output']['slice_dft']['dft-frequencies'],
                    'dft_wavelet_coefficients': task_dict['output']['slice_dft']['dft-wavelet-coefficients']
                }

                output_slice_frequency_list_of_dict.append(output_slice_frequency_dict)

    # DFT volume
    output_volume_frequency_list_of_dict = []
    if 'volume_dft' in task_dict['output']:
        for sl in task_dict['output']['volume_dft']['volumes'].values():
            for field in sl['fields']:
                output_volume_frequency_dict = {
                    'type': [task_dict['output']['volume_dft']['type']],
                    'format': [task_dict['output']['volume_dft']['format']],
                    'attribute': [field_conversion[field]],
                    'filename_prefix': [str(output_folder_uid / f"{sl['filename-prefix']}_{field}")],
                    'dft_frequencies': task_dict['output']['volume_dft']['dft-frequencies'],
                    'dft_wavelet_coefficients': task_dict['output']['volume_dft']['dft-wavelet-coefficients']
                }

                output_volume_frequency_list_of_dict.append(output_volume_frequency_dict)

    # Concatenate output dictionaries
    output_dict = (
        output_gathers_list_of_dict +
        output_slice_time_list_of_dict +
        output_slice_frequency_list_of_dict +
        output_volume_frequency_list_of_dict
    )

    #######################################################
    # Put everything together
    #######################################################
    sections = {
        'grid': [grid_dict,],
        'decomposition': [decomposition_dict,],
        'time': [time_dict,],
        'model': [model_dict,],
        'simulation': [simulation_dict,],
        'input': [input_dict,],
        'output': output_dict
    }

    # for i, j in sections.items():
    #     print('Section: {}'.format(i))
    #     print(j)
    #     print('---------------------')

    # Write sections to file
    with open(matterhorn_file, 'w', newline=newline) as f:
        # Iterate over sections
        for section_key, section_val in sections.items():
            # Check if section_val is a list (for output and input)
            if isinstance(section_val, list):
                # Iterate over the element of the list
                for item in section_val:
                    # Open section
                    f.write('<{}>\n'.format(section_key))
                    # Iterate over dictionary items
                    for key, val in item.items():
                        if not isinstance(val, str):
                            # Convert the values to a nice looking string
                            val = ' '.join(str(v) for v in val)
                        # Key val pair
                        f.write('{} {}\n'.format(key, val))
                    # Close section
                    f.write('</{}>\n\n'.format(section_key))

    return (task_dict['general']['uid'], matterhorn_file)

# def _get_source_frequencies(stf_file: str=None, freqs: list=None, df: float=None) -> list:
#     """ Transform the source-time function
#     to the frequency domain and extract the complex values
#     corresponding to the required frequencies.

#     Parameters
#     ----------
#     str_file: str
#         Source-time function file.
#     freqs: list
#         List of frequencies.
#     df: float
#         Sampling in frequency.

#     Returns
#     -------
#     list
#         List of complex values.
#     """

#     stf = np.fromfile(stf_file, dtype=np.float32)
#     # print(f"stf size and type are: {stf.size} {stf.dtype}")
#     stf_freq = np.fft.fft(stf)
#     # print(f"stf_freq size and type are: {stf_freq.size} {stf_freq.dtype}")
#     stf_freq_list = [stf_freq[int(freq/df)] for freq in freqs]
#     # temp = [int(freq/df) for freq in freqs]
#     # print(temp)

#     return stf_freq_list

def _create_matterhorn_binary_from_hdf5(
    fn_h5: os.PathLike,
    required_models: list,
    verbose: bool = False,
):
    """Create Matterhorn-read binary files from cervino HDF5 file.

    Parameters
    ----------
    fn_h5: str
        File name of the netCDF file.
    required_models: list
        List of requird model parameters, 'VP', 'RHO', ...

    Returns
    -------
    dataset: xarray.Dataset
        Xarray dataset containing the model.
    """

    # Load the HDF5 file
    dataset = xr.load_dataset(fn_h5, engine="h5netcdf")

    # is3D = 'y' in ds.coords

    dims = ("y", "x", "z") if 'y' in dataset.coords else ("x", "z")

    for m in required_models:
        dataset[m].transpose(*dims).data.astype("float32").tofile(
            os.fspath(fn_h5).replace(".h5", f"_{m}.bin")
        )
