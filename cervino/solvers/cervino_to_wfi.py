import numpy as np
import toml

def _wfi(toml_file: str=None) -> str:
    """ Convert a TOML input file to a WFI input file.

    """

    wfi_input_file = toml_file.replace('.toml', '.sim')
    wfi_data_file = toml_file.replace('.toml', '.dat')

    td = toml.load(toml_file)

    # Free surface
    for bd in td['physics']['boundaries']:
        if bd['type'] == 'pml':
            fs = 1 if bd['freesurface'] == 'True' else 0

    # Create input file
    input_dict = {
        'GEOFILE': f"{wfi_data_file}\n",
        'DATAFILE': f"{wfi_data_file}\n",
        'MODELFILE': f"{td['domain']['model']['filename'].replace('.h5','.mod')}\n\n",
        'WFIMODE': f"{1}\n",
        'FREESURFACE': f"{fs}\n",
        'INV': f"{0}\n\n",
        'NPML': f"{10}\n",
        'PML_EXP': f"{2.0}\n",
        'PML_SIGMA': f"{10.0}\n\n",
        'GFOUTPUT': f"{1}\n",
        'GFONLY': f"{1}\n"
    }

    with open(wfi_input_file, 'w') as f:
        for key, val in input_dict.items():
            f.write(f"{key} {val}")

    # Create data file

    # SPOINT - Sources
    counter = 0
    spoint_list = []
    if len(td['source']['point-source']) > 1:
        raise f"For now this works when only one source location is defined!"
    loc = td['source']['point-source'][0]['location']
    spoint_list.append(f"SPOINT {counter:4d} {loc[0]:f} {loc[2]:f}\n")
    ns = len(spoint_list)
    counter +=1

    # SPOINT - Receivers
    for rec in td['output']['shot_gather']['receivers']:
        for ir in range(rec['number-of-receivers']):
            x = rec['receiver-origin'][0] + rec['receiver-increment'][0] * ir
            z = rec['receiver-origin'][2] + rec['receiver-increment'][2] * ir
            spoint_list.append(f"SPOINT {counter:4d} {x:f} {z:f}\n")
            counter +=1

    # FREQ
    freq_list = []
    freqs = td['output']['slice_dft']['dft-frequencies']
    for i, freq in enumerate(freqs):
        freq_list.append(f"FREQ {i:4d} {freq:f}\n")

    # SOURCE
    stf_file = td['source']['point-source'][0]['source-time-function']['filename-prefix']
    stf_format = td['source']['point-source'][0]['source-time-function']['format']
    df = 1 / (td['time']['number-of-timesteps'] * td['time']['time-step-in-seconds'])
    stf_freq = _get_source_frequencies(f"{stf_file}.{stf_format}", td['output']['slice_dft']['dft-frequencies'], df)
    source_list = []
    for i, freq in enumerate(stf_freq):
        source_list.append(f"SOURCE {i:4d} {freq.real:e} {freq.imag:e}\n")

    # RECEIVER
    recfac = np.complex(1.0, 0.0)
    receiver_list = []
    for i, freq in enumerate(freqs):
        for ir in range(rec['number-of-receivers']):
            counter = i * rec['number-of-receivers'] + ir
            receiver_list.append(f"RECEIVER {counter:4d} {recfac.real:e} {recfac.imag:e}\n")

    # DATA
    data_list = []
    for i, freq in enumerate(freqs):
        for j in range(ns):
            for ir in range(rec['number-of-receivers']):
                counter = i * rec['number-of-receivers'] + ir
                data_list.append(f"DATA {j:4d} {ns+ir:4d} {i:4d} {j*len(freqs)+i:4d} {counter:4d} 0 1.0 0.0 1.0\n")

    # Write everything to file
    with open(wfi_data_file, 'w') as f:
        f.writelines(spoint_list)
        f.writelines(freq_list)
        f.writelines(source_list)
        f.writelines(receiver_list)
        f.writelines(data_list)
    
    return wfi_input_file
