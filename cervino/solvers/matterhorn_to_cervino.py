import os
from collections import OrderedDict
import numpy as np
import toml
import h5py
import segyio
import xarray as xr
from pathlib import Path
from pprint import pprint
from typing import List, Tuple

from ..core.core import Tasks
from ..core.definitions import axis_valid, outputs_valid

def _convert_output_to_xarray(fn_h5: os.PathLike, task: dict, output_folder: os.PathLike, work_folder: os.PathLike) -> str:
    """Gather and group the outputs of Matterhorn
    and convert them to an xarray.Dataset.

    Parameters
    ----------
    fn_h5: string
        HDF5 output file.
    outputs: dict
        Dictionary of output files.
    output_folder: str
        General output folder of the cervino simulation.
    work_folder: str
        Work folder of the cervino simulation.
    """
    
    output_file_template = {
        'gather': "{}_{}.{}",
        'slice_time': "{}_{}_*.{}",
        'slice_dft': "{}_{}_*.{}",
        'volume_dft': "{}_{}_{}.{}",
    }

    output_files = Tasks._get_output_files_from_task(task, output_folder)
    
    # Initialize the Dataset
    ds = xr.Dataset(
        coords={
            "time": Tasks._get_coords_time(task),
            }
        )

    for out_id, (out_type, out_fn) in output_files.items():
        # Obtain common info
        fmt = task['output'][out_type]['format']
        fields = task['output'][out_type][outputs_valid[out_type]][out_id]['fields']
        nfields = len(fields)
        
        # Specific output type
        if out_type == 'gather':
            time_gather = Tasks._get_coords_time_gather(task)
            traces, recx, recy, recz = Tasks._get_coords_traces_xyz(task['output']['gather']['receivers'][out_id])
            data = np.empty((nfields, len(time_gather), len(traces)), dtype=np.float32)
            for i, field in enumerate(fields):
                fn_gather = work_folder / f"{output_file_template[out_type].format(str(out_fn), field, fmt)}"
                with segyio.su.open(fn_gather, ignore_geometry=True, endian='little') as sufile:
                    data[i, ...] = sufile.trace.raw[:].T
            da = xr.DataArray(
                data=data,
                coords={
                    "fields_gather": fields,
                    "time_gather": time_gather,
                    "traces": traces,
                    'recx': ('traces', recx),
                    'recy': ('traces', recy),
                    'recz': ('traces', recz),
                },
                dims=['fields_gather', 'time_gather', 'traces'],
                name=out_id
            )
        elif out_type == 'slice_time':
            time_slice = Tasks._get_coords_time_slice(task)
            x, y, z = Tasks._get_coords_xyz(task)
            # Remove the proper axis
            axis = OrderedDict(
                {
                    'x': x,
                    'y': y,
                    'z': z,
                }
            )
            _ = axis.pop(task['output']['slice_time']['slices'][out_id]['axis'])
            shape = [len(a) for a in axis.values()]
            data = np.empty((nfields, len(time_slice), *shape), dtype=np.float32)
            for i, field in enumerate(fields):
                # Get and sort filenames of slices
                fn_slices = sorted(work_folder.glob(output_file_template[out_type].format(str(out_fn), field, fmt)))
                # Read slices into ndarray
                for j, fn_item in enumerate(fn_slices):
                    with segyio.su.open(fn_item, ignore_geometry=True, endian='little') as sl:
                        data[i, j, ...] = sl.trace.raw[:]
            coords = {
                "fields_time_slice": fields,
                "time_slice": time_slice,
            }
            coords.update(axis)
            da = xr.DataArray(
                data=data,
                coords=coords,
                dims=['fields_time_slice', 'time_slice', *axis.keys()],
                name=out_id
            )
        if out_type == 'slice_dft':
            x, y, z = Tasks._get_coords_xyz(task)
            freq = Tasks._get_coords_freq(task)
            # Remove the proper axis
            axis_slice = task['output']['slice_dft']['slices'][out_id]['axis']
            axis = OrderedDict(
                {
                    'z': z,
                    'y': y,
                    'x': x,
                }
            )
            _ = axis.pop(axis_slice)
            shape = [len(a) for a in axis.values()]
            data = np.empty((nfields, len(freq), *shape), dtype=np.complex64)
            for i, field in enumerate(fields):
                # Name of the file. The file extension is SU but it is a simple binary file.
                fn_slices = os.fspath(list(work_folder.glob(output_file_template[out_type].format(str(out_fn), field, fmt)))[0])
                data[i, ...] = np.fromfile(fn_slices, dtype=np.complex64).reshape(len(freq), *shape)
            coords = {
                "fields_dft_slice": fields,
                "freq_dft": freq
            }
            coords.update(axis)
            da = xr.DataArray(
                data=data,
                coords=coords,
                dims=['fields_dft_slice', 'freq_dft', *axis.keys()],
                name=out_id
            )
        if out_type == 'volume_dft':
            x, y, z = Tasks._get_coords_xyz(task)
            freq = Tasks._get_coords_freq(task)
            axis = OrderedDict(
                {
                    'z': z,
                    'y': y,
                    'x': x,
                }
            )
            shape = [len(a) for a in axis.values()]
            data = np.empty((nfields, len(freq), *shape), dtype=np.complex64)
            decomposition = task['domain']['geometry']['decomposition']
            for i, field in enumerate(fields):
                # Name of the file. The file extension is SU but it is a simple binary file.
                for iz, sizez in enumerate(decomposition[2]):
                    for iy, sizey in enumerate(decomposition[1]):
                        for ix, sizex in enumerate(decomposition[0]):
                            nodeid = int(ix + iy * 2 + iz * 2 * 2)
                #             print(f"The nodeid is '{nodeid}'")
                            fn = work_folder / output_file_template[out_type].format(str(out_fn), field, nodeid, fmt)
                            temp = np.fromfile(os.fspath(fn), dtype=np.complex64).reshape((len(freq), sizez, sizey, sizex))
                            startx = sum(decomposition[0][:ix])
                            endx = startx + sizex
                            starty = sum(decomposition[1][:iy])
                            endy = starty + sizey
                            startz = sum(decomposition[2][:iz])
                            endz = startz + sizez
                            print(f"{nodeid} = {startx}:{endx}, {starty}:{endy}, {startz}:{endz}")
                            data[i, :, startz:endz, starty:endy, startx:endx] = temp
            coords = {
                "fields_dft_slice": fields,
                "freq_dft": freq
            }
            coords.update(axis)
            da = xr.DataArray(
                data=data,
                coords=coords,
                dims=['fields_dft_slice', 'freq_dft', *axis.keys()],
                name=out_id
            )
        # Add DataArray to Dataset
        ds[out_id] = da
    # Write HDF5 file
    ds.to_netcdf(fn_h5, format="NETCDF4", engine="h5netcdf")
    return fn_h5

# def _get_input_files_from_task(self, task) -> set:

#     input_list_for_one_task = set()
#     # Solver input file
#     # self.input_list.update(self._solver_input_file)
#     # User defined source time functions
#     # for key, val in task['source'].items():
#     #     for source in task['source'][key]:
#     #         input_list.update(f"{source[sources_valid[key]]['filename-prefix']}.bin")
#     for key, source in task['source'].items():
#         input_list_for_one_task.update([f"{source[sources_valid[key]]['filename-prefix']}.bin"])
#     # Model files
#     filename_prefix = Path(task['domain']['model']['filename'])
#     # filename_prefix = os.fspath(filename_prefix.relative_to(Path.cwd())).replace('.h5', '')
#     filename_prefix = os.fspath(filename_prefix).replace('.h5', '')

#     for model in task['domain']['model']['required-models']:
#         input_list_for_one_task.update([f"{filename_prefix}_{model}.bin"])
#     # HPC cluster files
#     # self.input_list.append()

#     return input_list_for_one_task

def _create_hdf5_from_matterhorn_binary(
    filename_prefix: os.PathLike,
    fn_h5: os.PathLike,
    required_models: list,
    n: Tuple[int],
    d: Tuple[float],
    origin: Tuple[float] = (0, 0, 0),
    verbose: bool = False,
):
    """Create HDF5 domain file from Matterhorn-ready binary files.

    Parameters
    ----------
    filename_prefix : str
        Filename prefix of the files containing the model parameters.
    fn_h5 : str
        Filename of the cervino HDF5 file.
    required_models: list
        List of requird model parameters, 'VP', 'RHO', ...
    n : tuple of int
        Dimensions in (x, y, z). Set y to 1 for a 2D medium.
    d : tuple of floats
        Spatial discretization in (x, y, z).
    origin : tuple of floats
        Origin in (x, y, z).

    Returns
    -------
    dataset: xarray.Dataset
        Xarray dataset containing the model.
    """

    is3D = True if n[1] > 1 else False
    if is3D: print(f"3D domain")
    else: print(f"2D domain")

    # Initialize the dataset with some coordinate arrays
    coords = {
        "x": np.arange(n[0]) * d[0] + origin[0],
        "z": np.arange(n[2]) * d[2] + origin[2],
    }
    if is3D:
        coords["y"] = np.arange(n[1]) * d[1] + origin[1]
    dataset = xr.Dataset(
        coords=coords
    )
    # Copy the data into xarray's data structure
    if is3D:
        shape = (n[1], n[0], n[2])
        labels = ("y", "x", "z")
    else:
        shape = (n[0], n[2])
        labels = ("x", "z")
    for model in required_models:
        fn = f"{os.fspath(filename_prefix)}_{model}.bin"
        data = np.fromfile(fn, dtype=np.float32).reshape(shape) #.T # WHY WAS I TRANSPOSING THIS IN 2D? SEE ALSO BELOW
        dataset[model] = (labels, data)

    # Write data to disk in netCDF format
    result = dataset.to_netcdf(fn_h5, format="NETCDF4", engine="h5netcdf")

    return result

def _matterhorn_get_data(fn_data: str=None) -> str:
    pass

