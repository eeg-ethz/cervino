from functools import partial
import numpy as np
import toml
import pickle

import xarray as xr

from ..core.definitions import fields_salvus_valid, fields_salvus_dft_valid

from salvus.flow import api
from salvus.flow import simple_config as config

from salvus.toolbox import toolbox

def _convert_from_cervino(task_dict: dict) -> tuple:
    """Convert a Cervino input to a Salvus input file

    Parameters
    ----------
    task_dict: dict
        Task dictionary

    Returns
    -------
    uid: str
        uid
    file_name: str
        Input file name
    """

    # 2D or 3D
    dims = task_dict['domain']['dimension']

    #######################################################
    # Input section
    #######################################################

    # Important
    # Remember that, for 2D simulations with Salvus,
    # the axis are x and y (not x and z)!

    spatial_type_func = {
        'q': getattr(config.source.cartesian, f"ScalarPoint{dims}D"),
        'fx': getattr(config.source.cartesian, f"ScalarGradientPoint{dims}D"),
        'fz': getattr(config.source.cartesian, f"ScalarGradientPoint{dims}D"),
        'fy': getattr(config.source.cartesian, f"ScalarGradientPoint{dims}D"),
    }

    spatial_type_components = {
        'q': {'f': 1.0},
        'fx': {'fx': 1.0, 'fy': 0.0} if dims == 2 else {'fx': 1.0, 'fy': 0.0, 'fz': 0.0},
        'fz': {'fx': 0.0, 'fy': 1.0} if dims == 2 else {'fx': 0.0, 'fy': 1.0, 'fz': 0.0},
        'fy': {'fx': 0.0, 'fy': 0.0, 'fz': 1.0},
    }

    center_frequency = task_dict['source']['point-source']['source-time-function']['center-frequency']
    wavelet = config.stf.Ricker(
        center_frequency=center_frequency,
        time_shift_in_seconds=task_dict['source']['point-source']['source-time-function']['time-shift-in-seconds']
    )

    x = task_dict['source']['point-source']['location'][0]

    max_y = (task_dict['domain']['geometry']['number-of-cells'][2] - 1)
    max_y *= task_dict['domain']['geometry']['cell-size'][2]
    max_y += task_dict['domain']['geometry']['origin'][2]
    y = max_y - task_dict['source']['point-source']['location'][2]

    source = spatial_type_func[task_dict['source']['point-source']['spatial-type']](
        source_time_function=wavelet, x=x, y=y, **spatial_type_components[task_dict['source']['point-source']['spatial-type']]
    )

    #######################################################
    # Model section
    #######################################################
    filename = task_dict['domain']['model']['filename']
    model = xr.load_dataset(filename)

    if dims == 2:
        # Rename the `z` axis to `y` and then reverse it
        model = model.rename(
            {
                "z": "y",
            }
        )
        model.coords['y'].data = model.coords['y'].data[::-1]
    else:
        raise NotImplementedError('Implement the model section for 3D simulations.')

    #######################################################
    # Physics section
    #######################################################
    num_absorbing_layers = 0
    absorbing_side_sets = []
    for bd in task_dict['physics']['boundaries']:
        if bd['type'] == 'pml':
            absorbing_side_sets = ["x0", "x1", "y0", "y1"]
            num_absorbing_layers = bd['pml-width-in-gridpoints']

    dirichlet_yes = False
    for bd in task_dict['physics']['boundaries']:
        if bd['freesurface'] == 'True':
            try:
                absorbing_side_sets.remove("y1")
            except:
                pass
            # Define Dirichlet boundary at top surface (pressure-free).
            dirichlet = config.boundary.HomogeneousDirichlet(side_sets=["y1"])
            dirichlet_yes = True

    elements_per_wavelength = 1.5
    mesh_frequency = 2.0 * wavelet.center_frequency
    min_vel_field = 'VS' if 'VS' in model else 'VP'
    width_in_meters = float(model[min_vel_field].values.min()) / mesh_frequency / elements_per_wavelength * (num_absorbing_layers)
    print(f"The width in meters is '{width_in_meters}'")

    # Define coupled Clayton-Enqist / Kosloff
    # damping boundaries at the the remaining side-s`ets.
    absorbing = config.boundary.Absorbing(
        width_in_meters=width_in_meters, #513.333 #200.0, #685.333,
        side_sets=absorbing_side_sets,
        taper_amplitude=wavelet.center_frequency,
    )

    #######################################################
    # Mesh section
    #######################################################
    salvus_mesh = toolbox.mesh_from_xarray(
        model_order=4,
        data=model,
        slowest_velocity='VS' if 'VS' in model else 'VP',
        maximum_frequency=mesh_frequency,
        elements_per_wavelength=elements_per_wavelength,
        absorbing_boundaries=(absorbing_side_sets, num_absorbing_layers),
    )

    #######################################################
    # Output section
    #######################################################

    # Gathers
    try:
        gather_bool = True
        receivers_list = []
        for i, rec in enumerate(task_dict['output']['gather']['receivers'].values()):
            nr = rec['number-of-receivers']
            fields = set()
            for field in rec['fields']:
                fields.add(fields_salvus_valid[field])
            receivers = []
            for ir in range(rec['number-of-receivers']):
                x = ir * rec['receiver-increment'][0] + rec['receiver-origin'][0]
                y = max_y - (ir * rec['receiver-increment'][2] + rec['receiver-origin'][2])
                receivers.append(
                        config.receiver.cartesian.Point2D(
                            x=x,
                            y=y,
                            station_code=f"xx{i}_{ir}",
                            fields=list(fields),
                        )
                    )
            receivers_list.append(receivers)
            #
            fn_gather = rec['filename-prefix']
    except:
        gather_bool = False

    # Time slices
    try:
        slice_time_bool = True
        for i, sl in enumerate(task_dict['output']['slice_time']['slices'].values()):
            slice_time_fields = set()
            for field in sl['fields']:
                slice_time_fields.add(fields_salvus_valid[field])
            #
            fn_slice_time = sl['filename-prefix']
    except:
        slice_time_bool = False

    # DFT slices
    try:
        slice_dft_bool = True
        for i, sl in enumerate(task_dict['output']['slice_dft']['slices'].values()):
            slice_dft_fields = set()
            for field in sl['fields']:
                slice_dft_fields.add(fields_salvus_dft_valid[field])
            #
            fn_slice_dft = sl['filename-prefix']
    except:
        slice_dft_bool = False

    #######################################################
    # Simulation section
    #######################################################
    s = config.simulation.Waveform(
        mesh=salvus_mesh, sources=source, receivers=receivers
    )
    #for receivers in receivers_list:
    #    s.add_receivers(receivers, cpu_count=4)

    # Add the boundaries to the parameter file
    if dirichlet_yes:
        s.physics.wave_equation.boundaries = [absorbing, dirichlet]
    else:
        s.physics.wave_equation.boundaries = [absorbing]

    # Initialize point receiver output
    if gather_bool:
        s.output.point_data.filename = f"{fn_gather}.h5"
        s.output.point_data.format = "hdf5"

    # Initialize wavefield output: time domain
    if slice_time_bool:
        s.output.volume_data.filename = f"{fn_slice_time}.h5"
        s.output.volume_data.format = "hdf5"
        s.output.volume_data.fields = list(slice_time_fields) #["phi", "gradient-of-phi"]
        s.output.volume_data.sampling_interval_in_time_steps = task_dict['output']['slice_time']['timestep-increment']

    # Initialize wavefield output: frequency domain
    if slice_dft_bool:
        s.output.frequency_domain.filename = f"{fn_slice_dft}.h5"
        s.output.frequency_domain.format = "hdf5"
        s.output.frequency_domain.fields = list(slice_dft_fields) #["phi"]
        s.output.frequency_domain.frequencies = [float(f) for f in task_dict['output']['slice_dft']['dft-frequencies']]

    # s.output.frequency_domain.filename = "frequency_domain.h5"
    # s.output.frequency_domain.format = "hdf5"
    # s.output.frequency_domain.fields = ["phi"]
    # s.output.frequency_domain.frequencies = [10.0, 20.0]

    #######################################################
    # Time section
    #######################################################
    s.physics.wave_equation.start_time_in_seconds = 0.0
    s.physics.wave_equation.time_step_in_seconds = task_dict['time']['time-step-in-seconds']
    end_time = (task_dict['time']['number-of-timesteps'] - 1) * task_dict['time']['time-step-in-seconds']
    s.physics.wave_equation.end_time_in_seconds = end_time

    # Dump s to file
    output_file = f"salvus_{task_dict['general']['uid']}.pkl"
    with open(output_file, 'wb') as f:
        pickle.dump(s.get_dictionary(), f)

    return (task_dict['general']['uid'], s)

def _get_source_frequencies(stf_file: str=None, freqs: list=None, df: float=None) -> list:
    """ Transform the source-time function
    to the frequency domain and extract the complex values
    corresponding to the required frequencies.

    Parameters
    ----------
    str_file: str
        Source-time function file.
    freqs: list
        List of frequencies.
    df: float
        Sampling in frequency.

    Returns
    -------
    list
        List of complex values.
    """

    stf = np.fromfile(stf_file, dtype=np.float32)
    # print(f"stf size and type are: {stf.size} {stf.dtype}")
    stf_freq = np.fft.fft(stf)
    # print(f"stf_freq size and type are: {stf_freq.size} {stf_freq.dtype}")
    stf_freq_list = [stf_freq[int(freq/df)] for freq in freqs]
    # temp = [int(freq/df) for freq in freqs]
    # print(temp)

    return stf_freq_list
