from collections import OrderedDict
import numpy as np
import toml
import h5py
import segyio
import xarray as xr
from pathlib import Path
from pprint import pprint

from ..core.core import Tasks
from ..core.definitions import (
        axis_valid, outputs_valid,
        fields_salvus_valid, fields_salvus_dft_valid
        )

from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator

def _convert_output_to_xarray(fn_h5: str, task: dict, output_folder: str) -> dict:
    """Gather and group the outputs of Salvus
    and convert them to an xarray.Dataset.

    Parameters
    ----------
    fn_h5: string
        HDF5 output file.
    outputs: dict
        Dictionary of output files.
    output_folder: str
        General output folder of the cervino simulation.
    """

    # 2D or 3D
    dims = task['domain']['dimension']

    output_file_template = {
        'gather': "{}.{}",
        'slice_time': "{}.{}",
        'slice_dft': "{}.{}",
    }

    components = {
        'phi': [0,],
        'phi_t': [0,],
        'gradient-of-phi': [i for i in range(dims)],
        'displacement': [i for i in range(dims)]
    }

    output_files = Tasks._get_output_files_from_task(task, output_folder)
    print(f"#####\nMessage from `{__name__}`\n#####")
    pprint(output_files)

    # Initialize the Dataset
    ds = xr.Dataset(
        coords={
            "time": Tasks._get_coords_time(task),
            }
        )

    for out_id, (out_type, out_fn) in output_files.items():
        # Obtain common info
        fmt = 'h5'
        fields_cervino = sorted(task['output'][out_type][outputs_valid[out_type]][out_id]['fields'])
        # Implement a check for the fields
        # because, in Salvus, "gradient-of-phi" always includes two or three fields
        if out_type == 'slice_dft':
            fields_salvus = sorted(list(set([fields_salvus_dft_valid[field] for field in fields_cervino])), reverse=True)
        else:
            fields_salvus = sorted(list(set([fields_salvus_valid[field] for field in fields_cervino])), reverse=True)
        nfields = 0
        if 'phi' in fields_salvus:
            nfields += 1
        if 'phi_t' in fields_salvus:
            nfields += 1
        if 'gradient-of-phi' in fields_salvus:
            nfields += dims

        print("Fields: cervino")
        print(fields_cervino)
        print("Fields: Salvus")
        print(fields_salvus)

        fn_salvus = output_file_template[out_type].format(out_fn, fmt)

        # Specific output type
        if out_type == 'gather':
            time_gather = Tasks._get_coords_time_gather(task)
            traces, recx, _, recz = Tasks._get_coords_traces_xyz(task['output']['gather']['receivers'][out_id])
            data = np.empty((nfields, len(time_gather), len(traces)), dtype=np.float32)

            with h5py.File(fn_salvus, 'r') as f:
                # From salvus_toolbox.get_shotgather
                # Is this OK for 3D too?
                receivers = f['receiver_ids_ACOUSTIC_point'][:]
                index_sorted = receivers.argsort()
                i = 0
                for field in fields_salvus:
                    for cmp in components[field]:
                        print(f"Reading {field} field and {cmp} component")
                        data[i, ...] = f[f"point/{field}"][:, cmp, :][index_sorted, :].T
                        i += 1
            da = xr.DataArray(
                data=data,
                coords={
                    "fields_gather": fields_cervino,
                    "time_gather": time_gather,
                    "traces": traces,
                    'recx': ('traces', recx),
                    'recz': ('traces', recz),
                },
                dims=['fields_gather', 'time_gather', 'traces'],
                name=out_id
            )
        elif out_type == 'slice_time':
            time_slice = Tasks._get_coords_time_slice(task)
            # For some reason, Salvus has an extra time step
            nt = len(time_slice) + 1
            time_slice = np.arange(nt) * 1e-4
            print(f"nt is {nt}")
            x, y, z = Tasks._get_coords_xyz(task)
            # Remove the proper axis
            axis = OrderedDict(
                {
                    'x': x,
                    'y': y,
                    'z': z,
                }
            )
            _ = axis.pop(task['output']['slice_time']['slices'][out_id]['axis'])
            shape = [len(a) for a in axis.values()]
            data = np.empty((nfields, nt, *shape), dtype=np.float32)
            print(f"The shape of data is:")
            print(data.shape)

            nxj = complex(0, len(x))
            nyj = complex(0, len(y))
            nzj = complex(0, len(z))
            x_min, x_max, dx = x.min(), x.max(), np.mean(np.diff(x))
            z_min, z_max, dz = z.min(), z.max(), np.mean(np.diff(z))
            grid_x, grid_z = np.mgrid[x_min:x_max:nxj, z_min:z_max:nzj]
            print(x_min, x_max, dx)
            print(z_min, z_max, dz)
            print(grid_x.shape)
            print(grid_z.shape)

            with h5py.File(fn_salvus, 'r') as f:
                # Is this OK for 3D too?
                coords = f['coordinates_ACOUSTIC'][:, :, :].reshape((-1, dims))
                i = 0
                for field in fields_salvus:
                    for cmp in components[field]:
                        print(f"Reading {field} field and {cmp} component")
                        das = f['volume'][field][:, :, cmp, :25].reshape((nt, -1))
                        print(das.shape)
                        #das = das.reshape((das.shape[0], -1))
                        print(coords.shape)
                        for it in range(nt):
                            print(f"\tInterpolating {it} out of {nt}")
                            # interpolator = NearestNDInterpolator(coords[:, :], das[it, :])
                            interpolator = LinearNDInterpolator(coords[:, :], das[it, :])
                            data[i, it, ...] = interpolator(grid_x, grid_z)
                        i += 1
            coords = {
                "fields_time_slice": fields_cervino,
                "time_slice": time_slice,
            }
            coords.update(axis)
            da = xr.DataArray(
                data=data,
                coords=coords,
                dims=['fields_time_slice', 'time_slice', *axis.keys()],
                name=out_id
            )
        elif out_type == 'slice_dft':
            freqs = Tasks._get_coords_freq(task)
            print(freqs)
            x, y, z = Tasks._get_coords_xyz(task)
            # Remove the proper axis
            axis = OrderedDict(
                {
                    'x': x,
                    'y': y,
                    'z': z,
                }
            )
            _ = axis.pop(task['output']['slice_dft']['slices'][out_id]['axis'])
            shape = [len(a) for a in axis.values()]
            data = np.empty((nfields, len(freqs), *shape), dtype=np.complex64)
            print(f"The shape of data is:")
            print(data.shape)

            nxj = complex(0, len(x))
            nyj = complex(0, len(y))
            nzj = complex(0, len(z))
            x_min, x_max, dx = x.min(), x.max(), np.mean(np.diff(x))
            z_min, z_max, dz = z.min(), z.max(), np.mean(np.diff(z))
            grid_x, grid_z = np.mgrid[x_min:x_max:nxj, z_min:z_max:nzj]
            print(x_min, x_max, dx)
            print(z_min, z_max, dz)
            print(grid_x.shape)
            print(grid_z.shape)

            with h5py.File(fn_salvus, 'r') as f:
                # Is this OK for 3D too?
                coords = f['coordinates_ACOUSTIC'][:, :, :].reshape((-1, dims))
                i = 0
                # Currently, these two loops are unnecessary because
                # the DFT can only be computed for the primary field
                # (phi for acoustic simulations, which has only one component)
                for field in fields_salvus:
                    for cmp in components[field]:
                        print(f"Reading {field} field and {cmp} component")
                        for ifreq, freq in enumerate(freqs):
                            omega = 2.0 * np.pi * freq
                            for ipart, part in enumerate(['real', 'img']):
                                print(f"\tInterpolating {part} part of {freq}")
                                field_name = f"FFT-{part}-{freq:.6f}-Hz"
                                print(field_name)
                                das = f['volume'][field_name][-1, :, cmp, :25].reshape((-1))
                                print(das.shape)
                                #das = das.reshape((das.shape[0], -1))
                                print(coords.shape)
                                # interpolator = NearestNDInterpolator(coords[:, :], das[it, :])
                                interpolator = LinearNDInterpolator(coords[:, :], das[:])
                                # Compute time derivative: Multiply by \omega i
                                # Be careful: the real part is nedeed to
                                # compute the imaginary part of the derivative
                                if part == 'real':
                                    data[i, ifreq, ...].imag = omega * interpolator(grid_x, grid_z)
                                else:
                                    data[i, ifreq, ...].real = omega * interpolator(grid_x, grid_z)
                        i += 1
            coords = {
                "fields_dft_slice": fields_cervino,
                "freq_dft": freqs,
            }
            coords.update(axis)
            da = xr.DataArray(
                data=data,
                coords=coords,
                dims=['fields_dft_slice', 'freq_dft', *axis.keys()],
                name=out_id
            )

        # Add DataArray to Dataset
        ds[out_id] = da
    # Write HDF5 file
    ds.to_netcdf(fn_h5, format="NETCDF4", engine="h5netcdf")
    return fn_h5

