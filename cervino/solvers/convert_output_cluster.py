import os
from pathlib import Path
from argparse import ArgumentParser
import pickle
from cervino.solvers.matterhorn_to_cervino import _convert_output_to_xarray

parser = ArgumentParser()
parser.add_argument('--job', type=str, help='Job type.')
parser.add_argument('--job_pkl', type=str, help='Pickle containing the tasks of the job.')
parser.add_argument('--output_folder', type=str, help='Output folder.')
parser.add_argument('--work_folder', type=str, help='Work folder.')

def convert(job: str, fn_tasks: str, output_folder: os.PathLike, work_folder: os.PathLike):
    """Gather and group the outputs of Matterhorn
    and convert them to an xarray.Dataset.

    Parameters
    ----------
    job: string
        Job type.
    fn_tasks: dict
        Pickle containing the tasks dictionary.
    output_folder: str
        General output folder of the cervino simulation.
    work_folder: str
        Work folder of the cervino simulation.
    """

    # Load tasks dictionary
    with open(fn_tasks, 'rb') as f:
        tasks = pickle.load(f)

    output_files_hdf5 = dict()
    # Convert the output files for the selected job
    for uid, task in tasks.items():
        fn = work_folder / output_folder / f"job_{uid}" / f"output_{uid}.h5"
        _ = _convert_output_to_xarray(fn, task, output_folder, work_folder)
        output_files_hdf5[uid] = fn
    
    return output_files_hdf5

if __name__ == '__main__':

    args = parser.parse_args()

    # Command line arguments
    job = args.job
    fn_tasks = args.job_pkl
    output_folder = args.output_folder
    work_folder = args.work_folder

    output_files_hdf5 = convert(job, Path(fn_tasks), Path(output_folder), Path(work_folder))

    print('Done!')