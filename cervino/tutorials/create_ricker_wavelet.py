r"""
01. Create a Ricker wavelet
===========================
This tutorial explains how to create a Ricker wavelet.
"""

import numpy as np
import matplotlib.pyplot as plt
from cervino import *

###############################################################################
# Create the time axis

time_step_in_seconds = 4e-4
start_time_in_seconds = 0.0
end_time_in_seconds = 2.0

t = np.arange(start_time_in_seconds, end_time_in_seconds, time_step_in_seconds)

###############################################################################
# Use a Ricker wavelet as source time function

center_frequency = 30.0
stf = RickerWavelet(t, fc=center_frequency, delay=1.5, delay_type='cycles', create_binary=False)

###############################################################################
# Define a point source

name = "source0"
location = (4000.0, 0.0, 20.0)
spatial_type = "fz"
scale = 1.

source0 = PointSource(loc=location, stf=stf, spatial_type=spatial_type)

###############################################################################
# Plot the source time function

plt.plot(t, source0.stf.wavelet)
_ = plt.title('Ricker wavelet')
_ = plt.xlabel('Time [s]')