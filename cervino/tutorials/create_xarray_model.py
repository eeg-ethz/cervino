# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

r"""
02. Create an xarray model from SEGY
====================================
"""

# +
# This tutorial explains how to create an xarray model from existing SEGY files.
# Here, we will download the Marmousi model, read the SEGY files, and create an `xarray.Dataset` from them.
# -

import os
from pathlib import Path
from urllib.request import urlretrieve
import tarfile

import numpy as np
import xarray as xr
import segyio

# +
# Url of the archive
fn_url = 'http://trip.rice.edu/downloads/ieee.tar.gz'

# Files and directories
dirname = Path('marmousi_original_model')
fn_archive = Path('marmousi_archive.tar.gz')
fn_h5 = dirname / 'marmousi_original.h5'
fn_prefix = dirname / 'marmousi_original'

# Download, extract, and rename the directory
result = urlretrieve(fn_url, fn_archive)
tarfile.open(fn_archive, mode='r:gz').extractall()
Path('ieee').rename(dirname)

# Model list
model_list = {'VP': (dirname / 'velocity.segy', 'm/s'),
              'RHO': (dirname / 'density.segy', 'kg/m^3')}

# Fix the header - Wrong format
# This step is specific for these SEGY files only
for file in model_list.values():
    with segyio.open(file[0], mode = 'r+', ignore_geometry=True) as f:
        f.bin[segyio.su.format] = 5

# Domain parameters
number_of_cells = [2301, 1, 751]
cell_size = [4., 4., 4.]
origin = [0.0, 0.0, 0.0]

# Initialize the dataset with some coordinate arrays
dataset = xr.Dataset(
    coords={
        "x": np.arange(number_of_cells[0]) * cell_size[0] + origin[0],
        "z": np.arange(number_of_cells[2]) * cell_size[2] + origin[2],
    }
)

# Copy the data into xarray's data structure
for model, (file, units)  in model_list.items():
    data = segyio.open(file, ignore_geometry=True).trace.raw[:]
    dataset[model] = (("z", "x"), data)
# -

_ = dataset['VP'].plot(aspect="auto", figsize=(10, 3), yincrease=False)

# +
# Write data to disk in netCDF format
#dataset.to_netcdf(fn_h5, format='NETCDF4')

# Create binary files for Matterhorn
dataset['VP'].transpose('x', 'z').data.astype('float32').tofile(f"{fn_prefix}_{'vp.bin'}")
dataset['RHO'].transpose('x', 'z').data.astype('float32').tofile(f"{fn_prefix}_{'rho.bin'}")
