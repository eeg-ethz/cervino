import os
import sys
import numpy
import time
from threading import Lock
from textwrap import dedent

import dash
import dash_html_components as html
import dash_core_components as dcc

# import plotly.graph_objs as go

import dash_daq as daq
from dash.dependencies import Input, Output, State

from cervino.core.definitions import (
    tasks_valid,
    solvers_valid,
    sites_valid
)

DEMO = False

# class to represent all controls
class Control:
    def __init__(
        self, new_ctrl_id, new_ctrl_name, new_component_type, new_component_attr
    ):
        self.ctrl_id = new_ctrl_id  # id for callbacks
        self.ctrl_name = new_ctrl_name  # name for label
        self.component_type = new_component_type  # dash-daq component type
        self.component_attr = new_component_attr  # component attributes

    # creates a new control box with defined component, id, and name
    def create_ctrl_div(self, pwrOff):
        # create dash-daq components
        try:
            component_obj = getattr(daq, self.component_type)
        except AttributeError:
            component_obj = getattr(dcc, self.component_type)

        # disable if power is off
        # self.component_attr["disabled"] = pwrOff

        component = component_obj(**self.component_attr)

        # generate html code
        new_control = html.Div(
            [
                html.Div(className="control-name", children=[self.ctrl_name]),
                html.Div(className="control-component", children=[component]),
            ],
            id=self.ctrl_id,
            className="control-wrapper"
        )
        return new_control

    # gets whether we look for "value", "on", etc.
    def val_string(self):
        if "value" in self.component_attr:
            return "value"
        elif "on" in self.component_attr:
            return "on"

    # changes value ('on' or 'value', etc.)
    def update_value(self, new_value):
        self.component_attr[self.val_string()] = new_value

# lock for modifying information about spectrometer
spec_lock = Lock()
# lock for communicating with spectrometer
comm_lock = Lock()

app = dash.Dash(__name__)
server = app.server
app.title = "Cervino builder"

colors = {
    "background": "#bbbbbb",
    "primary": "#efefef",
    "secondary": "#efefef",
    "tertiary": "#dfdfdf",
    "grid-colour": "#cccccc",
    "accent": "#2222ff",
}

controls = []
##############################
# General
##############################
controls_general = []
task_type = Control(
    "general-task-type",
    "Task type",
    "Dropdown",
    {
        "id": "general-task-type-input",
        "className": "control__dropdowns",
        "options": [{'label': i, 'value': i} for i in tasks_valid],
        "value": list(tasks_valid.keys())[0],
    },
)
controls_general.append(task_type)

solver = Control(
    "general-solver",
    "Solver",
    "Dropdown",
    {
        "id": "general-solver-input",
        "className": "control__dropdowns",
        "options": [{'label': i, 'value': i} for i in solvers_valid],
        "value": list(solvers_valid.keys())[0],
    },
)
controls_general.append(solver)

site = Control(
    "general-site",
    "Site",
    "Dropdown",
    {
        "id": "general-site-input",
        "className": "control__dropdowns",
        "options": [{'label': i, 'value': i} for i in sites_valid],
        "value": list(sites_valid.keys())[0],
    },
)
controls_general.append(site)

user = Control(
    "general-user",
    "User",
    "Input",
    {
        "id": "general-user-input",
        "className": "control__dropdowns",
        "placeholder": "Enter your username",
        "type": "text",
        "value": "",
    },
)
controls_general.append(user)

uid = Control(
    "general-uid",
    "Uid",
    "Input",
    {
        "id": "general-uid-input",
        "className": "control__dropdowns",
        "placeholder": "Uid of the simulation",
        "type": "text",
        "value": "",
    },
)
controls_general.append(uid)

desc = Control(
    "general-desc",
    "Description",
    "Textarea",
    {
        "id": "general-desc-input",
        "className": "control__dropdowns",
        "placeholder": "Description of the simulation (optional)",
        "value": "",
    },
)
controls_general.append(desc)

##############################
# Domain
##############################
controls_domain = []

dimension = Control(
    "domain-dimension",
    "Dimension",
    "RadioItems",
    {
        "id": "domain-dimension-input",
        "className": "control__dropdowns",
        "options": [{'label': f"{i}D", 'value': i} for i in [2, 3]],
        "value": 2,
        "labelStyle": {'display': 'inline-block'},
    },
)
controls_domain.append(dimension)

# ge = Control(
#     "domain-dimension",
#     "Dimension",
#     "RadioItems",
#     {
#         "id": "domain-dimension-input",
#         "className": "control__dropdowns",
#         "options": [{'label': f"{i}D", 'value': i} for i in [2, 3]],
#         "value": 2,
#         "labelStyle": {'display': 'inline-block'},
#     },
# )
# controls_domain.append(dimension)


controls = controls_general

base_intro = """This app was created to act as an interface for an Ocean Optics \
            spectrometer. Use controlling elements to control various \
            properties of the instrument; the integration time, the number of \
            scans to average over, the strobe and strobe period, and the \
            light source.
"""

extend_intro = """This app was created to act as an interface for an Ocean Optics 
spectrometer. Use controlling elements to control various 
properties of the instrument; the integration time, the number of 
scans to average over, the strobe and strobe period, and the
light source.
Clicking "Update" after putting in the desired settings will 
result in your parameter settings being sent to the device. A status message 
will appear below the button indicating which commands, if any, 
were unsuccessful; below the unsuccessful commands, a list of
successful commands can be found.
           
The dial labelled "Light intensity" will affect the current 
selected light source, if any. The switch labelled autoscale 
plot will change the axis limits of the plot to fit all of the 
data. Please note that the animations and speed of the graph will 
improve if this autoscale is turned off, and that it will not be 
possible to zoom in on any portion of the plot if it is turned 
on.
"""

page_layout = [
    html.Div(
        [
            html.Div(
                [
                    html.Img(
                        src=app.get_asset_url("matterhorn_logo.png"), className="logo"
                    ),
                    html.Div(html.H2("General section"), className="control"),
                    html.Div(
                        id="controls-general",
                        title="Parameters of the general section.",
                        children=[ctrl.create_ctrl_div(True) for ctrl in controls_general],
                    ),
                    html.Div(html.Label("Domain section"), className="control"),
                    html.Div(
                        id="controls-domain",
                        title="Parameters of the domain section.",
                        children=[ctrl.create_ctrl_div(True) for ctrl in controls_domain],
                    ),
                    html.Div(
                        [
                            html.Button(
                                "update",
                                id="submit-button",
                                n_clicks=0,
                                n_clicks_timestamp=0,
                            )
                        ],
                        title="Click to send all of the control values to the spectrometer.",
                        className="control",
                    ),
                    html.Div(
                        id="submit-status",
                        title="Contains information about the success or failure of your \
                commands.",
                        children=[""],
                    ),
                ],
                className="two-thirds column left__section",
            ),
            html.Div(
                [
                    html.Div(
                        id="graph-container",
                        children=[
                            html.Div(
                                children=[
                                    html.H6(
                                        id="graph-title", children=["Ocean Optics"]
                                    ),
                                    html.Div(
                                        id="power-button-container",
                                        title="Turn the power on to begin viewing the data and controlling \
        the spectrometer.",
                                        children=[
                                            daq.PowerButton(
                                                id="power-button",
                                                size=50,
                                                color=colors["accent"],
                                                on=True,
                                            )
                                        ],
                                    ),
                                    dcc.Markdown(
                                        dedent(base_intro), id="graph-title-intro"
                                    ),
                                    html.Button(
                                        "Learn More", id="learn-more-btn", n_clicks=0
                                    ),
                                    dcc.Graph(
                                        id="spec-readings",
                                        animate=True,
                                        figure=dict(
                                            data=[],
                                            layout=dict(
                                                height=600,
                                                paper_bgcolor="rgba(0,0,0,0)",
                                                plot_bgcolor="rgba(0,0,0,0)",
                                            ),
                                        ),
                                    ),
                                    dcc.Interval(
                                        id="spec-reading-interval",
                                        interval=2
                                        * 1000,  # change from 1 sec to 2 seconds
                                        n_intervals=0,
                                    ),
                                ]
                            )
                        ],
                    )
                ],
                className="one-third column right__section",
            ),
        ]
    )
]

app.layout = html.Div(id="main", children=page_layout)


############################
# Callbacks
############################


@app.callback(
    [Output("graph-title-intro", "children"), Output("learn-more-btn", "children")],
    [Input("learn-more-btn", "n_clicks")],
)
def display_info_box(btn_click):
    if (btn_click % 2) == 1:
        return dedent(extend_intro), "Close"
    else:
        return dedent(base_intro), "Learn More"


# disable/enable the update button depending on whether options have changed
@app.callback(
    Output("submit-button", "style"),
    [Input(ctrl.component_attr["id"], ctrl.val_string()) for ctrl in controls]
    + [Input("submit-button", "n_clicks_timestamp")],
)
def update_button_disable_enable(*args):
    now = time.time() * 1000
    disabled = {
        "color": colors["accent"],
        "backgroundColor": colors["background"],
        "cursor": "not-allowed",
    }
    enabled = {
        "color": colors["background"],
        "backgroundColor": colors["accent"],
        "cursor": "pointer",
    }

    # if the button was recently clicked (less than a second ago), then
    # it's safe to say that the callback was triggered by the button; so
    # we have to "disable" it
    if int(now) - int(args[-1]) < 500 and int(args[-1]) > 0:
        return disabled
    else:
        return enabled


# spec model
@app.callback(Output("graph-title", "children"), [Input("power-button", "on")])
def update_spec_model(_):
    return "Ocean Optics %s" % 'ciao'


# disable/enable controls
# @app.callback(Output("controls", "children"), [Input("power-button", "on")])
# def disable_enable_controls(pwr_on):
#     return [ctrl.create_ctrl_div(not pwr_on) for ctrl in controls]


# keep light intensity from resetting, update the value,
# or disable in the event of no light sources
# @app.callback(
#     Output("light-intensity-knob-container", "children"),
#     [Input("light-intensity-knob", "value")],
#     state=[State("light-source-input", "value")] + [State("power-button", "on")],
# )
# def preserve_set_light_intensity(intensity, ls, pwr):
#     if ls != "" and ls is not None:
#         spec.send_light_intensity(ls, intensity)
#     disable = not (pwr and ls != "" and ls is not None)
#     return [
#         daq.Knob(
#             id="light-intensity-knob",
#             size=110,
#             color=colors["accent"],
#             scale={"interval": "1", "labelInterval": "1"},
#             disabled=disable,
#             value=intensity,
#         )
#     ]


# send user-selected options to spectrometer
@app.callback(
    Output("submit-status", "children"),
    [Input("submit-button", "n_clicks")],
    state=[State(ctrl.component_attr["id"], ctrl.val_string()) for ctrl in controls]
    + [State("power-button", "on")],
)
def update_spec_params(n_clicks, *args):
    # don't return anything if the device is off
    if not args[-1]:
        return [
            'Press the power button to the top-right of the app, then \
            press the "update" button above to apply your options to \
            the spectrometer.'
        ]

    # dictionary of commands; component id and associated value
    commands = {controls[i].component_attr["id"]: args[i] for i in range(len(controls))}

    failed, succeeded = ({}, {}) #spec.send_control_values(commands)

    summary = []

    if len(failed) > 0:
        summary.append(
            "The following parameters were not \
        successfully updated: "
        )
        summary.append(html.Br())
        summary.append(html.Br())

        for f in failed:
            # get the name as opposed to the id of each control
            # for readability
            [ctrlName] = [c.ctrl_name for c in controls if c.component_attr["id"] == f]
            summary.append(ctrlName.upper() + ": " + failed[f])
            summary.append(html.Br())

        summary.append(html.Br())
        summary.append(html.Hr())
        summary.append(html.Br())
        summary.append(html.Br())

    if len(succeeded) > 0:
        summary.append("The following parameters were successfully updated: ")
        summary.append(html.Br())
        summary.append(html.Br())

        for s in succeeded:
            [ctrlName] = [c.ctrl_name for c in controls if c.component_attr["id"] == s]
            summary.append(ctrlName.upper() + ": " + succeeded[s])
            summary.append(html.Br())

    return html.Div(summary)


# update the plot
# @app.callback(
#     Output("spec-readings", "figure"),
#     state=[State("power-button", "on"), State("autoscale-switch", "on")],
#     inputs=[Input("spec-reading-interval", "n_intervals")],
# )
# def update_plot(on, auto_range, _):
#     traces = []
#     wavelengths = []
#     intensities = []

#     x_axis = {
#         "title": "Wavelength (nm)",
#         "titlefont": {"family": "Helvetica, sans-serif", "color": colors["secondary"]},
#         "tickfont": {"color": colors["tertiary"]},
#         "dtick": 100,
#         "color": colors["secondary"],
#         "gridcolor": colors["grid-colour"],
#     }
#     y_axis = {
#         "title": "Intensity (AU)",
#         "titlefont": {"family": "Helvetica, sans-serif", "color": colors["secondary"]},
#         "tickfont": {"color": colors["tertiary"]},
#         "color": colors["secondary"],
#         "gridcolor": colors["grid-colour"],
#     }

#     if on:
#         spectrum = spec.get_spectrum()
#         wavelengths = spectrum[0]
#         intensities = spectrum[1]
#     else:
#         wavelengths = numpy.linspace(400, 900, 5000)
#         intensities = [0 for wl in wavelengths]

#     if on:
#         if auto_range:
#             x_axis["range"] = [min(wavelengths), max(wavelengths)]
#             y_axis["range"] = [min(intensities), max(intensities)]
#     traces.append(
#         go.Scatter(
#             x=wavelengths,
#             y=intensities,
#             name="Spectrometer readings",
#             mode="lines",
#             line={"width": 1, "color": colors["accent"]},
#         )
#     )

#     layout = go.Layout(
#         height=600,
#         font={"family": "Helvetica Neue, sans-serif", "size": 12},
#         margin=dict(l=40, r=40, t=40, b=40, pad=10),
#         titlefont={
#             "family": "Helvetica, sans-serif",
#             "color": colors["primary"],
#             "size": 26,
#         },
#         xaxis=x_axis,
#         yaxis=y_axis,
#         paper_bgcolor="rgba(0,0,0,0)",
#         plot_bgcolor="rgba(0,0,0,0)",
#     )

#     return {"data": traces, "layout": layout}


if __name__ == "__main__":
    app.run_server(debug=True)