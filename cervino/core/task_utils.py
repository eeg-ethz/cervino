from .definitions import solvers_valid

################################
# Dictionaries
################################
def get_general_dict(uid: str, task_type: str, solver: str, desc: str=None) -> dict:
    """Return a dictionary with the parameters of the general section.

    The dictionary can be used to create
    the general section of a task's dictionary.

    Parameters
    ----------
    uid: string
        Unique identifier for the task.
    task_type: string
        Type of task.
    solver: string
        'matterhorn', 'salvus', or 'wfi'.
    desc: string
        Short description of the simulation.

    Returns
    -------
    general_dict: dict
        General section of the task's dictionary.
    """

    if solver not in solvers_valid.keys():
        raise Exception('The {} solver is not a valid solver!'.format(solver))
    # Set the description
    if desc is None:
        desc = 'Write a short description for the simulation!'

    return {
        "user": None,
        "site": None,
        "uid": uid,
        "task_type": task_type,
        "solver": solver,
        "desc": desc
    }

def get_time_dict(ts: float, nt: int, t0: float=0.0) -> dict:
    """Return a dictionary with the parameters of the time section.

    The dictionary can be used to create
    the time section of a TOML input file.

    Parameters
    ----------
    ts: float
        Time step in seconds.
    nt: int
        Number of time steps.
    t0: float
        Start time in seconds.
    """
    
    return {
        "time-step-in-seconds": ts,
        "number-of-timesteps": nt,
        "start-time-in-seconds": t0
    }

def get_frequency_dict(freqs: list=None) -> dict:
    """Return a dictionary with the parameters of the frequency section.

    The dictionary can be used to create
    the frequency section of a TOML input file.

    Parameters
    ----------
    freqs: list of floats
        List of frequencies
    """

    # Set the frequency list
    if freqs is None:
        raise Exception('The list of frequencies is not defined')

    return {
        "frequencies": freqs
    }

def get_pml_dict(fs: bool=False, width: int=10, power: int=4, frequency: float=None, vel: float=None) -> dict:
    """Return a dictionary with the parameters of the PML region.

    The dictionary can be used to create
    the physics section of a TOML input file.

    Parameters
    ----------
    fs: bool
        Free surface
    width: int
        Width of the PML region in grid points
    power: int
        Parameter for the PML formulation
    frequency: float
        Central frequency of the source wavelet
    vel: float
        Velocity that will be greatly attenuated
    """

    # Set the frequency
    if frequency is None:
        raise Exception('The frequency is not defined')
    # Set the velocity
    if vel is None:
        raise Exception('The velocity is not defined')

    return {
        "type": 'pml',
        "freesurface": str(fs),
        "pml-width-in-gridpoints" : width,
        "pml-power": power,
        "pml-frequency": frequency,
        "pml-damping-vel": vel
    }

def get_physics_dict(order: int=4, boundaries: list=None) -> dict:
    """Return a dictionary with the parameters of the physics section.

    The dictionary can be used to create
    the physics section of a TOML input file.

    Parameters
    ----------
    order: int
        Order of accuracy of the spatial derivative
    boundaries: list
        Boundaries information. It is a list of dictionaries.
    """

    spatial_operator = 'taylor_{}'.format(order)

    # Set the frequency
    if boundaries is None:
        raise Exception('The boundaries are not defined')

    return {
        "spatial-operator": spatial_operator,
        "boundaries": boundaries
    }