# required_sections = {
#     'general': validate_general,
#     }

def validate_general():
    """Validate
    """
    print("This is not implemented yet!")

    pass

# [domain]
# dimension = 3
# polynomial-order = 4 # Only for Salvus

# [domain.geometry]
# origin = [ 0, 0, 0,]
# number-of-cells = [ 500, 1, 250,]
# cell-size = [ 2.0, 2.0, 2.0,]

# [domain.model]
# filename = "acoustic2d.h5"
# format = "hdf5"

# # What about using end-time-in-seconds instead of number-of-timesteps
# [time]
# time-step-in-seconds = 0.0001
# number-of-timesteps = 12000
# start-time-in-seconds = 0.0

# [physics]
# spatial-operator = "taylor_4"

# [[physics.boundaries]]
# type = "pml"
# freesurface = true
# pml-width-in-gridpoints = 10
# pml-power = 4
# pml-frequency = 30.0
# pml-damping-vel = 3000.0

# [source]

# [[source.point-source]]
# location = [ 400.0, 0.0, 20.0,]
# spatial-type = "fz"
# spatial-weights = 1.0

# [[source.point-source.source-time-function]]
# center-frequency = 30.0
# wavelet = "ricker"
# time-shift-in-seconds = 0.0

# [[source.point-source]]
# location = [ 300.0, 0.0, 10.0,]
# spatial-type = "fx"
# spatial-weights = 1.0

# [[source.point-source.source-time-function]]
# center-frequency = 25.0
# wavelet = "ricker"
# time-shift-in-seconds = 0.0

# [[source.boundary-source]]
# location = "locations.loc"
# spatial-type = "fz"

# [output]

# [output.gather]
# format = "su"
# start-timestep = 0
# end-timestep = 11999
# timestep-increment = 10

# [[output.gather.receivers]]
# receiver-origin = [ 0.0, 0.0, 20.0,]
# receiver-increment = [ 2.0, 0.0, 0.0,]
# number-of-receivers = 500
# attribute = "V01Z"
# filename-prefix = "vz_gather"

# [[output.gather.receivers]]
# receiver-origin = [ 0.0, 0.0, 20.0,]
# receiver-increment = [ 2.0, 0.0, 0.0,]
# number-of-receivers = 500
# attribute = "V010X"
# filename-prefix = "vx_gather"

# [output.snapshot]
# format = "su"
# start-timestep = 0
# end-timestep = 11999
# timestep-increment = 1000

# [[output.snapshot.slice]]
# axis = "y"
# slice-index = 0
# attribute = "V01Z"
# filename-prefix = "vz_slice"

# [output.dft]
# format = "bin"
# dft_frequencies = [5.0, 10.0, 15.0]

# [[output.dft.freq]]
# axis = "y"
# slice-index = 0
# attribute = "V01Z"
# filename-prefix = "dft_5"

# # [hpc]