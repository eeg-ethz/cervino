# Tasks of the same job MUST use the same solver
workflows_valid = {
    'forward': ['compute_forward_simulation',],
    'backward': ['compute_backward_simulation',],
    'inversion': [
        'compute_forward_simulation',
        'compute_residuals',
        'compute_backward_simulation',
        'compute_gradients',
        'compute_step_length',
        'update_model']
} #: Un due tre prova

workflow_valid_string = " ".join(workflows_valid.keys())

tasks_valid = {
    'compute_forward_simulation': '',
    'compute_backward_simulation': '',
    'compute_residuals': '',
    'compute_gradients': '',
    'compute_step_length': '',
    'update_model': ''
}

tasks_mh_valid = {
    'compute_forward_simulation': '',
    'compute_backward_simulation': '',
    'compute_residuals': '',
    'compute_gradients': '',
    'compute_step_length': '',
    'update_model': ''
}

solvers_valid = {
    'matterhorn': '',
    'salvus': '',
    'wfi': ''
}

outputs_valid = {
    'gather': 'receivers',
    'slice_time': 'slices',
    'slice_dft': 'slices',
    'volume_dft': 'volumes'
    }

sources_valid = {
    'point-source': 'source-time-function',
    }

source_spatial_type_valid = {
    'q': '',
    'fx': '',
    'fy': '',
    'fz': '',
}
source_spatial_type_valid_string = ", ".join(source_spatial_type_valid.keys())

fields_valid = {
    'vx': '',
    'vy': '',
    'vz': '',
    'pressure': '',
    'sxx': '',
}
fields_valid_string = ", ".join(fields_valid.keys())

# Before 13.03.2020
fields_salvus_valid = {
    'vx': "gradient-of-phi",
    'vy': "gradient-of-phi",
    'vz': "gradient-of-phi",
    'pressure': "phi_t",
    'sxx': "phi_t",
}

fields_salvus_dft_valid = {
    'sxx': "phi",
}

sites_cluster_valid = {
    'euler': 'euler.ethz.ch',
    'pizdaint': 'cluster',
}

# axis_valid = {
#     'x': (2, 1),
#     'y': (2, 0),
#     'z': (1, 0),
# }

axis_valid = {
    'x': ('z', 'y'),
    'y': ('z', 'x'),
    'z': ('y', 'x'),
}
