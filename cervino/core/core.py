import os
import sys
import socket
from pathlib import Path, PurePosixPath
import toml
import h5py
import segyio
import numpy as np
from collections import OrderedDict, namedtuple
import pickle
import importlib
from typing import Callable, Union, List, Set, Dict

from .cluster import _run_cluster, _check_status_cluster, _fetch_cluster
from .definitions import (
    workflows_valid,
    sites_cluster_valid,
    solvers_valid,
    sources_valid,
    outputs_valid
)

SolverSite = namedtuple('SolverSite', ['solver', 'site'])

class Workflow():
    """Code class which defines a specific workflow for a cervino's simulation.
    A workflow is composed of one or more jobs.
    Each job is composed of one or more tasks of the same type.

    Parameters
    ----------
    workflow_type: string
        Define the workflow: 'forward', 'backward, or 'inversion'.
    workflow_dict: dict
        Workflow dictionary.

    Attributes
    ----------
    jobs: OrderedDict
        Jobs that compose the workflow.
    """

    def __init__(self, workflow_type: str, workflow_dict: dict):
        self.type = workflow_type
        self.dict = workflow_dict
        self.jobs = OrderedDict.fromkeys(self.dict.keys(), Tasks())

    def __repr__(self):
        return 'The jobs are:\n' + '\n'.join(self.jobs)

    # def iterate_over_jobs(self, d: dict, func: Callable, verbose: bool=False):
    #     """Helper function that allows one to apply a function
    #     to all the tasks of all jobs.

    #     Parameters
    #     ----------
    #     d:
    #         Dictionary to update.
    #     func: function
    #         Function that will be applied to each task.
    #         The function needs to return a list.
    #     verbose: bool, optional
    #         Print more information.
    #     """

    #     if verbose: print(f"The type of out is '{type(d)}'")
    #     for job, tasks in self.jobs.items():
    #         if verbose: print(f"Iterating over '{job}' job.")
    #         for uid, task in tasks.get_tasks_dict().items():
    #             if verbose: print(f"Iterating over '{uid}' task.")
    #             d[job][uid] = func(task)

    # def iterate_over_jobs_and_tasks(self, out, func, verbose: bool=False):
    #     """Helper function that allows one to apply a function
    #     to all the tasks of all jobs.

    #     Parameters
    #     ----------
    #     out:
    #         List or set to update.
    #     func: function
    #         Function that will be applied to each task.
    #     verbose: bool, optional
    #         Print more information.
    #     """

    #     if verbose: print(f"The type of out is '{type(out)}'")
    #     for job, tasks in self.jobs.items():
    #         if verbose: print(f"Iterating over '{job}' job.")
    #         for uid, task in tasks.get_tasks_dict().items():
    #             if verbose: print(f"Iterating over '{uid}' task.")
    #             if isinstance(out, set):
    #                 out.update(func(task),)
    #             elif isinstance(out, list):
    #                 out.append(func(task),)

class Tasks():
    """Tasks that compose a job of the cervino's workflow.
    """

    def __init__(self):
        self._tasks_dict = None

    def get_tasks_dict(self):
        """Return task list.

        Returns
        -------
        task_list: list
            List of tasks.
        """

        return self._tasks_dict

    def get_tasks_uids(self):
        """Return a list of task uids.

        Returns
        -------
        uid_list: list
            List of task uids.
        """

        return self._tasks_dict.keys()

    def _iterate_over_tasks(self, out: Union[List, Set, Dict], func: Callable, verbose: bool=False):
        """Helper function that allows one to apply a function
        to all the tasks in a job.

        Parameters
        ----------
        out:
            List or set or dictionary to update.
        func: function
            Function that will be applied to each task.
        verbose: bool, optional
            Print more information.
        """

        if verbose: print(f"The type of out is '{type(out)}'")
        for uid, task in self.get_tasks_dict().items():
            if verbose: print(f"Iterating over '{uid}' task.")
            if isinstance(out, set):
                out.update(func(task),)
            elif isinstance(out, list):
                out.append(func(task),)
            elif isinstance(out, dict):
                k, v = func(task)
                out[k] = v

    def _attach_tasks_from_pickle(self, fn: os.PathLike):
        """Load a dictionary of existing tasks from a pickle file.

        Parameters
        ----------
        fn: str or Path
            File name of the pickle file containing the tasks.
        """

        with open(fn, 'rb') as f:
            self._tasks_dict = pickle.load(f)

    def _attach_tasks_from_dictionary(self, tasks_dict: dict):
        """Load a dictionary of existing tasks from a dictionary.

        Parameters
        ----------
        tasks_dict: dict
            Dictionary containing the tasks.
        """

        self._tasks_dict = tasks_dict

    def _attach_tasks_from_toml(self, fn: os.PathLike):
        """Load a dictionary of existing tasks from a TOML file.

        Parameters
        ----------
        fn: str or Path
            File name of the TOML file containing the tasks.
        """

        self._tasks_dict = toml.load(fn)

    @staticmethod
    def _get_output_files_from_task(task: dict, output_folder: os.PathLike) -> dict:
        """Get file names of output files
        from task dictionary.

        Parameters
        ----------
        task: dict
            Task dictionary.
        output_folder: str
            General output folder of the cervino simulation.

        Returns
        -------
        output_files: dict
            Dictionary with each key as the `id` of an output
            and each value as a tuple `(output_type, filename_prefix)`.
        """

        output_folder_uid = output_folder / f"job_{task['general']['uid']}"

        output_files = dict()

        for key, val in task['output'].items():
            for out_id, out_val in val[outputs_valid[key]].items():
                output_files[out_id] = (key, output_folder_uid / f"{out_val['filename-prefix']}")
                # print(output_folder_uid / f"{out_val['filename-prefix']}")
                # output_files.append((key, output['filename-prefix']))

        return output_files

    @staticmethod
    def _get_coords_time(task: dict) -> np.ndarray:
        """Get time axis as ndarray
        from task dictionary.

        Parameters
        ----------
        task: dict
            Task dictionary.

        Returns
        -------
        time: ndarray
            Time axis of the simulation.
        """

        nt = task['time']['number-of-timesteps']
        t0 = task['time']['start-time-in-seconds']
        dt = task['time']['time-step-in-seconds']

        return np.arange(nt) * dt + t0

    @staticmethod
    def _get_coords_xyz(task: dict) -> (np.ndarray, np.ndarray, np.ndarray):
        """Get time axis as ndarray
        from task dictionary.

        Parameters
        ----------
        task: dict
            Task dictionary.

        Returns
        -------
        x: ndarray
            x coordinates of domain.
        y: ndarray
            y coordinates of domain.
        z: ndarray
            z coordinates of domain.
        """

        origin = task['domain']['geometry']['origin']
        d = task['domain']['geometry']['cell-size']
        n = task['domain']['geometry']['number-of-cells']
        x, y, z = [np.arange(n[i]) * d[i] + origin[i] for i in range(3)]

        return x, y, z

    @staticmethod
    def _get_coords_time_gather(task: dict) -> dict:
        """Get time axis of a gather as ndarray
        from task dictionary.

        Parameters
        ----------
        task: dict
            Task dictionary.

        Returns
        -------
        time: ndarray
            Time axis of gathers.
        """

        t = Tasks._get_coords_time(task)

        it0 = task['output']['gather']['start-timestep']
        it1 = task['output']['gather']['end-timestep'] + 1
        idt = task['output']['gather']['timestep-increment']

        return t[it0:it1:idt]

    @staticmethod
    def _get_coords_traces_xyz(receiver: dict) -> (np.ndarray, np.ndarray, np.ndarray, np.ndarray):
        """Get time axis as ndarray
        from task dictionary.

        Parameters
        ----------
        receiver: dict
            Receiver dictionary.

        Returns
        -------
        traces: ndarray
            Traces.
        x: ndarray
            x coordinates of receivers.
        y: ndarray
            y coordinates of receivers.
        z: ndarray
            z coordinates of receivers.
        """

        if 'receivers-filename' in receiver:
            with open(receiver['receivers-filename'], 'r') as f:
                x, y, z = np.loadtxt(f, delimiter=" ", unpack=True, dtype=np.float32)
                # print(f"The shape of x is {x.shape}")
                traces = np.arange(x.shape[0], dtype=np.int)
        else:
            traces = np.arange(receiver['number-of-receivers'], dtype=np.int)
            d0 = receiver['receiver-origin']
            dr = receiver['receiver-increment']
            index = np.argwhere(np.array(dr) > 0.0)
            xyz = np.empty((3, len(traces)))
            for i in range(3):
                if i == index:
                    xyz[i, :] = np.arange(len(traces)) * dr[i] + d0[i]
                else:
                    xyz[i, :] = np.ones(len(traces)) * d0[i]
            x, y, z = xyz

        return traces, x, y, z

    @staticmethod
    def _get_coords_time_slice(task: dict) -> dict:
        """Get time axis of a time slice as ndarray
        from task dictionary.

        Parameters
        ----------
        task: dict
            Task dictionary.

        Returns
        -------
        time: ndarray
            Time axis of slices.
        """

        t = Tasks._get_coords_time(task)

        it0 = task['output']['slice_time']['start-timestep']
        it1 = task['output']['slice_time']['end-timestep'] + 1
        idt = task['output']['slice_time']['timestep-increment']

        return t[it0:it1:idt]

    @staticmethod
    def _get_coords_freq(task: dict) -> (np.ndarray):
        """Get freq axis as ndarray
        from task dictionary.

        Parameters
        ----------
        task: dict
            Task dictionary.

        Returns
        -------
        freq: ndarray
            Frequencies of DFTs.
        """
        if 'slice_dft' in task['output']:
            freq = np.array(task['output']['slice_dft']['dft-frequencies'], dtype=np.float64)
        elif 'volume_dft' in task['output']:
            freq = np.array(task['output']['volume_dft']['dft-frequencies'], dtype=np.float64)
        else:
            raise ValueError("Something is wrong here!")
        
        return freq

################################
# Cervino class
################################
class Cervino():
    """cervino's main class.

    General Cervino class which allows one to do most of the work.

    Parameters
    ----------
    workflow_type: string
        Define the workflow: 'forward', 'backward, or 'inversion'.
    workflow_dict: dict
        Workflow dictionary.
    name: string
        Short name to identify the workflow.
    user: string
        Username which will be used for authentication purposes.
    desc: string
        Extended description of the simulation.
    output_folder: str
        Output folder where the results will be stored.
    """

    def __init__(
        self, 
        workflow_type: str,
        workflow_dict: dict,
        name: str,
        user: str=None,
        desc: str=None,
        remote_folder: str=None,
        output_folder: str='output'
    ):
        """Constructor for the Cervino class.
        """

        if workflow_type not in workflows_valid.keys():
            raise ValueError(f"The '{workflow_type}' workflow is not a valid workflow.")
        else:
            self.workflow = Workflow(workflow_type, workflow_dict)
        self.name = name
        if user is None:
            if sys.platform == 'win32':
                self.user = os.getenv('username')
            else:
                self.user = os.getenv('USER')
        else:
            self.user = user

        self._desc = desc or 'You forgot to add a description!'
        
        # Validation flag
        self._is_validated = False

        # Output folder where all the output files will be created
        # More information about the output folder structure can be found
        # in the official documentation
        self._output_folder = Path(output_folder)

        # Remote folder where all the input files will be created
        # On a local site, this should normally be the current directory
        if remote_folder is None:
            self._remote_folder = Path().cwd()
        else:
            self._remote_folder = PurePosixPath(remote_folder)

        # Some important dictionaries below
        # File dictionaries
        self._input_files = OrderedDict.fromkeys(self.workflow.jobs.keys(), OrderedDict())
        self._output_files = OrderedDict.fromkeys(self.workflow.jobs.keys(), OrderedDict())
        self._job_tasks_files = OrderedDict.fromkeys(self.workflow.jobs.keys(), '')

        # Cluster attributes
        self._job_id_dict = OrderedDict.fromkeys(self.workflow.jobs.keys(), OrderedDict())
        self._job_status_dict = OrderedDict.fromkeys(self.workflow.jobs.keys(), OrderedDict())

    def summary(self, verbose_level: int=2, save: bool=False) -> list:
        """Describe the Cervino simulation.

        Returns
        -------
        summary: list
            Summary as a list of strings.
        """

        summary = []

        # Workflow
        m = f"The Cervino simulation will perform a '{self.workflow.type}' workflow."
        summary.append(m)

        # Jobs
        m = f"There are {len(self.workflow.jobs)} jobs in the workflow:"
        summary.append(m)
        for i, job in enumerate(self.workflow.jobs.keys()):
            m = f"\tThe {i}th job is '{job}'"
            summary.append(m)
            if verbose_level >= 2:
                # Tasks
                for j, (uid, task) in enumerate(self.workflow.jobs[job].get_tasks_dict().items()):
                    m = f"\t\tThe {j}th task is {uid}"
                    summary.append(m)

        # Validation
        if self._is_validated:
            m = f"The simulation has been validated."
        else:
            m = f"The simulation has NOT been validated."
        summary.append(m)

        # Output
        m = f"The results will be saved in the '{self._output_folder}' folder."
        summary.append(m)

        return summary

    # def _repr_html_(self):
    #     # table = self._repr_html_table()
    #     # geometry = self._repr_html_geometry()

    #     both = [
    #         '<table>',
    #         '<tr>',
    #         '<td>',
    #         'Workflow',
    #         '</td>',
    #         '<td>',
    #         f"{self.workflow}",
    #         '</td>',
    #         '</tr>',
    #         '</table>',
    #     ]
    #     return '\n'.join(both)

    def run(self, run_dict: dict) -> int:
        """Run a Cervino simulation.

        Parameters
        ----------
        run_dict: dict
            Fill this out!
        """

        run_dict['concurrent_jobs'] = run_dict['max_ranks'] // run_dict['n_ranks']
        print(f"The number of concurrent jobs is {run_dict['concurrent_jobs']}")

        results_dict = dict()

        # Iterate over jobs of the workflow
        for job, tasks in self.workflow.jobs.items():
            solver = self.workflow.dict[job][0] #.solver
            site_name = self.workflow.dict[job][1] #.site
            site_type = 'cluster' if (site_name in sites_cluster_valid) else 'local'
            if run_dict['verbose']: print(f"Executing job '{job}' with solver '{solver}' on '{site_name}'")
            func_name = f"_run_{job}_{site_type}"
            mod = importlib.import_module(f"..solvers.{solver}", 'cervino.core')
            run_func = getattr(mod, func_name)
            # results[job], output_files_hdf5 = run_func(self, run_dict, tasks)
            # uid_input_list, additional_input_list, submission_input_list = run_func(self, run_dict, tasks)
            results_dict[job] = run_func(self, run_dict, tasks, timing=True)
            # self._jobid_dict[job] = uid_jobid_list 

        # return results, output_files_hdf5
        # return uid_input_list, additional_input_list, submission_input_list
        return results_dict

    def attach_tasks_to_job_from_file(self, job: str, fn: str, fmt: str='pickle'):
        """Load a dictionary of existing tasks from a pickle file.

        Parameters
        ----------
        job: str
            Job type.
        fn: str
            File name of the pickle or TOML file containing the tasks.
        fmt: str
            'pickle' or 'toml'.
        """

        fn = Path(fn)

        if fmt == 'pickle':
            self.workflow.jobs[job]._attach_tasks_from_pickle(fn)
        elif fmt == 'toml':
            self.workflow.jobs[job]._attach_tasks_from_toml(fn)
        else:
            raise ValueError(f"The chosen format `{fmt}` is not valid.`")
        self._job_tasks_files[job] = Path(fn)

    def check_status_cluster(self) -> dict:
        """Check the status of a Cervino workflow
        running on a cluster.
        """

        # Iterate over jobs of the workflow
        for job in self.workflow.jobs.keys():
            solver = self.workflow.dict[job][0] #.solver
            site_name = self.workflow.dict[job][1] #.site
            site_type = 'cluster' if (site_name in sites_cluster_valid) else 'local'
            if site_type == 'local':
                print('This is not implemented for a simulation launched on a local machine.')
                # return 0
            elif site_type == 'cluster':
                func_name = f"_check_status_cluster"
                mod = importlib.import_module(f"..solvers.{solver}", 'cervino.core')
                run_func = getattr(mod, func_name)
                self._job_status_dict[job] = run_func(self, job)
        
        # Iterate over jobs of the workflow
        status_summary_dict = OrderedDict()
        for job, tasks in self._job_status_dict.items():
            n_pend = n_run = n_done = n_other = 0
            for d in tasks.values():
                if d['STAT'] == 'PEND':
                    n_pend += 1
                elif d['STAT'] == 'RUN':
                    n_run += 1
                elif d['STAT'] == 'DONE':
                    n_done += 1
                else:
                    n_other += 1
            status_summary_dict[job] = (n_pend, n_run, n_done, n_other)

        return status_summary_dict

    def print_status_cluster(self, task_uid: str=None, jobid: int=None) -> dict:
        """Print the status of a Cervino workflow
        running on a cluster.

        Parameters
        ----------
        task_uid: str
            UID of a task running the cluster queue.
        jobid: int
            ID of the job submitted to the cluster queue.
        """

        status_summary_dict = self.check_status_cluster()

        # Iterate over jobs of the workflow
        for job, tasks in self._job_status_dict.items():
            print(f"Status of job `{job}`")
            print(f"PEND, RUN, DONE, OTHER: {status_summary_dict[job]}")
            for uid, d in tasks.items():
                print(f"\tThe status of task `{uid}` (job {d['JOBID']}) is {d['STAT']}.")

    def prepare_output_on_cluster(self) -> int:
        """Convert to the cervino format the output created
        by a cervino simulation running on a cluster.

        Parameters
        ----------
        dir_local: str
            Local directory where to save the retrieved files.
        """

        # Iterate over jobs of the workflow
        for i, job in enumerate(self.workflow.jobs.keys()):
            solver = self.workflow.dict[job][0] #.solver
            site_name = self.workflow.dict[job][1] #.site
            site_type = 'cluster' if (site_name in sites_cluster_valid) else 'local'
            if site_type == 'local':
                print('This is not implemented for a simulation launched on a local machine.')
                # return 0
            elif site_type == 'cluster':
                func_name = f"_convert_output_cluster"
                mod = importlib.import_module(f"..solvers.{solver}", 'cervino.core')
                run_func = getattr(mod, func_name)
                result = run_func(self, job, i)

        return 0

    def fetch_from_cluster(self, dir_local: str=Path().cwd()) -> int:
        """Retrieve the output created by a Cervino simulation
        running on a cluster.

        Parameters
        ----------
        dir_local: str
            Local directory where to save the retrieved files.
        """

        # Iterate over jobs of the workflow
        for job in self.workflow.jobs.keys():
            solver = self.workflow.dict[job][0] #.solver
            site_name = self.workflow.dict[job][1] #.site
            site_type = 'cluster' if (site_name in sites_cluster_valid) else 'local'
            if site_type == 'local':
                print('This is not implemented for a simulation launched on a local machine.')
                # return 0
            elif site_type == 'cluster':
                func_name = f"_fetch_from_cluster"
                mod = importlib.import_module(f"..solvers.{solver}", 'cervino.core')
                run_func = getattr(mod, func_name)
                result = run_func(self, job)

        return result
    
    # def fetch(self, dir_local: str=None) -> int:
    #     """Retrieve the output created by a Cervino simulation
    #     running on a cluster.

    #     Parameters
    #     ----------
    #     dir_local: str
    #         Local directory where to save the retrieved files.
    #     """

    #     if self._site == 'local':
    #         print('This is not implemented for a simulation launched on a local machine.')
    #         return 0
    #     elif self._site == 'cluster':
    #         return _fetch_cluster(self._cluster_address,
    #                               self._input_dict['general']['user'],
    #                               self.get_output_files(),
    #                               dir_local,
    #                               self._dir_remote)

################################
# Utilities
################################
def create_workflow_dict(workflow_type: str) -> OrderedDict:
    """Create a workflow dictionary with
    solver and site specification.

    Parameters
    ----------
    workflow_type: str
        Type of workflow.

    Returns
    -------
    workflow_dict: dict
        Dictionary which defines solver and site
        for each job of the workflow.
    """
    workflow_dict = OrderedDict()
    for job in workflows_valid[workflow_type]:
        workflow_dict[job] = ('', '')
    return workflow_dict

