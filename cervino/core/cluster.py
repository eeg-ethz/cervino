#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 11.06.2019

@author: Filippo Broggini (ETH Zürich) - filippo.broggini@erdw.ethz.ch
"""

from pathlib import Path
import subprocess
from tqdm import tqdm_notebook
import time
from paramiko import SSHClient
import re
from collections import OrderedDict

def _create_submission_script_lsf(fn_input: str=None, fn_script: str='script.sh', job_name: str='cervino', ncores: int=24):
    r"""Create script to submit a job
    to a LSF scheduler
    """

    script = []

    script.append('#!/bin/bash\n')

    # Load modules
    script.append('module load new\n')
    script.append('module load intel/16.0.2.181\n')
    script.append('module load open_mpi\n')

    script.append(f"bsub -n {ncores} -N -B -J \"{job_name}\" -o {job_name}.out -e {job_name}.err \'export OMP_NUM_THREADS={ncores}; matterhorn {fn_input}\'\n")
    
    # Write script to file
    with open(fn_script, 'w') as f:
        # Further file processing goes here
        f.writelines(''.join(script))

    return fn_script

def _run_cluster(cluster_address, username, cmd, dir_remote, progress, fn_progress, input_list, verbose, ncores, fn_solver_input, fn_submission_script, jobname) -> int:
    r"""Submit a Cervino job to a cluster
    """

    # Create submission script
    fn_script = _create_submission_script_lsf(fn_solver_input, fn_submission_script, jobname, ncores)

    # Connect to the cluster (ssh)
    cluster = SSHClient()
    cluster.load_system_host_keys()
    cluster.connect(hostname=cluster_address, username=username)

    # Add script to input files to transfer to the cluster
    input_list.append(fn_script)

    # Transfer input files to the cluster
    ftp_client = cluster.open_sftp()
    for fn in input_list:
        dir_euler_sub = (Path(dir_remote) / fn).parent
        stdin, stdout, stderr = cluster.exec_command(f"test -d {dir_euler_sub} && echo 1 || echo 0")
        result = int(stdout.readline())
        if not result:
            print(f"Directory '{dir_euler_sub}' does not exist. I will create it for you!")
            result = cluster.exec_command(f"mkdir {dir_euler_sub}")
        # print(fn)
        # print(Path(dir_remote) / fn)
        ftp_client.put(fn, str(Path(dir_remote) / fn))
    ftp_client.close()

    # Submit the job
    result = cluster.exec_command(f"cd {dir_remote} && bash {fn_submission_script}")

    # Extract the job id
    jobid = re.findall(r'(?<=<)[\d]+(?=>)', result[1].readlines()[0])[0]
    print(f"The jobid is {jobid}")

    # Close connection
    cluster.close()

    return int(jobid)

def _check_status_cluster(cluster_address, username, jobid) -> dict:
    r"""Check the status of a submitted job
    """

    # Connect to the cluster (ssh)
    cluster = SSHClient()
    cluster.load_system_host_keys()
    cluster.connect(hostname=cluster_address, username=username)
    
    # Check the status of the job
    result = cluster.exec_command(f"bjobs {jobid}")

    bjobs = result[1].readlines()
    bjobs_header = bjobs[0].split()
    bjobs_values = bjobs[1].split()
    # Compose SUBMIT_TIME value
    submit_time = f"{bjobs_values[-3]} {bjobs_values[-2]} {bjobs_values[-1]}"
    del bjobs_values[-3:]
    bjobs_values.append(submit_time)
    # Check if the EXEC_HOST field is empty
    if len(bjobs_values) < 8:
        bjobs_values.insert(4, '')
    bjobs_dict = OrderedDict()
    for i in range(8):
        bjobs_dict[bjobs_header[i]] = bjobs_values[i]

    # Close the connection
    cluster.close()

    print(f"The status of the job {jobid} is {bjobs_dict['STAT']}.")

    return bjobs_dict

def _convert_output_to_hdf5():
    pass

def _fetch_cluster(cluster_address, username, output_list, dir_local, dir_remote) -> int:
    r"""Retrieve the output of a Cervino job from a cluster
    """

    # Connect to Euler (ssh)
    cluster = SSHClient()
    cluster.load_system_host_keys()
    cluster.connect(hostname=cluster_address, username=username)

    # Transfer output files from the cluster
    ftp_client = cluster.open_sftp()
    files = sorted(ftp_client.listdir(str(dir_remote)))
    # for fn in output_list:
    #     ftp_client.get(str(Path(dir_remote) / fn), str(Path(dir_local) / fn))
    for file in files:
        if any(substring in file for substring in output_list):
            remote_file = str(Path(dir_remote) / file)
            local_file = str(Path(dir_local) / file)
            ftp_client.get(remote_file, local_file)
    
    # Close connection
    ftp_client.close()
    cluster.close()

    return 0