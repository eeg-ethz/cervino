import os
import math
import numpy as np
import h5py
from pathlib import Path, PurePosixPath
from typing import List, Tuple
import xarray as xr

from ..solvers.matterhorn_to_cervino import _create_hdf5_from_matterhorn_binary
from ..solvers.cervino_to_matterhorn import _create_matterhorn_binary_from_hdf5

# DTYPE = np.float64

# __all__ = ['BaseDomain', 'AcousticDomain', 'ElasticIsotropicDomain']

class BaseDomain():
    """Base class for domain.

    Attributes
    ----------
    domain_type: string
        Type of domain (Acoustic, Elastic isotropic, ...)
    required_models: tuple
        Tuple of models required by the domain
    """

    def __init__(self, domain_type: str, required_models: tuple):
        """
        Parameters
        ----------
        domain_type: string
            Type of domain (Acoustic, Elastic isotropic, ...)
        required_models: tuple
            Tuple of models required by the domain
        """

        self.type = domain_type
        self.required_models = required_models
        self._hdf5_file = None

        self._origin = [None, None, None]
        self._d = [None, None, None]
        self._n = [-1, -1, -1]
        self._ndim = 0
        self._decomposition = {
            0: [],
            1: [],
            2: [],
        }
        self._decomposition_set = False
        self._geometry_set = False
        self._geometry_from_file = False

    # def _repr_html_(self):
    #     table = self._repr_html_table()
    #     geometry = self._repr_html_geometry()
    #     # try:
    #     #     grid = self.to_svg(size=config.get('array.svg.size', 120))
    #     # except NotImplementedError:
    #     #     grid = ""

    #     both = [
    #         '<table>',
    #         '<tr>',
    #         '<td>',
    #         table,
    #         '</td>',
    #         '<td>',
    #         geometry,
    #         '</td>',
    #         '</tr>',
    #         '</table>',
    #     ]
    #     return '\n'.join(both)

    # def _repr_html_table(self):
    #     table = [
    #         '<table>'
    #         '  <thead>'
    #         '    <tr><td> </td><th> Values </th></tr>',
    #         '  </thead>',
    #         '  <tbody>',
    #         '    <tr><th> Type </th><td> %s </td></tr>' % (self.type),
    #         '    <tr><th> Shape </th><td> %s </td></tr>' % (str(self._n)),
    #         '    <tr><th> dx, dy, dz </th><td> %s </td></tr>' % (str(self._d)),
    #         '  </tbody>',
    #         '</table>'
    #     ]
    #     return '\n'.join(table)

    # def _repr_html_geometry(self):
    #     geometry = [
    #         '<svg width="200" height="150" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">',
    #         '  <g>',
    #         '    <rect id="svg_1" width="180" height="130" x="10" y="10" stroke-width="3" stroke="#000000" fill="#00FFFF"/>',
    #         '  </g>',
    #         '/svg>'
    #     ]
    #     return '\n'.join(geometry)

    def _set_geometry(self, d: List[float], n: List[int], origin: List[float]=(0, 0, 0)):
        """Set the geometry of the domain.

        Parameters
        ----------
        d : tuple of floats
            Spatial discretization in (x, y, z)
        n : tuple of int
            Dimensions in (x, y, z). Set y to 0 for a 2D medium.
        origin : tuple of floats
            Origin in (x, y, z)
        """

        if len(d) == 2 and len(n) == 2:
            self._ndim = 2
            self._d = [d[0], 1.0, d[1]]
            self._n = [n[0], 1, n[1]]
            self._origin = [origin[0], 0.0, origin[1]]
        elif len(d) == 3 and len(n) == 3:
            self._ndim = 3
            self._d = d
            self._n = n
            self._origin = origin
        else:
            raise ValueError('d and n should include three values each')

        for i, j in enumerate(self._n):
            self._decomposition[i] = [j,]
        self._decomposition_set = True

        self._geometry_set = True

    def _get_geometry(self) -> dict:
        """Get the geometry of the domain.

        Returns
        -------
        geometry: dict
            Dictionary with geometry of the domain.
        """

        if self._geometry_set:
            return {
                "origin": self._origin,
                "number-of-cells": self._n,
                "cell-size": self._d,
                "decomposition": self._decomposition
                }
        else:
            print("Geometry is not set! You need to set it first")
            return None

    def set_decomposition(self, decomposition: tuple, decomposition_dict: dict=None):
        """Set the domain decomposition
        for a simulation across multiple nodes.
        
        Parameters
        ----------
        decomposition: tuple
            Number of sub-domains along each dimension.       
        decomposition_dict: dict
            Dict of ...
        """
        
        for i, parts in enumerate(decomposition):
            quotient, remainder = divmod(self._n[i], parts)
            self._decomposition[i] = [quotient + int(i < remainder) for i in range(parts)]

        # Check
        for k, v in self._decomposition.items():
            assert sum(v) == self._n[k]

    def set_model_from_hdf5(self, fn_h5: str, create_solver_binary: str=None):
        """Read HDF5 file of the domain
        and check if the required models are included.

        This HDF5 needs to be consistent with the netCDF format.
        See the Cervino website for additional information.

        Parameters
        ----------
        fn_h5: str
            Path of the HDF5 file containing the domain.
        create_solver_binary: str
            Create the model parameters binary files required by
            the specific solver.
        """

        fn = Path(fn_h5)
        required_dims = ('x', 'z')
        missing_models = 0
        with h5py.File(fn, 'r') as f:
            for m in self.required_models:
                if m not in f.keys():
                    print(f"'{m}' model is missing from the HDF5 file")
                    missing_models += 1
            for m in required_dims:
                if m not in f.keys():
                    print(f"'{m}' coordinates are missing from the HDF5 file")
                    missing_models += 1
            if 'y' in f.keys():
                print(f"'y' coordinate is present in the HDF5 file: 3D domain")
                dims = ('x', 'y', 'z')
            else:
                print(f"'y' coordinate is not present in the HDF5 file: 2D domain")
                dims = ('x', 'z')

        if missing_models:
            raise ValueError(f"{missing_models} models are missing from the HDF5 file.")
        else:
            self._hdf5_file = fn #str(fn) #os.fspath(fn)
        
        d = []
        n = []
        origin = []
        with h5py.File(fn, 'r') as f:
            for m in dims:
                d.append(np.mean(np.diff(f[m])))
                n.append(len(f[m]))
                origin.append(f[m][0])
        self._set_geometry(d=d, n=n, origin=origin)
        self._geometry_from_file = True

        if create_solver_binary == 'matterhorn':
            _create_matterhorn_binary_from_hdf5(
                fn_h5=self._hdf5_file,
                required_models=self.required_models
            )

    # def set_model_from_xarray(self, dataset: xr.Dataset, fn_h5: str=None):
    #     """Set the domain from an existing xarray.Dataset
    #     and check if the required models are included.

    #     Parameters
    #     ----------
    #     dataset: xarray.Dataset
    #         xarray Dataset containing the domain.
    #     fn_h5: str
    #         If `fn_h5` is defined, the Dataset will be saved as a HDF5 file.
    #     """

    #     required_dims = ('x', 'z')
    #     missing_models = 0
    #     for m in self.required_models:
    #         if m not in dataset.keys():
    #             print(f"'{m}' model is missing from the xarray Dataset")
    #             missing_models += 1
    #     for m in required_dims:
    #         if m not in dataset.keys():
    #             print(f"'{m}' coordinates are missing from the xarray Dataset")
    #             missing_models += 1
    #     if 'y' in dataset.keys():
    #         print(f"'y' coordinate is present in xarray Dataset: 3D domain")
    #         dims = ('x', 'y', 'z')
    #     else:
    #         print(f"'y' coordinate is not present in the xarray Dataset: 2D domain")
    #         dims = ('x', 'z')

    #     if missing_models:
    #         raise ValueError(f"{missing_models} models are missing from the xarray Dataset.")

    #     if fn_h5 is not None:
    #         dataset.to_netcdf(fn_h5, format='NETCDF4', engine="h5netcdf")
    #         self._hdf5_file = str(fn_h5)
        
    #     d = []
    #     n = []
    #     origin = []
    #     for m in dims:
    #         d.append(np.mean(np.diff(dataset[m])))
    #         n.append(len(dataset[m]))
    #         origin.append(dataset[m].data[0])
    #     self._set_geometry(d=d, n=n, origin=origin)

    def set_model_from_matterhorn_binary(
        self,
        filename_prefix: str,
        n: Tuple[int],
        d: Tuple[float],
        origin: Tuple[float] = (0, 0, 0),
        create_hdf5: bool = False,
        verbose: bool = False,
    ):
        """Set the domain from existing Matterhorn-ready binary files.

        Parameters
        ----------
        filename_prefix : str
            Filename prefix template of the files containing the domain.
        n : tuple of int
            Dimensions in (x, y, z). Set y to 1 for a 2D medium.
        d : tuple of floats
            Spatial discretization in (x, y, z).
        origin : tuple of floats
            Origin in (x, y, z).
        create_hdf5: bool
            If `True`, the `cervino` domain HDF5 file is created.

        Returns
        -------
        fn_h5: str
            File name of the HDF5 netCDF file.
        """

        missing_models = 0
        for m in self.required_models:
            fn = Path(f"{filename_prefix}_{m}.bin")
            if not fn.exists():
                print(f"The '{fn}' file is missing.")
                missing_models += 1

        if missing_models:
            raise ValueError(f"{missing_models} models are missing from the HDF5 file.")
        else:
            self._hdf5_file = Path(f"{filename_prefix}.h5") #os.fspath(f"{filename_prefix}.h5")
        
        self._set_geometry(d=d, n=n, origin=origin)

        if create_hdf5:
            result = _create_hdf5_from_matterhorn_binary(
                filename_prefix=Path(filename_prefix),
                fn_h5=self._hdf5_file,
                required_models=self.required_models,
                n=n,
                d=d,
                origin=origin
            )
            return result

    # def set_model_from_numpy(self, models_array: np.ndarray=None, geometry: dict=None,
    #                         fn_prefix: str=None, solver: str=None, create_hdf5: bool=False,
    #                         verbose: bool=False):
    #     """Write hdf5 file of the domain
    #     from numpy ndarrays.

    #     Parameters
    #     ----------
    #     models_array : ndarray
    #         Ndarray containing the required models (e.g., vp, vs, rho, for an elastic isotropic simulation)
    #     geometry: dict
    #         Dictionary of models
    #     fn_prefix: string
    #         Filename prefix used to create the binary files required by the solver.
    #     solver: string
    #         If `solver` is one of 'matterhorn', 'salvus', or 'wfi',
    #         then the required binary (specific to the solver) files will be also created.
    #     create_hdf5: bool
    #         If `True`, an HDF5 for this model will also be created.
    #     """
        
    #     # 3D cube
    #     # The third dimension is the number of models provided
    #     # Create binary files?
    #     # Create hdf5 file?
    #     # Geometry must be provided
    #     # Choose path carefully

    #     # Check if models_array has the correct shape
    #     if models_array.ndim not in (3, 4):
    #         raise ValueError('''The ndarray must have 3 dimensions for 2D simulations(nmodels x nx x nz) or \n
    #                             4 dimensions for 3D simulations(nmodels x nx x ny x nz)''')

    #     # Check if models_array has the correct number of required models
    #     if models_array.shape[0] != len(self.required_models):
    #         raise ValueError(f"The ndarray does not contain the correct number of models: {len(self.required_models)}")

    #     fn = Path(fn_prefix) #.relative_to(Path.cwd())
    #     if verbose:
    #         print(f"The file name prefix is {fn}")

    #     # Set the geometry
    #     self._set_geometry(d=geometry['cell-size'], n=geometry['number-of-cells'], origin=geometry['origin'])
    #     # self._geometry_from_file = True

    #     # Create binary files
    #     if solver == 'matterhorn':
    #         # Create binary files for Matterhorn
    #         fn.parent.mkdir(parents=True, exist_ok=True)
    #         for i in range(models_array.shape[0]):
    #             models_array[i, ...].tofile('{}_{}.bin'.format(fn_prefix, self.required_models[i]))
    #     elif solver == 'wfi':
    #         # Create binary files for WFI
    #         fn.parent.mkdir(parents=True, exist_ok=True)
    #         # WFI requires the velocity model only
    #         # WFI models are written in double precision
    #         with open('{}_{}'.format(fn_prefix, 'vp.bin'), 'wb') as f:
    #             f.write(np.int32(geometry['number-of-cells'][0]))
    #             f.write(np.int32(geometry['number-of-cells'][2]))
    #             f.write(np.float64(geometry['cell-size'][0]))
    #             f.write(np.float64(geometry['cell-size'][2]))
    #             f.write(np.float64(geometry['origin'][0]))
    #             f.write(np.float64(geometry['origin'][2]))
    #             i = 0 # Velocity
    #             models_array[i, ...].astype('float64').tofile(f)
    #     else:
    #         if solver is not None:
    #             raise ValueError('The solver is not recognized!')
        
    #     # Create HDF5 file
    #     if create_hdf5:
    #         fn_h5 = fn_prefix + '.h5'
    #         if verbose:
    #             print(f"The name of the HDF5 file is {fn_h5}")
    #         self._hdf5_file = str(fn_h5)
    #         with h5py.File(fn_h5, 'w') as f:
    #             # Create models group
    #             model_group = f.create_group('Models')

    #             for i, model in enumerate(self.required_models):
    #                 model_group.create_dataset(model, data=models_array[i, ...])
                
    #             # Create geometry group
    #             geometry_group = f.create_group('Geometry')
    #             geometry_group.attrs['number-of-cells'] = geometry['number-of-cells']
    #             geometry_group.attrs['cell-size'] = geometry['cell-size']
    #             geometry_group.attrs['origin'] = geometry['origin']

    def get_domain_dict(self) -> dict:
        """ Return a dictionary with the domain parameters.

        The dictionary can be used to create
        the domain section of a TOML input file.

        Returns
        -------
        dictionary: dict
            Dictionary with domain parameters.
        """

        return {
            "dimension": self._ndim,
            "geometry": self._get_geometry(),
            "model": {
                "filename": os.fspath(self._hdf5_file),
                "format": "hdf5",
                "required-models": self.required_models
            }
        }


class AcousticDomain(BaseDomain):
    """Acoustic domain.

    Velocity and density models for an acoustic medium.
    """

    def __init__(self):

        domain_type = 'Acoustic'
        required_models = ('VP', 'RHO')

        super().__init__(domain_type, required_models)

        # Vis parameters


class ElasticIsotropicDomain(BaseDomain):
    """Elastic isotropic domain.

    Velocities and density models for an elastic isotropic medium.
    """

    def __init__(self):

        domain_type = 'ElasticIsotropic'
        required_models = ('VP', 'VS', 'RHO')

        super().__init__(domain_type, required_models)

        # Vis parameters
