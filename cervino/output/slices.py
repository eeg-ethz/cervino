from typing import List

import numpy as np
import matplotlib.pyplot as plt

from ..core.definitions import fields_valid, fields_valid_string

__all__ = ['TimeSlice', 'DFTSlice']

class BaseSlice():
    """Base class for representing a slice on a grid.
    """

    def __init__(self, slice_type, fmt):
        """Constructor for the BaseSlice class.
        """
        self.type = slice_type
        self.format = fmt

        # Actual slices
        self.slices = dict()

    def _validate_slices(self, slice: dict):
        """Validate the slice parameters.
        """

        # Check fields
        for field in slice['fields']:
            if field not in fields_valid.keys():
                raise ValueError(f"The field '{field}' is not valid! The valid fields are '{fields_valid_string}'.")

class TimeSlice(BaseSlice):
    """Derived class of BaseSlice for representing one or more
    time slices on a grid.

    Parameters
    ----------
    start_timestep : int
        Initial time step recorded by the receivers.
    end_timestep : int
        Final time step recorded by the receivers.
    timestep_increment : int
        Time step increment.
    fmt: string, optional
        Format of the output file.
    """

    def __init__(self, start_timestep, end_timestep, timestep_increment, fmt: str='bin'):
        """Constructor for the TimeSlice class.
        """

        slice_type = 'slice_time'

        super().__init__(slice_type, fmt)

        self.start_timestep = start_timestep
        self.end_timestep = end_timestep
        self.timestep_increment = timestep_increment

    def add_slices(self, axis: str, slice_index: int, fields: List[str], filename: str='slice', fmt: str=None) -> dict:
        """Add a slice on a grid.

        Parameters
        ----------
        axis : str
            Axis along which the slice is taken: 'x', 'y', or 'z'.
        slice_index: int, optional
            Location (index) along the chosen axis.
        fields : list of string
            Fields to record. Stress: 'sx'.
            Velocities: 'vx', 'vy', 'vz'.
        filename: string, optional
            Name of the output file.
        fmt: string, optional
            Format of the output file.
        """

        slice_ = dict()

        unique_id = f"{len(self.slices):04d}"
        slice_['axis'] = axis
        slice_['slice-index'] = slice_index
        slice_['fields'] = fields
        slice_['filename-prefix'] = filename
        if fmt is not None:
            slice_['fmt'] = fmt
        
        self._validate_slices(slice_)

        self.slices[f"slice_time_{unique_id}"] = slice_

        return slice_

    def get_slices_dict(self):
        """ Return a dictionary with the slice parameters.

        The dictionary can be used to create
        the output section of a TOML input file.
        """

        return {
            "start-timestep" : self.start_timestep,
            "end-timestep" : self.end_timestep,
            "timestep-increment" : self.timestep_increment,
            "slices" : self.slices,
            "format": self.format,
            "type": self.type
        }

class DFTSlice(BaseSlice):
    """Derived class of BaseSlice for representing one or more
    DFT slices on a grid.

    Parameters
    ----------
    dft_frequencies: list
        List of frequencies
    dft_wavelet_coefficients: list
        List of complex-valued coefficients
        of the FFT of the source time functions
        corresponding to the the selected frequencies listed in `dft_frequencies`
    fmt: string, optional
        Format of the output file.
    """

    def __init__(self, dft_frequencies: list=None, dft_wavelet_coefficients: list=None, fmt: str='bin'):
        """Constructor for the DFTSlice class.
        """

        slice_type = 'slice_dft'

        super().__init__(slice_type, fmt)

        self.dft_frequencies = dft_frequencies
        self.dft_wavelet_coefficients = dft_wavelet_coefficients

    def add_slices(self, axis: str, slice_index: int, fields: List[str], filename: str='slice', fmt: str=None):
        """Add a slice on a grid.

        Parameters
        ----------
        axis : str
            Axis along which the slice is taken: 'x', 'y', or 'z'.
        slice_index: int, optional
            Location (index) along the chosen axis.
        fields : list of string
            Fields to record. Stress: 'sx'.
            Velocities: 'vx', 'vy', 'vz'.
        filename: string, optional
            Name of the output file.
        fmt: string, optional
            Format of the output file.
        """

        slice_ = dict()

        unique_id = f"{len(self.slices):04d}"
        slice_['axis'] = axis
        slice_['slice-index'] = slice_index
        slice_['fields'] = fields
        slice_['filename-prefix'] = filename
        if fmt is not None:
            slice_['fmt'] = fmt

        self._validate_slices(slice_)

        self.slices[f"slice_dft_{unique_id}"] = slice_

        return slice_

    def get_slices_dict(self):
        """ Return a dictionary with the slice parameters.

        The dictionary can be used to create
        the output section of a TOML input file.
        """

        return {
            "dft-frequencies" : self.dft_frequencies,
            "dft-wavelet-coefficients" : self.dft_wavelet_coefficients,
            "slices" : self.slices,
            "format": self.format,
            "type": self.type
        }
