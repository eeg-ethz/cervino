from typing import List

import numpy as np
import matplotlib.pyplot as plt

from ..core.definitions import fields_valid, fields_valid_string

__all__ = ['Gather']

class BaseReceiver():
    """Base class for representing a receiver on a grid.
    """

    def __init__(self, output_type, start_timestep, end_timestep, timestep_increment, fmt):
        """Constructor for the BaseReceiver class.
        """
        self.type = output_type
        self.fmt = fmt
        self.start_timestep = start_timestep
        self.end_timestep = end_timestep
        self.timestep_increment = timestep_increment

        # Actual receivers
        self.receivers = dict()

    def get_receivers_dict(self):
        """Return a dictionary with the receiver parameters.

        The dictionary can be used to create
        the output section of a TOML input file.
        """

        return {
            "start-timestep" : self.start_timestep,
            "end-timestep" : self.end_timestep,
            "timestep-increment" : self.timestep_increment,
            "receivers" : self.receivers,
            "format": self.fmt,
            "type": self.type
        }

    def _validate_receivers(self, receiver: dict):
        """Validate the receiver parameters.
        """

        # Check fields
        for field in receiver['fields']:
            if field not in fields_valid.keys():
                raise ValueError(f"The field '{field}' is not valid! The valid fields are '{fields_valid_string}'.")

class Gather(BaseReceiver):
    """Derived class of BaseReceiver for representing one or more
    point receivers on a grid.

    Parameters
    ----------
    start_timestep : int
        Initial time step recorded by the receivers.
    end_timestep : int
        Final time step recorded by the receivers.
    timestep_increment : int
        Time step increment.
    fmt: string, optional
        Format of the output file.
    """

    def __init__(self, start_timestep, end_timestep, timestep_increment, fmt='su'):
        """Constructor for the Gather class.
        """

        output_type = 'gather'

        super().__init__(output_type, start_timestep, end_timestep, timestep_increment, fmt)

    def add_receivers(self, origin: tuple, increment: tuple, nr: int, fields: List[str], filename: str='gather', fmt: str=None) -> dict:
        """Add an array of point receivers on a grid.

        Parameters
        ----------
        origin : float, ndarray
            Location of the source in the physical coordinates of the domain.
            loc should be a (n x 3) ndarray, where n denotes
            the number of sources.
        increment : float, ndarray
            Increment (group interval) along one of the axis.
        nr: integer
            Number of receivers.
        fields : list of string
            Fields to record. Stress: 'sx'.
            Velocities: 'vx', 'vy', 'vz'.
        filename: string, optional
            Name of the output file.
        fmt: string, optional
            Format of the output file.

        Returns
        -------
        receiver: dict
            Returen dictionary for the gather.
        """

        receiver = dict()

        unique_id = f"{len(self.receivers):04d}"
        receiver['receiver-origin'] = origin
        receiver['receiver-increment'] = increment
        receiver['number-of-receivers'] = nr
        receiver['fields'] = fields
        receiver['filename-prefix'] = filename
        if fmt is not None:
            receiver['fmt'] = fmt
        
        self._validate_receivers(receiver)

        self.receivers[f"gather_{unique_id}"] = receiver

        return receiver

    def add_receivers_from_file(self, receivers_filename: str, fields: List[str], filename: str='gather', fmt: str=None) -> dict:
        """Add an array of point receivers on a grid.

        Parameters
        ----------
        receivers_filename: str
            Name of the output file.
        fields : list of str
            Fields to record. Stress: 'sx'.
            Velocities: 'vx', 'vy', 'vz'.
        filename: string, optional
            Name of the output file.
        fmt: str, optional
            Format of the output file.

        Returns
        -------
        receiver: dict
            Returen dictionary for the gather.
        """

        receiver = dict()

        unique_id = f"{len(self.receivers):04d}"
        receiver['receivers-filename'] = receivers_filename
        receiver['fields'] = fields
        receiver['filename-prefix'] = filename
        if fmt is not None:
            receiver['fmt'] = fmt
        
        self._validate_receivers(receiver)

        self.receivers[f"gather_{unique_id}"] = receiver

        return receiver