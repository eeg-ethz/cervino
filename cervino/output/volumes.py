from typing import List

import numpy as np
import matplotlib.pyplot as plt

from ..core.definitions import fields_valid, fields_valid_string

__all__ = ['TimeVolume', 'DFTVolume']

class BaseVolume():
    """Base class for representing a volume on a grid.
    """

    def __init__(self, volume_type, fmt):
        """Constructor for the BaseVolume class.
        """
        self.type = volume_type
        self.format = fmt

        # Actual volumes
        self.volumes = dict()

    def _validate_volumes(self, volume: dict):
        """Validate the volume parameters.
        """

        # Check fields
        for field in volume['fields']:
            if field not in fields_valid.keys():
                raise ValueError(f"The field '{field}' is not valid! The valid fields are '{fields_valid_string}'.")

class TimeVolume(BaseVolume):
    """Derived class of BaseVolume for representing one or more
    time volumes on a grid.

    Parameters
    ----------
    fmt: string, optional
        Format of the output file.
    """

    def __init__(self, fmt: str='bin'):
        """Constructor for the TimeVolume class.
        """

        volume_type = 'volume_time'

        super().__init__(volume_type, fmt)

    def add_volumes(self, fields: List[str], filename: str='volume', fmt: str=None) -> dict:
        """Add a volume on a grid.

        Parameters
        ----------
        fields : list of string
            Fields to record. Stress: 'sx'.
            Velocities: 'vx', 'vy', 'vz'.
        filename: string, optional
            Name of the output file.
        fmt: string, optional
            Format of the output file.
        """

        volume_ = dict()

        unique_id = f"{len(self.volumes):04d}"
        volume_['fields'] = fields
        volume_['filename-prefix'] = filename
        if fmt is not None:
            volume_['fmt'] = fmt
        
        self._validate_volumes(volume_)

        self.volumes[f"volume_time_{unique_id}"] = volume_

        return volume_

    def get_volumes_dict(self):
        """ Return a dictionary with the volume parameters.

        The dictionary can be used to create
        the output section of a TOML input file.
        """

        return {
            "volumes" : self.volumes,
            "format": self.format,
            "type": self.type
        }

class DFTVolume(BaseVolume):
    """Derived class of BaseVolume for representing one or more
    DFT volumes on a grid.

    Parameters
    ----------
    dft_frequencies: list
        List of frequencies
    dft_wavelet_coefficients: list
        List of complex-valued coefficients
        of the FFT of the source time functions
        corresponding to the the selected frequencies listed in `dft_frequencies`
    fmt: string, optional
        Format of the output file.
    """

    def __init__(self, dft_frequencies: list=None, dft_wavelet_coefficients: list=None, fmt: str='bin'):
        """Constructor for the DFTVolume class.
        """

        volume_type = 'volume_dft'

        super().__init__(volume_type, fmt)

        self.dft_frequencies = dft_frequencies
        self.dft_wavelet_coefficients = dft_wavelet_coefficients

    def add_volumes(self, fields: List[str], filename: str='volume', fmt: str=None):
        """Add a volume on a grid.

        Parameters
        ----------
        fields : list of string
            Fields to record. Stress: 'sx'.
            Velocities: 'vx', 'vy', 'vz'.
        filename: string, optional
            Name of the output file.
        fmt: string, optional
            Format of the output file.
        """

        volume_ = dict()

        unique_id = f"{len(self.volumes):04d}"
        volume_['fields'] = fields
        volume_['filename-prefix'] = filename
        if fmt is not None:
            volume_['fmt'] = fmt

        self._validate_volumes(volume_)

        self.volumes[f"volume_dft_{unique_id}"] = volume_

        return volume_

    def get_volumes_dict(self):
        """ Return a dictionary with the volume parameters.

        The dictionary can be used to create
        the output section of a TOML input file.
        """

        return {
            "dft-frequencies" : self.dft_frequencies,
            "dft-wavelet-coefficients" : self.dft_wavelet_coefficients,
            "volumes" : self.volumes,
            "format": self.format,
            "type": self.type
        }
