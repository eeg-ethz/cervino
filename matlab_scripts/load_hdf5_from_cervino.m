function [data] = load_hdf5_from_cervino(filename, output_uid)
% LOAD_HDF5_FROM_CERVINO  Load a data array from a cervino output.
%   [data] = LOAD_HDF5_FROM_CERVINO(filename, output_uid) extract the
%   `output_id` data array from the `filename` cervino output file.
    
fid = H5F.open(filename);
dset_id = H5D.open(fid, output_uid);
data = H5D.read(dset_id);
H5D.close(dset_id);
H5F.close(fid);   

end