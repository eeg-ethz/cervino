function [vp] = create_simple_model_for_cervino()
% CREATE_SIMPLE_MODEL_FOR_CERVINO  Create simple model for cervino.
%   [data] = CREATE_SIMPLE_MODEL_FOR_CERVINO() 

nx = 128;
nz = 256;
% The order of the dimension is important!
vp = ones(nz, nx, 'single');
vp(150:end, :) = 2500;
imagesc(vp)
fid = fopen('model_vp_matlab', 'w', 'n');
fwrite(fid, vp, 'single');
fclose(fid);

end