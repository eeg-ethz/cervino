function [im] = plot_dft_2d(data, index_freq, cmp, clip)
% PLOT_DFT_2D  Plot a 2D DFT extracted from a cervino output file.
%   [im] = PLOT_DFT_2D(data, index_freq, cmp, clip) plots the DFT slice at the
%   chosen `index_freq`, for the chosen `cmp` component, and clip the
%   values to [-clip, +clip].

im = figure();
subplot(1, 2, 1);
imagesc(data.r(:, :, index_freq, cmp).', clip * [-1, 1]);
colormap(french)
colorbar;
title(['Real, i: ' num2str(index_freq) ', cmp: ' num2str(cmp)]);
subplot(1, 2, 2);
imagesc(data.i(:, :, index_freq, cmp).', clip * [-1, 1]);
colormap(french)
colorbar;
title(['Imag, i: ' num2str(index_freq) ', cmp: ' num2str(cmp)]);


end