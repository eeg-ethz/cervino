function [im] = plot_gather_2d(data, cmp, clip)
% PLOT_GATHER_2D  Plot a 2D gather extracted from a cervino output file.
%   [im] = PLOT_GATHER_2D(data, cmp, clip) plots the selected component
%   `cmp` of the gather and clip the values to [-clip, +clip].

im = figure();
subplot(1, 1, 1);
imagesc(data(:, :, cmp).', clip * [-1, 1]);
colormap(french)
colorbar;
title(['Component: ' num2str(cmp)]);

end