# Cervino

**C**omputational **E**nvironment for **R**esearch in **V**isualization, **IN**version, and **O**ptimization

[https://eeg-ethz.gitlab.io/cervino/](https://eeg-ethz.gitlab.io/cervino/)
