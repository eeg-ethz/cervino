########
Jupytext
########

``Jupytext`` is a Jupyter plugin that reads and writes notebooks as plain text
files: either Julia, Python, R scripts, Markdown, or R Markdown documents.

- Jupytext official `documentation <https://jupytext.readthedocs.io>`_
