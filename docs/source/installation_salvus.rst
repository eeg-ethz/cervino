#########################
Installation on GNU/Linux
#########################
To use Salvus, it is first mandatory to obtain a license from Mondaic.
You will be required to have a valid license ``.salvus-licenses.toml`` file.

The Mondaic website provides updated
`documentation <https://mondaic.com/docs/installation>`_
for installing and configuring Salvus on local and remote systems.

.. note::

    In the following code blocks, you will need to replace the paths
    with actual paths on your local and remote systems.

**************
Local computer
**************
A local installation is required also if you want to run simulations exclusively
on a remote system, such as `Euler <https://scicomp.ethz.ch/wiki/Euler>`_.

Step 1
======
Obtain the Mondaic Downloader:

.. code-block:: shell

    bash -c "$(curl -sSL https://get.mondaic.com)"

Step 2
======
Install the downloaded Python package with ``pip``:

.. code-block:: shell

    pip install /work_directory/Salvus/restricted_releases/fft_eeg/python_packages/salvus-*.whl

.. warning::

    Don't forget to activate the cervino environment before installing
    the Salvus Python package:

    .. code-block:: shell

        conda activate cervino

Step 3
======
Configure the ``site`` following the
`instructions <https://mondaic.com/docs/installation/salvus_flow_configuration>`_
on the Mondaic website:

.. code-block:: shell

    salvus-flow add-site
    salvus-flow init-site NAME_OF_THE_CHOSEN_SITE

The second command will start a small simulation to test if
Salvus is correctly installed and configured.
The configuration is saved into this configuration file:
``~/.config/SalvusFlow/1.0/salvus-flow-config.toml``.
The configuration for a local system should resemble this one:

.. code-block:: toml

    [sites.NAME_OF_THE_CHOSEN_SITE]
        site_type = "local"
        default_ranks = 4
        max_ranks = 8
        salvus_binary = "/path/to/Salvus/salvu"
        run_directory = "/path/to/run/directory"
        tmp_directory = "/path/to/tmp/directory"

*****
Euler
*****
Salvus needs to be installed also on Euler. This should **not** be done by each
user but only once and installed in a location accessible by all users.
Salvus is currently installed inside the ``$PROJECT/apps`` directory.
The necessary configuration to run Salvus on Euler needs to be
performed only on your local system.

.. note::

    To install Salvus on Euler, you should acess a login node and
    follow Steps 1 and 2 of the local installation procedure above.

Step 1
======
On your local system, you should add a new ``site`` following the
`instructions <https://mondaic.com/docs/installation/salvus_flow_configuration>`_
on the Mondaic website:

.. code-block:: shell

    salvus-flow add-site

You will need to create a ``remote`` site of ``lsf`` type.
The recommended name for the new site is ``euler``.
The SSH hostname is ``euler.ethz.ch`` and the SSH username is your username.

The configuration is saved inside
``~/.config/SalvusFlow/1.0/salvus-flow-config.toml``
and you will need to add additional information to it.
You can either modify the configuration file directly or
execute ``salvus-flow edit-config`` which uses ``vim`` to edit the file.
On the Mondaic website, you can find an
`example configuration <https://mondaic.com/docs/0.10.11/installation/salvus_flow/example_sites#euler-ethz-zurich-switzerland>`_
for Euler.
The configuration for Euler should resemble this one:

.. code-block:: toml

    [sites.euler]
        site_type = "lsf"
        default_ranks = 24
        max_ranks = 240
        salvus_binary = "/cluster/project/eeg/apps/Salvus/restricted_releases/fft_eeg/bin/salvus"
        run_directory = "/cluster/home/${USER}/salvus_flow_run"
        tmp_directory = "/cluster/scratch/${USER}/salvus_flow_temp"
        use_license_tokens = true
        [sites.euler.ssh_settings]
            hostname = "euler.ethz.ch"
            username = "${USER}"
        [sites.euler.site_specific]
            modules_to_load = ['new', 'impi']
            [[sites.euler.site_specific.additional_bsub_argument]]
                name = "R"
                value = "fullnode"
                condition = "ranks >= 18"
            [[sites.euler.site_specific.additional_bsub_argument]]
                name = "R"
                value = "rusage[mem=2048]"

Step 2
======
Finally, you should test the configuration:

.. code-block:: shell

    salvus-flow init-site euler

This command will start a small simulation to test if
Salvus is correctly configured to run remote simulations on Euler.
