########################################
Installation instructions for Matterhorn
########################################

``Matterhorn`` is the finite-difference time-domain solver
for acoustic and elastic wave propagation developed and maintained by the
`EEG group <https://eeg.ethz.ch>`_.
We currently use `CMake <https://cmake.org>`_ to ensure a smooth and
straightforward cross-platform building process on GNU/Linux, Windows 10,
and macOS systems. ``Matterhorn`` has been succesfully compiled with the
`clang++ <https://clang.llvm.org>`_, `g++ <https://gcc.gnu.org>`_,
and `Intel <https://software.intel.com/en-us/c-compilers>`_ compilers.
`Open MPI <https://www.open-mpi.org>`_ is the MPI implementation we are
currently using. In the following instructions, we primarily show
how to build ``Matterhorn`` using ``clang++``.

.. note::
    ``Matterhorn`` was previously only compiled with the
    `Intel <https://software.intel.com/en-us/c-compilers>`_ compiler using
    a tailored Makfile.

*************************
Installation on GNU/Linux
*************************
The instructions for a GNU/Linux system are currently focused towards
`Ubuntu 18.04 LTS <http://releases.ubuntu.com/18.04.4/>`_.

Step 1
======
First, you need to install these fundamental development packages:

.. code-block:: shell

    sudo apt-get install cmake
    sudo apt-get install clang-9
    sudo apt-get install libopenmpi-dev

Step 2
======
After completing the previous step, you can clone the ``Matterhorn``
repository to your own computer.
First, choose an appropriate location on your file system and then clone it
using ssh if you have followed the steps :ref:`here <ssh>`:

.. code-block:: shell

    git clone git@gitlab.com:eeg-ethz/matterhorn.git

Alternatively, you can clone with HTTPS but you will need to provide your
GitLab username and password:

.. code-block:: shell

    git clone https://gitlab.com/eeg-ethz/matterhorn.git

You need to insert ``yes`` when asked.

Step 3
======
Before compiling ``Matterhorn``, you need to choose a compiler toolset and
set two environment variables:

.. code-block:: shell

    # clang toolset
    export CXX=mpic++
    export OMPI_CXX=clang++-9

.. code-block:: shell

    # gcc toolset
    export CXX=mpic++
    export OMPI_CXX=g++

.. code-block:: shell

    # Intel toolset
    export CXX=icpc
    export CXX=mpiicpc

Then, you can start the configuration and compilation process:

.. code-block:: shell

    cd matterhorn
    mkdir build
    cd build
    cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=".." ..
    make VERBOSE=1
    make install

If the compilation process ends without error, the ``Matterhorn`` executable
will be located in the ``bin`` folder.

Step 4
======
Finally, it is recommended to add the location of the ``Matterhorn``
binary to ``$PATH``. To do so, it is recommened to add this line inside
the ``~/.bashrc`` file:

.. code-block:: shell

    export PATH=/path/to/matterhorn/folder/bin:$PATH

*********************
Installation on macOS
*********************
The instructions for macOS are currently focused towards macOS Sierra (10.12.6).

.. warning::
    The installation and compilation process on macOS requires more testing!

Step 1
======
First, you need to install these fundamental development packages using the
package manager `Homebrew <https://brew.sh/>`_:

.. code-block:: shell

    brew install -v cmake
    brew install -v libomp
    brew install -v open-mpi

The ``-v`` flag prompts additional information during the installation process.

.. warning::
    The installation process for the packages above is CPU intensive and on
    an older Mac could take a long time, specially the compilation of the
    Open MPI package. If this is the case, interrupt the process and see below.

Alternative way to install Open MPI
-----------------------------------
First, download the source code for Open MPI from
`here <https://www.open-mpi.org/software/ompi/v4.0/>`_.
Then uncompress the archive and start the compilation process:

.. code-block:: shell
    
    tar -xzvf openmpi-4.0.3.tar.gz
    cd openmpi-4.0.3
    ./configure --prefix=/path/of/installation/openmpi-4.0.3 --enable-mpi-cxx CC=clang CXX=clang++
    make VERBOSE=1
    make install

Before moving to the next step, you need to add the location of the
``mpic++`` binary to the search path. To do so, add this line inside
the ``~/.bashrc`` file:

.. code-block:: shell

    export PATH=/path/of/installation/openmpi-4.0.3/bin:$PATH

Step 2
======
After completing the steps above, you can follow Steps 2 and 3 of the
instructions for a GNU/Linux system.

Step 3
======
Finally, it is recommended to add the location of the ``Matterhorn``
binary to ``$PATH``. To do so, it is recommened to add this line inside
the ``~/.bash_profile`` file:

.. code-block:: shell

    export PATH=/path/to/matterhorn/folder/bin:$PATH

**********************************
Installation on WSL for Windows 10
**********************************
``Matterhorn`` has been succesfully compiled and tested using the
Windows Subsytem for Linux (WSL) on Windows 10. You can read more about the
WSL `here <https://docs.microsoft.com/en-us/windows/wsl/about>`_.
The firs steps of the following instructions will show you how to activate the
WSL and install Ubuntu.

Step 1
======
First, you need to turn on the WSL from the Control Panel.

.. figure:: images/wsl_turn_on.png
    :width: 500px
    :align: center
    :alt: Turn on WSL
    :figclass: align-center

    Turn on the WSL from the Control Panel.

Step 2
======
Then you need to install Ubuntu 18.04 LTS from the Marketplace.

.. figure:: images/ubuntu_marketplace.png
    :width: 500px
    :align: center
    :alt: Ubuntu on Microsoft Marketplace
    :figclass: align-center

    Get and install Ubuntu from the Marketplace.

Step 3
======
After launching Ubuntu for the first time, you will need to choose a username
and a password.

.. figure:: images/launch_ubuntu.png
    :width: 500px
    :align: center
    :alt: Launch Ubuntu
    :figclass: align-center

    How to launch Ubuntu.

Step 4
======
The Ubuntu system can be accessed from the WSL terminal that was launched
with the previous step.

.. figure:: images/wsl_terminal.png
    :width: 500px
    :align: center
    :alt: WSL terminal
    :figclass: align-center

    Access Ubuntu from the WSL terminal.

First, you need to install these fundamental development packages:

.. warning::
    The installation and compilation process on WSL requires more testing!

.. code-block:: shell

    sudo apt-get update
    sudo apt-get install cmake
    sudo apt-get install clang-9
    sudo apt-get install libopenmpi-dev

.. warning::
    If the installation of ``clang-9`` fails, you might need to follow these
    additional instructions:

    .. code-block:: shell

        sudo rm /usr/share/doc/python2.7-minimal/changelog.Debian.gz
        sudo apt-get -f install
        sudo apt-get install clang-9

Step 5
======
After completing the steps above, you can follow Steps 2 and 3 of the
instructions for a GNU/Linux system.

Step 6
======
Finally, it is recommended to add the location of the ``Matterhorn``
binary to ``$PATH``. To do so, it is recommened to add this line inside
the ``~/.bashrc`` file:

.. code-block:: shell

    export PATH=/path/to/matterhorn/folder/bin:$PATH

Step 7
======
If you want to access the Ubuntu's file system from Windows 10, you need to
run the following command from within the WSL terminal:

.. code-block:: shell
    
    explorer.exe .

This will launch File Explorer showing the current Linux directory.
You can browse the Ubuntu environment's file system from there.
