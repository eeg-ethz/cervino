#################
Cervino workflows
#################

``cervino`` allows the execution of three different *workflows*:

.. include:: workflow_block.rst

Each workflow defines defines a specific set of simulations and
data processing stages connected in series,
where the output of one stage is the input of the next one.
Eache stage of the workflow is also called a *job*.
The elements of a workflow can be executed in a parallel or serial fashion.

Additionally, ``cervino`` defines a set of specific jobs that can be executed
as part of a workflow. All the taskes that compose a job must be of the same type.
The types of tasks implemented in ``cervino`` are:

.. include:: jobs_block.rst

The composition of each workflow is the following:

.. include:: workflow_jobs_block.rst