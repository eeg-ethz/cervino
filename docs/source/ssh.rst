.. _ssh:

############
SSH key pair
############

You will need a
`SSH key pair <https://www.ssh.com/ssh/public-key-authentication>`_
if you plan to access the `cervino` repository for further development,
to access a solver repository, or to access Euler in a password-less fashion.

Step 1
======
Unless already done, open a terminal to 
`generate a new SSH key pair <https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair>`_:

.. code-block:: shell
    
    ssh-keygen -o -t rsa -b 4096 -C "insert_your_email_here"

You should press Enter to accept the default location.
You can also set an empty passphrase pressing Enter twice.

.. note::
    When using an older version of ``ssh``,
    the command above might give an error.

Step 2
======
Copy the public key to the clipboard:

.. code-block:: shell

    cat ~/.ssh/id_rsa.pub # GNU/Linux and macOS
    cat ~/.ssh/id_rsa.pub | clip # Windows

Step 3
======
Finally, add the public key to your GitLab settings in the ``SSH Keys`` section.