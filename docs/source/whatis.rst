###############
What is cervino
###############

The research Python package ``cervino`` is ...

``cervino`` is steered by Python
`dictionaries <https://docs.python.org/3/tutorial/datastructures.html>`_.
``cervino`` a few fundamental dictionaries and their description can be found
under the `CERVINO DICTIONARIES` section of the navigation sideber of this
documentation website.

``cervino``'s interface is the `Python <https://www.python.org/>`_ programming language
via `Jupyter <https://jupyter.org/>`_ notebooks.
