cervino
=======


.. image:: logo/matterhorn_logo.*
    :width: 60%
    :align: center


This is the official documentation for the *cervino* project:
a **C**\omputational **E**\nvironment for **R**\esearch in **V**\isualization, **IN**\version, and **O**\ptimization.

Cervino components
------------------

    * ``core``: core module which defines the *Cervino* class
    * ``domain``: module to define geometries and model parameters
    * ``input``: define point or boundary sources
    * ``output``: define various acquisition geometries
    * ``solvers``: module which includes functions to manage the specific solver
    * ``util``: utilities, ...

Details are available in the *cervino* :ref:`api`.

For further information and contact information please
see these two web sites:

* Github repository of *cervino*: https://gitlab.com/eeg-ethz/cervino
* Website of the EEG group: http://www.eeg.ethz.ch/

.. warning::

   This project is in its early development.

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: About

   What is cervino <whatis.rst>
   Cervino workflows <workflow.rst>
   Output definition <output_definition.rst>
   Domain definition <domain_definition.rst>
   To Do <todo.rst>

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Cervino dictionaries

   Compute forward simulation <dict_task_forward.rst>
   Run dictionary <dict_run.rst>

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Getting started

   Installation <installation.rst>
   SSH <ssh.rst>
   Jupyter Notebook <jupyter.rst>
   HPC <cluster.rst>

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Tutorials

   Tutorials <tutorials/index.rst>

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Notebooks

   Meetings <notebooks/index.rst>

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Reference documentation

   API <api.rst>

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Tools

   External packages <tools.rst>

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Contributing

   How to document the code <documentation.rst>
   Development guidelines <development.rst>
   Credits <credits.rst>
