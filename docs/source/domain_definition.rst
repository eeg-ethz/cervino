*****************
Domain definition
*****************
``cervino`` defines its own file format for storing to disk the
model parameters required to run a simulation.
The model parameters, such as density and velocities, are stored in one file
using the `HDF5 <https://www.hdfgroup.org/solutions/hdf5/>`_
format and their structure follows the
`NetCDF <https://www.unidata.ucar.edu/software/netcdf/docs/faq.html>`_ convention.

.. note::

   However, in specific situations, such as when using Matterhorn as a solver,
   we still require Matterhorn-ready binary files.
   In the future, we hope to further develop Matterhorn and
   allow it to read the model paramters directly from HDF5 file.

=================
HDF5 cervino file
=================
The model parameters should be provided as an ``xarray.Dataset``
containing a ``xarray.DataArray`` for each model parameter (e.g., VP, RHO, ...)
and a ``xarray.DataArray`` for the coordinates in each dimension (e.g., x, y, z).

.. code-block:: python

   import xarray as xr

   fn_h5 = "model/model_salt3d.h5"
   ds = xr.load_dataset(fn_h5, engine="h5netcdf")

   print(ds.coords)
   print(ds.data_vars)

=============================
Matterhorn-ready binary files
=============================
Matterhorn requires that the models are created in a specific way
(see Matterhorn manual) before saving them to disk.

.. warning::

   The data should be saved to binary file in single precision (32bit)
   unless the ``DOUBLE_PRECISION`` flag was used during compilation.

After creating a 3D data cube with ``x, y, z`` dimensions
using your favorite software or programming language,
you need to transpose the dimensions according to ``y, x, z``
when using a row-major order software or according to ``z, x, y``
when using a column-major order software, such as MATLAB,
before saving the data to a binary file.
Here you can see how to do that in MATLAB :

.. code-block:: matlab

   % 2D
   nx = 128;
   nz = 256;
   vp = ones(nz, nx, 'single');
   fid = fopen('model_vp_matlab_2d', 'w', 'n');
   fwrite(fid, vp, 'single');
   fclose(fid);

.. code-block:: matlab

   % 3D
   nx = 128;
   ny = 64:
   nz = 256;
   vp = ones(nz, nx, ny, 'single');
   fid = fopen('model_vp_matlab_3d', 'w', 'n');
   fwrite(fid, vp, 'single');
   fclose(fid);

=========
Scenarios
=========
In this section, we will describe a few scenarios involving various combinations
of 1) solver chosen for the ``cervino`` simulation and 2) origin of the binary
files containing the model parameters.

.. code-block:: python

   domain_section = {
      'dimension': 2,
      'geometry': {
         'cell-size': [4.0, 1.0, 4.0],
         'number-of-cells': [2301, 1, 751],
         'origin': [0.0, 0.0, 0.0]
      },
      'model': {
         'filename': 'model/marmousi_original.h5',
         'format': 'hdf5',
         'required-models': ('VP', 'RHO')
      }
   }

Matterhorn and Matterhorn-ready binary files
============================================
If the Matterhorn-ready binary files are available and we want to use
Matterhorn as solver, then, the `domain` section of the task dictionary
should be created in this way:

.. code-block:: python

   domain = AcousticDomain()

   filename_prefix = "model/salt_body_3D"
   n = (676, 676, 210)
   d = (25.0, 25.0, 25.0)

   domain.set_model_from_matterhorn_binary(
      filename_prefix=filename_prefix,
      n=n,
      d=d,
      create_hdf5=False
   )

   def cervino_domain_dict():
      return domain.get_domain_dict()

Matterhorn and cervino HDF5 file
================================
If the `cervino` HDF5 file is available and we want to use
Matterhorn as solver, then, the `domain` section of the task dictionary
should be created in this way:

.. code-block:: python

   domain = AcousticDomain()

   fn_h5 = "model/salt_body_3D.h5"

   domain.set_model_from_hdf5(fn_h5=fn_h5, create_solver_binary='matterhorn')

   def cervino_domain_dict():
      return domain.get_domain_dict()

The binary files need to be created only once. After their creation, one should
follow the instructions in the *Salvus and cervino HDF5 file* scenario below.

Salvus and cervino HDF5 file
============================
If the `cervino` HDF5 file is available and we want to use
Salvus as solver, then, the `domain` section of the task dictionary
should be created in this way:

.. code-block:: python

   domain = AcousticDomain()

   fn_h5 = "model/salt_body_3D.h5"

   domain.set_model_from_hdf5(fn_h5=fn_h5)

   def cervino_domain_dict():
      return domain.get_domain_dict()

Salvus and Matterhorn-ready binary files
========================================
If the Matterhorn-ready binary files are available and we want to use
Salvus as solver, then, the `domain` section of the task dictionary
should be created in this way:

.. code-block:: python

   domain = AcousticDomain()

   filename_prefix = "model/salt_body_3D"
   n = (676, 676, 210)
   d = (25.0, 25.0, 25.0)

   domain.set_model_from_matterhorn_binary(
      filename_prefix=filename_prefix,
      n=n,
      d=d,
      create_hdf5=True
   )

   def cervino_domain_dict():
      return domain.get_domain_dict()

The HDF5 needs to be created only once. After its creation, one should
follow the instructions in the *Salvus and cervino HDF5 file* scenario above.
