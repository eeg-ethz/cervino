####
TOML
####

``TOML`` stands for *Tom's Own Minimal Language* and
it is a configuration language developed by Tom Preston-Werner.

- GitHub official `repository <https://github.com/toml-lang/toml>`_
- `Intro to TOML <https://npf.io/2014/08/intro-to-toml/>`_
