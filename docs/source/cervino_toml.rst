Cervino TOML file
=================

``Cervino`` is steered by input files.
These can be either `TOML <https://github.com/toml-lang/toml>`_ files or equivalent JSON files.
We recommend using Python dictionaries to generate the input files but you can also do it manually.

Example file with all the options. Please note that many of these might be mutally exclusive and/or optional.
A detailed description of all parameters follows.

.. literalinclude :: ../../notebooks/cervino_input_file_v1.toml
   :language: toml


General section
---------------
This groups is used to add general information, such as user name and solver used.

.. code-block:: toml

    [general]
    solver = "matterhorn"
    user = "bfilippo"

Domain section
---------------
This section describes the physical domain in which to run simulations.

.. code-block:: toml

    [domain]
    dimension = 3

    [domain.geometry]
    origin = [ 0, 0, 0,]
    number-of-cells = [ 500, 1, 250,]
    cell-size = [ 2.0, 2.0, 2.0,]

    [domain.model]
    filename = "acoustic2d.h5"
    format = "hdf5"

Time section
---------------
This section describes the time parameters of the simulations.

.. code-block:: toml

    [time]
    time-step-in-seconds = 0.0001
    number-of-timesteps = 12000
    start-time-in-seconds = 0.0

Physics section
---------------
This section introduces general parameters needed to define the physics of the simulation.

.. code-block:: toml

    [physics]
    spatial-operator = "taylor_4"

    [[physics.boundaries]]
    type = "pml"
    freesurface = true
    pml-width-in-gridpoints = 10
    pml-power = 4
    pml-frequency = 30.0
    pml-damping-vel = 3000.0

Source section
---------------
This section describes the sources used in the simulation.

.. code-block:: toml

    [source]
    [[source.point-source]]
    location = [ 400.0, 0.0, 20.0,]
    spatial-type = "fz"
    spatial-weights = 1.0

    [[source.point-source.source-time-function]]
    center-frequency = 30.0
    wavelet = "ricker"
    time-shift-in-seconds = 0.0

    [[source.boundary-source]]
    location = "locations.loc"
    spatial-type = "fz"

Output section
---------------
This section defines the output of the simulation.

.. code-block:: toml

    [output]

    [output.gather]
    format = "su"
    start-timestep = 0
    end-timestep = 11999
    timestep-increment = 10

    [[output.gather.receivers]]
    receiver-origin = [ 0.0, 0.0, 20.0,]
    receiver-increment = [ 2.0, 0.0, 0.0,]
    number-of-receivers = 500
    attribute = "V01Z"
    filename-prefix = "vz_gather"

    [[output.gather.receivers]]
    receiver-origin = [ 0.0, 0.0, 20.0,]
    receiver-increment = [ 2.0, 0.0, 0.0,]
    number-of-receivers = 500
    attribute = "V010X"
    filename-prefix = "vx_gather"

    [output.slice]
    format = "su"
    start-timestep = 0
    end-timestep = 11999
    timestep-increment = 1000

    [[output.snapshot.slice]]
    axis = "y"
    slice-index = 100
    attribute = "V01Z"
    filename-prefix = "vz_slice"