************
Dictionaries
************

===================
Workflow dictionary
===================

.. code-block:: python

    workflow_dict = {
        'compute_forward_simulation': SolverSite('matterhorn', 'aug04'),
    }

==================
Cervino dictionary
==================

.. code-block:: python

    cervino_dict = {
        'name': 'marmousi_matterhorn',
        'desc': 'Simple Matterhorn forward simulation on the Marmousi model',
        'output_folder': 'output',
        'workflow_type': 'forward',
        'workflow_dict': workflow_dict
    }

==============
Run dictionary
==============

.. code-block:: python

    run_dict = {
        'progress': True,
        'verbose': True,
        'n_ranks': 8,
        'max_ranks': 16,
    }