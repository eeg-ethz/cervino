**************************
Compute forward simulation
**************************

Here below we describe the dictionary required to define a task of the
``compute_forward_simulation`` job. The specific task is identified by a
unique identifier (UID). In this case, the UID is ``0000``.
The dictionary is shown here in TOML format because it is more human readable.

The full TOML file can be accessed `here <./dicts/dict_task_forward.toml>`_.

General section
---------------
This section is used to add general information, such as user name and solver.

.. code-block:: toml

    [0000.general]
    uid = "0000"
    task_type = "compute_forward_simulation"
    solver = "matterhorn"
    desc = "matterhorn and marmousi"

.. note::

    Currently, the ``solver`` value in the general section is overridden
    by the ``solver`` value provided directly during the creation of
    the ``Cervino`` object.

Domain section
---------------
This section describes the physical domain in which to run simulations.

.. code-block:: toml

    [0000.domain]
    dimension = 2

    [0000.domain.geometry]
    origin = [ "0.0", 0.0, "0.0",]
    number-of-cells = [ 2301, 1, 751,]
    cell-size = [ "4.0", 1.0, "4.0",]

    [0000.domain.model]
    filename = "marmousi_original_model/marmousi_original.h5"
    format = "hdf5"
    required-models = [ "VP", "RHO",]

Time section
---------------
This section describes the time parameters of the simulations.

.. code-block:: toml

    [0000.time]
    time-step-in-seconds = 0.0004
    number-of-timesteps = 5000
    start-time-in-seconds = 0.0

Physics section
---------------
This section introduces general parameters needed to define the physics of the simulation.

.. code-block:: toml

    [0000.physics]
    spatial-operator = "taylor_4"
    [[0000.physics.boundaries]]
    type = "pml"
    freesurface = "True"
    pml-width-in-gridpoints = 10
    pml-power = 4
    pml-frequency = 20.0
    pml-damping-vel = 2800.0

Source section
---------------
This section describes the sources used in the simulation.

.. code-block:: toml

    [0000.source.point-source]
    location = [ 2000, 0.0, 20.0,]
    spatial-type = "q"
    spatial-weights = 1.0

    [0000.source.point-source.source-time-function]
    center-frequency = 20.0
    wavelet = "ricker"
    time-shift-in-seconds = 0.075
    time-shift-in-cycles = 1.5
    filename-prefix = "source"

Output section
---------------
This section defines the output of the simulation.

.. code-block:: toml

    [0000.output.gather]
    start-timestep = 0
    end-timestep = 4999
    timestep-increment = 1
    format = "su"
    type = "gather"

    [0000.output.gather.receivers.gather_0000]
    receiver-origin = [ 0.0, 0.0, 2000.0,]
    receiver-increment = [ "4.0", 0.0, 0.0,]
    number-of-receivers = 2301
    fields = [ "sxx", "vx", "vz",]
    filename-prefix = "gather0"

    [0000.output.slice_time]
    start-timestep = 0
    end-timestep = 4999
    timestep-increment = 500
    format = "su"
    type = "slice_time"

    [0000.output.slice_time.slices.slice_time_0000]
    axis = "y"
    slice-index = 0
    fields = [ "vx", "vz",]
    filename-prefix = "slice_time"

    [0000.output.slice_dft]
    dft-frequencies = [ 5, 10, 15, 20, 25, 30, 35, 40,]
    dft-wavelet-coefficients = "dft_wavelet_coefficients_source0_nogf.bin"
    format = "su"
    type = "slice_dft"

    [0000.output.slice_dft.slices.slice_dft_0000]
    axis = "y"
    slice-index = 0
    fields = [ "sxx",]
    filename-prefix = "slice_dft"