##########################
Installation on Windows 10
##########################

Step 1
======
`Download <https://git-scm.com/download/win>`_ and install Git for Windows 64bit.
During the installation process, you can accept all the default options.

Step 2
======
`Download <https://www.anaconda.com/distribution/>`_
and install the Python Anaconda Distribution for Windows 10 64bit.
When asked about advanced options, accept the default choices.

.. image:: anaconda_path.jpg
   :width: 80pt

Step 3
======
Set email and user name for Git:

.. code-block:: shell

    git config --global user.email "insert_your_email_here"
    git config --global user.name "insert_your_user_name_here"

Step 4
======
Unless already done, `generate a new SSH key pair <https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair>`_:

.. code-block:: shell
    
    ssh-keygen -o -t rsa -b 4096 -C "insert_your_email_here"

You should press Enter to accept the default location. You can also set an empty passphrase pressing Enter twice.

Copy the public key to the clipboard:

.. code-block:: shell

    cat ~/.ssh/id_rsa.pub | clip

And then add it to your GitLab settings in the ``SSH Keys`` section:

Step 5
======
After completing all the previous steps, you can clone the repository to your own computer.
First choose an appropriate location on your file system and then clone it:

.. code-block:: shell

    git clone git@gitlab.com:matterhorn-eth/cervino.git

You need to insert ``yes`` when asked.

Step 6
======
Then you need to create a conda environment:

.. code-block:: shell

    conda update conda
    conda env create -f environment.yml
    conda activate cervino
    
    python -m ipykernel install --user --name cervino --display-name "Python (cervino)"

You will need to activate this environment every time you want to use Cervino.

Step 7
======
Download a modified version of the  TOML package and install it locally:

.. code-block:: shell

    git clone https://github.com/filippo82/toml.git
    cd toml
    python setup.py install

Step 8
======
Finally, you install Cervino in development mode:

.. code-block:: shell

    cd cervino
    python setup.py develop
