*****************
Output definition
*****************

The output of a ``cervino`` simulation is an HDF5 file.
Each HDF5 file is created from an ``xarray.Dataset``.
The ``xarray.Dataset`` containes the following groups, datasets, and attibutes:

* xxx
* xxx

.. note::

   An example will soon be added above!

You can load an existing ``cervino`` output file:

.. code-block:: python

    import xarray as xr
    ds = xr.open_dataset('output.h5')
