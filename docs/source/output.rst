Output
======

.. automodule:: cervino.output.receivers
    :members:

.. automodule:: cervino.output.slices
    :members:
