######
xarray
######

``xarray`` is an open source project and Python package
that makes working with labelled multi-dimensional arrays simple, efficient, and fun!

``xarray`` introduces labels in the form of dimensions, coordinates and attributes
on top of raw ``NumPy``-like arrays, which allows for a more intuitive, more concise,
and less error-prone developer experience.
The package includes a large and growing library of domain-agnostic functions
for advanced analytics and visualization with these data structures.

Useful resources
================

* https://xarray.pydata.org

* https://www.unidata.ucar.edu/software/netcdf/docs/index.html

* https://www.unidata.ucar.edu/software/netcdf/docs/interoperability_hdf5.html

* https://www.unidata.ucar.edu/software/netcdf/docs/faq.html#How-can-I-convert-netCDF-4-files-into-HDF5-files

* https://github.com/Unidata/netcdf4-python