#####
To Do
#####

*******
cervino
*******

core
====

core.py
-------

* [] Remove unnecessary static methods

domain
======

input
=====

output
======

solvers
=======

salvus.py
---------

* [x] Remove hardcoded site name, e.g., `aug04`

salvus_to_cervino.py
--------------------

* [] Adapt the gather conversion to 3D

util
====
