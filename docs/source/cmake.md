mkdir build
cd build
cmake ..
cmake --build .


# Resources

* [How to use a different compiler](https://gitlab.kitware.com/cmake/community/-/wikis/FAQ#how-do-i-use-a-different-compiler)
* [](http://www.brianlheim.com/2018/04/09/cmake-cheat-sheet.html)
* [It's Time To Do CMake Right](https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/)
* [An Introduction to Modern CMake](https://cliutils.gitlab.io/modern-cmake)
* [CGold: The Hitchhiker’s Guide to the CMake](https://cgold.readthedocs.io/en/latest/index.html)