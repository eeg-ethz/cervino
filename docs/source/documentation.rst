#################################
How to properly document the code
#################################

We use `Sphinx <http://www.sphinx-doc.org/en/master/index.html>`_
which is a tool that makes it easy to create intelligent and beautiful documentation.

The `documentation <https://eeg-ethz.gitlab.io/cervino/>`_
will be automatically generated whenever a modification
is committed to the repository. When this happens, the instructions found in
`.gitlab-ci.yml <https://gitlab.com/eeg-ethz/cervino/-/raw/master/.gitlab-ci.yml>`_
are exectued by `GitLab CI <https://docs.gitlab.com/ee/ci/>`_.

The documentation is hosted on
`GitLab Pages <https://docs.gitlab.com/ee/user/project/pages/>`_ and
it is beautifully formatted using the ReadTheDocs theme.
This `blog post <http://www.ericholscher.com/blog/2016/jul/1/sphinx-and-rtd-for-writers/>`_
also gives a nice overview of Sphinx and ReadTheDocs.

We use the `ReStructuredText <https://en.wikipedia.org/wiki/ReStructuredText>`_
format to write our documentation.