Input
=====

.. code-block:: toml

    [[source]]
    name = "source_one"
    location = [1000.0, 0.0, 500.0]
    temporal_type = "ricker"
    spatial_type = "fz"
    center_frequency = 25.0
    scale = [0.0, -1e10]
    delay = [1.5]

.. automodule:: cervino.input.sources
    :members:

.. automodule:: cervino.input.wavelets
    :members:
