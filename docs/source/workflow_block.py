from cervino.core import definitions

with open('./workflow_block.rst', 'w') as f:
    for k in definitions.workflows_valid.keys():
        f.write(f"* ``{k}``\n")

with open('./jobs_block.rst', 'w') as f:
    for k, v in definitions.tasks_valid.items():
        f.write(f"* ``{k}``\n\n")

with open('./workflow_jobs_block.rst', 'w') as f:
    for k, v in definitions.workflows_valid.items():
        f.write(f"* ``{k}``\n\n")
        for job in v:
            f.write(f"\t* ``{job}``\n")   