********************
Cervino and clusters
********************

=====
Euler
=====

`Euler <https://scicomp.ethz.ch/wiki/Euler>`_

.. warning::

    Work in progress!

Access Euler without having to type a password.

* Create `SSH keys <https://scicomp.ethz.ch/wiki/Accessing_the_clusters#SSH_keys>`_ and set up password-less access to Euler.

.. code-block:: shell

    # Path for Matterhorn
    export PATH=$PATH:/cluster/project/eeg/apps/matterhorn_gitlab/bin

* env2lmod

* module load gcc/8.2.0

* module load openmpi/4.0.2

* module load llvm/6.0.0

* export OMPI_CXX=clang++

* mpic++ --showme:command


* module load gcc/6.3.0
* module load openmpi/3.0.1
* module load llvm/6.0.0

env2lmod && module load gcc/8.2.0 && module load openmpi/4.0.2 && module load llvm/6.0.0 && module list
env2lmod && module load gcc/4.8.5 && module load openmpi/4.0.2 && module load llvm/5.0.1 && module list


/cluster/spack/apps/linux-centos7-x86_64/gcc-4.8.5/gcc-8.2.0-6xqov2fhvbmehix42slain67vprec3fs/

Compile LLVM and clang

env2lmod && module load gcc/8.2.0 && module load openmpi/4.0.2 && module load cmake/3.4.3


https://clang.llvm.org/get_started.html

cmake '-DLLVM_ENABLE_PROJECTS=clang;openmp' -DGCC_INSTALL_PREFIX=/cluster/spack/apps/linux-centos7-x86_64/gcc-4.8.5/gcc-8.2.0-6xqov2fhvbmehix42slain67vprec3fs/ -DCMAKE_INSTALL_PREFIX=/cluster/project/eeg/apps/llvm-project-2/install-dir -DCMAKE_BUILD_TYPE=Release -DLLVM_TARGETS_TO_BUILD=X86 -G "Unix Makefiles" ../llvm

bsub -n 24 -R "rusage[mem=2048]" -W 04:00 make -j24

make install

export PATH=/cluster/project/eeg/apps/llvm-project-2/install-dir/bin:$PATH


clang++ $COMPILE_FLAGS -o hello -L/lib hello.o -lstdc++ -lm -lmpi

/cluster/spack/apps/linux-centos7-x86_64/gcc-8.2.0/openmpi-4.0.2-vvr7fdofwljfy4qgkkhao36r5qx44hni/share/openmpi/mpic++-wrapper-data.txt

Info on compiler flags:
https://www.linuxquestions.org/questions/programming-9/gcc-and-march%3Dnative-optimization-4175619410/


Info on compiling and linking:
https://www3.ntu.edu.sg/home/ehchua/programming/cpp/gcc_make.html

Info on setting up the OpenMPI wrappers:
https://www.open-mpi.org/faq/?category=mpi-apps#override-wrappers-after-v1.0


env2lmod && module load gcc/8.2.0 && module load openmpi/4.0.2 && module load llvm/6.0.0 && module load cmake/3.9.4

