############
Installation
############

`cervino` is primarly developed and tested in a GNU/Linux environment.
However, it should be possible to install `cervino` and the solvers on 
Windows and macOS too.

Before starting with the installation of `cervino`, you need to make sure
that Git and Python are already installed on your copmuter.

***
Git
***
If not already present, install `Git <https://git-scm.com/downloads>`_
on your computer. During the installation process, you can accept all the
default options. Before using Git for the first time, you need to set
email and user name:

.. code-block:: shell

    git config --global user.email "insert_your_email_here"
    git config --global user.name "insert_your_user_name_here"

******
Python
******
If a working Python environment is not present on your computer,
it is recommended to
`download <https://www.anaconda.com/distribution/#download-section>`_
and install the Python Anaconda Distribution.
Information regarding the Anaconda installer are available for
`Windows <https://docs.anaconda.com/anaconda/install/windows/>`_,
`macOS <https://docs.anaconda.com/anaconda/install/mac-os/>`_,
and `Linux <https://docs.anaconda.com/anaconda/install/linux/>`_.
During the installation process, when asked about advanced options,
accept the default choices.

*******
cervino
*******

Step 1
======
You are now ready to clone the `cervino` repository to your own computer.
First choose an appropriate location on your file system and then clone it:

.. code-block:: shell

    git clone git@gitlab.com:eeg-ethz/cervino.git

    git clone https://gitlab.com/eeg-ethz/cervino.git

You need to insert ``yes`` when asked.

This process will create a `cervino` folder in the current location.

Step 2
======
Then you need to create a conda environment:

.. code-block:: shell

    cd cervino
    conda update conda
    conda env create -f environment.yml
    conda activate cervino
    
    python -m ipykernel install --user --name cervino --display-name "Python (cervino)"

You will need to activate this environment every time you want to use `cervino`:

.. code-block:: shell

    conda activate cervino

Step 3
======
Download the ``TOML`` package and install it locally:

.. code-block:: shell

    git clone https://github.com/uiri/toml.git
    cd toml
    python setup.py install

.. warning::

    Do not clone it insde the `cervino` folder and 
    do not install the ``TOML`` package using ``pip install toml``!

Step 4
======
Finally, you should install ``cervino`` in development mode:

.. code-block:: shell

    cd cervino
    python setup.py develop
    pip install -e .

Step 5
======
Activate this plug-in to show nicer error traces in the Notebooks:

.. code-block:: shell

    jupyter nbextension enable skip-traceback/main

***********************************
Instructions to install the solvers
***********************************

.. toctree::
    :maxdepth: 1
    
    Matterhorn <installation_matterhorn.rst>
    Salvus <installation_salvus.rst>