.. _api:

API
===

.. toctree::
    :maxdepth: 2
    
    core
    domain
    input
    output
    solvers
    objective_functions
    optimization
    utils
