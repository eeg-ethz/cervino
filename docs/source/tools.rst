External packages
==================

This section goes into some more details about
various tools and packages used by ``cervino``.

.. toctree::
    :maxdepth: 1

    xarray <xarray.rst>
    TOML <toml.rst>
    Jupytext <jupytext.rst>
