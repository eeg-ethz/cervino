################
Jupyter Notebook
################

What is it?
===========
The `Jupyter Notebook <https://jupyter.org/>`_ is an open-source web application
that allows one to create and share documents that contain
live code, equations, visualizations and narrative text.

How to start a Jupyter notebook
===============================
It is simple as this:

.. code-block:: shell

    coda activate cervino
    jupyter notebook

.. warning::

    The Jupyter server might not directly open
    in a web browser.


How to start a Jupyter notebook
===============================
It is simple as this:

On the remote host, open the terminal, change directory to where you have
your notebooks and type:
.. code-block:: shell

    coda activate cervino
    jupyter notebook --no-browser --port=8889

After starting the Jupyter server, 
.. code-block:: shell

    http://localhost:8889/?token=9d2e9fd3e03c3883296bddbefd48797f5fce733096252e54

In your local computer, open MS-DOS cmd (if using Windows) or Unix terminal, then type:

.. code-block:: shell

    coda activate cervino
    ssh -N -f -L localhost:8888:localhost:8889 username@your_remote_host_name



http://localhost:8889/?token=9d2e9fd3e03c3883296bddbefd48797f5fce733096252e54

http://localhost:8888/?token=9d2e9fd3e03c3883296bddbefd48797f5fce733096252e54