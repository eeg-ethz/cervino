###########
Development
###########

This section goes into some more details about
how to contribute to the development of ``cervino``.

.. note::

    Work in progress.

================
Add a new solver
================
Adding a new solver to ``cervino`` should be simple.
One needs to add the name of the solver to the module
`cervino.core.definition <https://gitlab.com/eeg-ethz/cervino/-/raw/master/cervino/core/definitions.py>`_
and create three additional submodules inside the `solvers <https://gitlab.com/eeg-ethz/cervino/-/tree/master/cervino%2Fsolvers>`_
submodule. For example, if the new solver is called ``eiger``,
the three new modules will be:

* ``eiger.py``
* ``eiger_to_cervino.py``
* ``cervino_to_eiger.py``

==============
Best practices
==============

* `Type hints <https://www.python.org/dev/peps/pep-0484/>`_
