######################
Installation on Ubuntu
######################

Step 1
======
Install Git:

.. code-block:: shell

    sudo apt-get install git

Step 2
======
Set email and user name for Git:

.. code-block:: shell

    git config --global user.email "insert_your_email_here"
    git config --global user.name "insert_your_user_name_here"

Step 3
======
Make sure that a working Python environment is installed.
To do so, it is recommended to 
`download <https://www.anaconda.com/distribution/>`_
and install the Python Anaconda Distribution for Linux 64bit.
When asked about advanced options, accept the default choices.

Step 4
======
Unless already done, `generate a new SSH key pair <https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair>`_:

.. code-block:: shell
    
    ssh-keygen -o -t rsa -b 4096 -C "insert_your_email_here"

You should press Enter to accept the default location.
You can also set an empty passphrase pressing Enter twice.

.. note::
    When using an older version of ``ssh``,
    the command above might give an error.

Copy the public key to the clipboard:

.. code-block:: shell

    cat ~/.ssh/id_rsa.pub

And then add it to your GitLab settings in the ``SSH Keys`` section:

Step 5
======
After completing all the previous steps,
you can clone the repository to your own computer.
First choose an appropriate location on your file system and then clone it:

.. code-block:: shell

    git clone git@gitlab.com:matterhorn-eth/cervino.git

You need to insert ``yes`` when asked.

Step 6
======
Then you need to create a conda environment:

.. code-block:: shell

    conda update conda
    conda env create -f environment.yml
    conda activate cervino
    
    python -m ipykernel install --user --name cervino --display-name "Python (cervino)"

You will need to activate this environment every time you want to use ``cervino``:

.. code-block:: shell

    conda activate cervino

Step 7
======
Download the ``TOML`` package and install it locally:

.. code-block:: shell

    git clone https://github.com/uiri/toml.git
    cd toml
    python setup.py install

.. warning::

    Do not install the ``TOML`` package using ``pip install toml``!

Step 8
======
Finally, you install ``cervino`` in development mode:

.. code-block:: shell

    cd cervino
    python setup.py develop

Step 9
======
Activating this plug-in to show nicer error traces in the Notebooks:

.. code-block:: shell

    jupyter nbextension enable skip-traceback/main