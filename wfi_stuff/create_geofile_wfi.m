clear all; close all;

geofile='/w07d2/krampev/simulation_Filippo/marmousi.dat';
inputfile='/w07d2/krampev/simulation_Filippo/wfi_fwd.inp';

%%
% source and receiver spacings
dd_s = 4.0;
dd_r = 4.0;

% frequencies
ff = 5:5:80;

% Geometry
% marmousi
xx_s = 4000;
zz_s = 20;
xx_r = 0:dd_r:2300*dd_r;
zz_r = ones(1,length(xx_r))*40;

np_s = length(xx_s);
np_r = length(xx_r);

% Frequencies
f = ff;
nf = length(f);

% Source function
sou = ones(np_s*nf,1);
recfac = ones(np_r*nf,1);

fid = fopen('/w07d2/krampev/simulation_Filippo/user_defined_ricker_20hz.bin');
stf_time = fread(fid,'float');
fclose(fid);
stf_freq = fft(stf_time);
sou = repmat(stf_freq(11:10:161),1,np_s);


% Output geometry
fid = fopen(sprintf(geofile),'wt');
for a = 1:np_s
    fprintf(fid,'SPOINT %4d %f %f\n',a-1,xx_s(a),zz_s(a));
end;
for a = 1:np_r
    fprintf(fid,'SPOINT %4d %f %f\n',a-1+np_s,xx_r(a),zz_r(a));
end;

% Output frequencies and source amplitudes
for a = 1:nf
    fprintf(fid,'FREQ %4d %f\n',a-1,f(a));
end;

for a = 1:nf
    for b = 1:np_s
        c = (a-1)*np_s + b-1;
       fprintf(fid,'SOURCE %4d %e %e\n',c,real(sou((a-1)*np_s+b)),imag(sou((a-1)*np_s+b)));
    end;
end;
for a = 1:nf
    for b = 1:np_r
        c = (a-1)*np_r + b-1;
       fprintf(fid,'RECEIVER %4d %e %e\n',c,real(recfac((a-1)*np_r+b)),imag(recfac((a-1)*np_r+b)));
    end;
end;

% Output dummy data file for computing synthtetics
for a = 1:nf
   for b = 1:np_s
      for c = 1:np_r 
         fprintf(fid,'DATA %4d %4d %4d %4d %4d 0 1.0 0.0 1.0\n',b-1,np_s+c-1,a-1,(b-1)*nf+a-1, (a-1)*np_r+c-1);
      end;
    end;
 end;
fclose(fid);

