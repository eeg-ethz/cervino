function dd = getwfidata(fname)
%
a = 0;
b = 0;
c = 0;
d = 0;
e = 0;

fid = fopen(fname,'rt');
while(feof(fid) == 0)
    ll = fgets(fid);
    if (strncmp('DATA',ll(1:4),4) == 1)
        a =  a + 1;
    end;
    if (strncmp('SPOINT',ll(1:5),5) == 1)
        b =  b + 1;
    end;
    if (strncmp('SOURCE',ll(1:6),6) == 1)
        c =  c + 1;
    end;
    if (strncmp('RECEIVER',ll(1:8),8) == 1)
        d =  d + 1;
    end;
    if (strncmp('FREQ',ll(1:4),4) == 1)
        e =  e + 1;
    end;
end;
fclose(fid);
ina = zeros(a,9);
inb = zeros(b,3);
inc = zeros(c,3);
ind = zeros(d,3);
ine = zeros(e,2);
a = 0;
b = 0;
c = 0;
d = 0;
e = 0;
fid = fopen(fname,'rt');
while(feof(fid) == 0)
    ll = fgets(fid);
    if (strncmp('DATA',ll(1:4),4) == 1)
        a =  a + 1;
        ina(a,:) = sscanf(ll,'%*s %d %d %d %d %d %d %e %e %e');
    end;
    if (strncmp('SPOINT',ll(1:5),5) == 1)
        b =  b + 1;
        inb(b,:) = sscanf(ll,'%*s %d %f %f');
    end;
    if (strncmp('SOURCE',ll(1:6),6) == 1)
        c =  c + 1;
        inc(c,:) = sscanf(ll,'%*s %d %f %f');
    end;
    if (strncmp('RECEIVER',ll(1:8),8) == 1)
        d =  d + 1;
        ind(d,:) = sscanf(ll,'%*s %d %f %f');
    end;
    if (strncmp('FREQ',ll(1:4),4) == 1)
        e =  e + 1;
        ine(e,:) = sscanf(ll,'%*s %d %f');
    end;
end;
fclose(fid);


dd.sno = ina(:,1);
dd.rno = ina(:,2);
dd.fno = ina(:,3);
dd.souno = ina(:,4);
dd.recfacno = ina(:,5);
dd.dtype = ina(:,6);
dd.data = ina(:,7) + 1i*ina(:,8);
dd.w = ina(:,9);

dd.gidx = inb(:,1);
dd.gx = inb(:,2);
dd.gz = inb(:,3);

dd.souidx = inc(:,1);
dd.sour = inc(:,2);
dd.soui = inc(:,3);

dd.recfacidx = ind(:,1);
dd.recfacr = ind(:,2);
dd.recfaci = ind(:,3);

dd.fidx = ine(:,1);
dd.f = ine(:,2);

